/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.bdfm.logic;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class ZarzadcaOkna implements WindowListener {
    
    private ZarzadcaOkna() {
    }
    
    public static ZarzadcaOkna dajInstancje() {
        return ZarzadcaOknaHolder.INSTANCE;
    }
 
    private static class ZarzadcaOknaHolder {

        private static final ZarzadcaOkna INSTANCE = new ZarzadcaOkna();
    }
    
    @Override
    public void windowClosing(WindowEvent we) {
        ZarzadcaBazaNeo4J.dajInstancje().zamknijBaze();
    }
    
//<editor-fold defaultstate="collapsed" desc="nieobsłużone zdarzenia">
    @Override
    public void windowOpened(WindowEvent we) {
    }
    
    @Override
    public void windowClosed(WindowEvent we) {
    }
    
    @Override
    public void windowIconified(WindowEvent we) {
    }
    
    @Override
    public void windowDeiconified(WindowEvent we) {
    }
    
    @Override
    public void windowActivated(WindowEvent we) {
    }
    
    @Override
    public void windowDeactivated(WindowEvent we) {
    }
//</editor-fold>
}
