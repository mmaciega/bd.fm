package pl.bdfm.logic;

import java.util.List;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class Artysta extends Wezel {

    private String nazwaZespolu;
    private List<String> listaGatunkow;
    private String krajPochodzenia;
    private String opis;

    private List<String> listaAlbumow;
    private List<String> listaPodobnych;

    public Artysta(long ID, String nazwaZespolu, String krajPochodzenia, List<String> listaGatunkow, String opis, List<String> listaAlbumow, List<String> listaPodobnych) {
        super(ID);

        this.nazwaZespolu = nazwaZespolu;
        this.krajPochodzenia = krajPochodzenia;
        this.listaGatunkow = listaGatunkow;
        this.opis = opis;
        this.listaAlbumow = listaAlbumow;
        this.listaPodobnych = listaPodobnych;
    }

    public Artysta(String nazwaZespolu, String krajPochodzenia, List<String> listaGatunkow, String opis) {
        super(-1);

        this.nazwaZespolu = nazwaZespolu;
        this.krajPochodzenia = krajPochodzenia;
        this.listaGatunkow = listaGatunkow;
        this.opis = opis;
        this.listaAlbumow = null;
        this.listaPodobnych = null;
    }

    /**
     * @return the nazwaZespolu
     */
    public String getNazwaZespolu() {
        return nazwaZespolu;
    }

    /**
     * @return the listaGatunkow
     */
    public List<String> getListaGatunkow() {
        return listaGatunkow;
    }

    /**
     * @return the krajPochodzenia
     */
    public String getKrajPochodzenia() {
        return krajPochodzenia;
    }

    /**
     * @return the opis
     */
    public String getOpis() {
        return opis;
    }

    /**
     * @return the listaAlbumow
     */
    public List<String> getListaAlbumow() {
        return listaAlbumow;
    }

    /**
     * @return the listaPodobnych
     */
    public List<String> getListaPodobnych() {
        return listaPodobnych;
    }

    @Override
    public String toString() {
        String gatunki = "";
        if(getListaAlbumow() != null && !getListaAlbumow().isEmpty()) {
            for (int i = 0; i < getListaGatunkow().size() - 1; i++) {
                gatunki += getListaGatunkow().get(i) + ": ";
            }
            gatunki += getListaGatunkow().get(getListaGatunkow().size() - 1);
        }

        String albumy = "";
        
        if(getListaAlbumow() != null && !getListaAlbumow().isEmpty()) {
            for (int i = 0; i < getListaAlbumow().size() - 1; i++) {
                albumy += getListaAlbumow().get(i) + ": ";
            }
            albumy += getListaAlbumow().get(getListaAlbumow().size() - 1);
        }
        
        return (getNazwaZespolu() + ", gatunki: " + gatunki + ", albumy: " + albumy + ", " + getKrajPochodzenia() + ", " + getOpis());

    }
}
