
package pl.bdfm.logic;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class FunkcjeTekstowe {
    public static String duzeLiteryKazdegoSlowa(String string) {
      char[] chars = string.toLowerCase().toCharArray();
      boolean found = false;
      for (int i = 0; i < chars.length; i++) {
        if (!found && Character.isLetter(chars[i])) {
          chars[i] = Character.toUpperCase(chars[i]);
          found = true;
        } else if (Character.isWhitespace(chars[i]) || chars[i]=='.' || chars[i]=='\'') { // You can add other chars here
          found = false;
        }
      }
      
      return String.valueOf(chars);
    }
    
    public static String pierwszaLiteraDuza(String string) {
        String string2 = string.toLowerCase();
        string2 = Character.toString(string2.charAt(0)).toUpperCase()+string2.substring(1);

        return string2;
    }
}
