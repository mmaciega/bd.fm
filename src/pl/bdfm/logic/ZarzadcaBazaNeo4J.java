/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bdfm.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.tooling.GlobalGraphOperations;
import pl.bdfm.gfx.OknoGlowne;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class ZarzadcaBazaNeo4J implements IObserwator {

    @Override
    public void aktualizacjaWezla(IObserwowany.TypyPowiadomienia typPowiadomienia, Wezel wezel) {
        switch (typPowiadomienia) {
            case DODANIE:
                dodajWezel(wezel);
                break;
            case EDYCJA:

                break;
            case USUNIECIE:
                usunWezel(wezel);
                break;
        }
    }

    @Override
    public void aktualizacjaRelacji(IObserwowany.TypyPowiadomienia typPowiadomienia, TypyRelacji typRelacji, Wezel wezel1, Wezel wezel2, Direction kierunek) {
        switch (typPowiadomienia) {
            case DODANIE:
                dodajRelacje(typRelacji, wezel1, wezel2, kierunek);
                break;
            case EDYCJA:

                break;

            case USUNIECIE:
                usunRelacje(typRelacji, wezel1, wezel2, kierunek);
                break;
        }
    }

    private void usunRelacje(TypyRelacji typRelacji, Wezel wezel1, Wezel wezel2, Direction kierunek) {
        String komunikat = null;
        TypyWezlow typWezla1 = null, typWezla2 = null;
        String nazwa1 = null, nazwa2 = null;

        switch (typRelacji) {
            case LUBI:
                typWezla1 = TypyWezlow.UZYTKOWNIK;
                nazwa1 = ((Uzytkownik) wezel1).getPseudonim();

                typWezla2 = TypyWezlow.ARTYSTA;
                nazwa2 = ((Artysta) wezel2).getNazwaZespolu();

                break;
            case ZNA:
                typWezla1 = TypyWezlow.UZYTKOWNIK;
                nazwa1 = ((Uzytkownik) wezel1).getPseudonim();

                typWezla2 = TypyWezlow.UZYTKOWNIK;
                nazwa2 = ((Uzytkownik) wezel2).getPseudonim();
                break;
            case PODOBNY:
                typWezla1 = TypyWezlow.ARTYSTA;
                nazwa1 = ((Artysta) wezel1).getNazwaZespolu();

                typWezla2 = TypyWezlow.ARTYSTA;
                nazwa2 = ((Artysta) wezel2).getNazwaZespolu();
                break;
            default:
                komunikat = "Błędny typ relacji";
        }

        if (komunikat == null) {
            try (Transaction tx = graphDB.beginTx()) {

                Node n1 = czyIstniejeWezel(typWezla1, nazwa1);
                Node n2 = czyIstniejeWezel(typWezla2, nazwa2);

                if (n1 != null && n2 != null) {
                    Relationship rel = sprawdzCzyIstniejeRelacja(n1, n2, typRelacji);
                    if (rel != null) {

                        if (kierunek == Direction.BOTH) {
                            rel.delete();
                            
                            rel = sprawdzCzyIstniejeRelacja(n2, n1, typRelacji);
                            rel.delete();
                        } else {
                            rel.delete();
                        }
                        
                    } else {
                        komunikat = "Relacja pomiędzy tymi węzłami nie istnieje.";
                    }
                } else {
                    komunikat = "Jeden z węzłów nie istnieje";
                }

                tx.success();
            }
        }

        if (komunikat == null) {
            zdejmijFlageProblemu();
        } else {
            ustawFlageProblemu(komunikat);
        }
    }
    
    private void dodajRelacje(TypyRelacji typRelacji, Wezel wezel1, Wezel wezel2, Direction kierunek) {
        String komunikat = null;
        TypyWezlow typWezla1 = null, typWezla2 = null;
        String nazwa1 = null, nazwa2 = null;

        switch (typRelacji) {
            case LUBI:
                typWezla1 = TypyWezlow.UZYTKOWNIK;
                nazwa1 = ((Uzytkownik) wezel1).getPseudonim();

                typWezla2 = TypyWezlow.ARTYSTA;
                nazwa2 = ((Artysta) wezel2).getNazwaZespolu();

                break;
            case ZNA:
                typWezla1 = TypyWezlow.UZYTKOWNIK;
                nazwa1 = ((Uzytkownik) wezel1).getPseudonim();

                typWezla2 = TypyWezlow.UZYTKOWNIK;
                nazwa2 = ((Uzytkownik) wezel2).getPseudonim();
                break;
            case PODOBNY:
                typWezla1 = TypyWezlow.ARTYSTA;
                nazwa1 = ((Artysta) wezel1).getNazwaZespolu();

                typWezla2 = TypyWezlow.ARTYSTA;
                nazwa2 = ((Artysta) wezel2).getNazwaZespolu();
                break;
            default:
                komunikat = "Błędny typ relacji";
        }

        if (komunikat == null) {
            try (Transaction tx = graphDB.beginTx()) {

                Node n1 = czyIstniejeWezel(typWezla1, nazwa1);
                Node n2 = czyIstniejeWezel(typWezla2, nazwa2);

                if (n1 != null && n2 != null) {
                    if (sprawdzCzyIstniejeRelacja(n1, n2, typRelacji) == null) {

                        if (kierunek == Direction.BOTH) {
                            ustalRelacje(n1, n2, typRelacji);
                            ustalRelacje(n2, n1, typRelacji);
                        } else {
                            ustalRelacje(n1, n2, typRelacji);
                        }
                        
                    } else {
                        komunikat = "Relacja pomiędzy tymi węzłami już istnieje.";
                    }
                } else {
                    komunikat = "Jeden z węzłów nie istnieje";
                }

                tx.success();
            }
        }

        if (komunikat == null) {
            zdejmijFlageProblemu();
        } else {
            ustawFlageProblemu(komunikat);
        }
    }

    public enum TypyRelacji implements RelationshipType {

        LUBI,
        PODOBNY,
        NALEZY,
        ZNA,
        NAGRAL,
        POCHODZI;
    }

    public enum TypyWezlow implements Label {

        UZYTKOWNIK,
        GATUNEK,
        ARTYSTA,
        ALBUM,
        KRAJ;
    }

    private static final String DB_PATH = "graphDB";

    public static boolean CZY_WYSTAPIL_PROBLEM;
    public static String KOMUNIKAT_OSTATNIEJ_OPERACJI;

    private void ustawFlageProblemu(String trescKomunikatu) {
        CZY_WYSTAPIL_PROBLEM = true;
        KOMUNIKAT_OSTATNIEJ_OPERACJI = trescKomunikatu;
    }

    private void zdejmijFlageProblemu() {
        CZY_WYSTAPIL_PROBLEM = false;
        KOMUNIKAT_OSTATNIEJ_OPERACJI = "";
    }

    private GraphDatabaseService graphDB;

    private ZarzadcaBazaNeo4J() {
        polaczZBaza();
//        graphDB = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
    }

    public static ZarzadcaBazaNeo4J dajInstancje() {
        return ZarzadcaBazaNeo4JHolder.INSTANCE;
    }

    private static class ZarzadcaBazaNeo4JHolder {

        private static final ZarzadcaBazaNeo4J INSTANCE = new ZarzadcaBazaNeo4J();
    }

    public final void polaczZBaza() {
        graphDB = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
    }

    public void zamknijBaze() {
        if (graphDB != null) {
            graphDB.shutdown();
        }
    }

    public List<String> dajWezly(TypyWezlow typWezlow) {
        List<String> listaWezlow = new ArrayList<String>();
        String nazwaPola = zwrocNazwePolaUnikatowego(typWezlow);

        try (Transaction tx = graphDB.beginTx()) {
            for (Node n : GlobalGraphOperations.at(graphDB).getAllNodesWithLabel(typWezlow)) {
                String nazwa = (String) n.getProperty(nazwaPola);
                listaWezlow.add(nazwa);
            }

            tx.success();
        }

        return listaWezlow;
    }

    public Wezel dajWezel(TypyWezlow typWezla, String nazwa) {
        Wezel wezel = null;

        switch (typWezla) {
            case ALBUM:
                wezel = dajAlbum(nazwa);
                break;
            case ARTYSTA:
                wezel = dajArtyste(nazwa);
                break;
            case UZYTKOWNIK:
                wezel = dajUzytkownika(nazwa);
                break;
        }

        return wezel;
    }

    private Album dajAlbum(String nazwa) {
        Album album = null;

        try (Transaction tx = graphDB.beginTx()) {
            for (Node n : GlobalGraphOperations.at(graphDB).getAllNodesWithLabel(ZarzadcaBazaNeo4J.TypyWezlow.ALBUM)) {
                String nz = (String) n.getProperty("nazwa");

                if (nz.equals(nazwa)) {
                    List<String> listaArtystow = dajNazwyWezlowWRelacji(n, ZarzadcaBazaNeo4J.TypyRelacji.NAGRAL, Direction.INCOMING);

                    album = new Album(n.getId(), nazwa,
                            listaArtystow,
                            (String) n.getProperty("czas_trwania", ""),
                            Integer.valueOf((String) n.getProperty("rok_wydania", "0")),
                            Integer.valueOf((String) n.getProperty("liczba_utworow", "0")));
                    break;
                }
            }

            tx.success();
        }

        return album;
    }

    private Uzytkownik dajUzytkownika(String pseudonim) {
        Uzytkownik uzytkownik = null;

        try (Transaction tx = graphDB.beginTx()) {
            for (Node n : GlobalGraphOperations.at(graphDB).getAllNodesWithLabel(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK)) {
                String ps = (String) n.getProperty("pseudonim");

                if (ps.equals(pseudonim)) {
                    List<String> listaZnajomych = dajNazwyWezlowWRelacji(n, ZarzadcaBazaNeo4J.TypyRelacji.ZNA, Direction.OUTGOING);
                    List<String> listaUlubionych = dajNazwyWezlowWRelacji(n, ZarzadcaBazaNeo4J.TypyRelacji.LUBI, Direction.OUTGOING);
                    List<String> listaKrajow = dajNazwyWezlowWRelacji(n, TypyRelacji.POCHODZI, Direction.OUTGOING);

                    uzytkownik = new Uzytkownik(n.getId(), pseudonim,
                            (String) n.getProperty("imie", ""),
                            (String) n.getProperty("nazwisko", ""),
                            listaKrajow.get(0),
                            Integer.valueOf((String) n.getProperty("wiek", "0")),
                            Uzytkownik.znajdzPlec((String) n.getProperty("plec", "")),
                            listaZnajomych,
                            listaUlubionych);
                    break;
                }
            }

            tx.success();
        }

        return uzytkownik;
    }

    private Artysta dajArtyste(String nazwa) {
        Artysta artysta = null;

        try (Transaction tx = graphDB.beginTx()) {
            for (Node n : GlobalGraphOperations.at(graphDB).getAllNodesWithLabel(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA)) {
                String na = (String) n.getProperty("nazwa");

                if (na.equals(nazwa)) {
                    List<String> listaAlbumow = dajNazwyWezlowWRelacji(n, ZarzadcaBazaNeo4J.TypyRelacji.NAGRAL, Direction.OUTGOING);
                    List<String> listaPodobnych = dajNazwyWezlowWRelacji(n, ZarzadcaBazaNeo4J.TypyRelacji.PODOBNY, Direction.OUTGOING);

                    artysta = new Artysta(n.getId(), nazwa,
                            dajNazwyWezlowWRelacji(n, TypyRelacji.POCHODZI, Direction.OUTGOING).get(0),
                            dajNazwyWezlowWRelacji(n, TypyRelacji.NALEZY, Direction.OUTGOING),
                            (String) n.getProperty("opis", ""),
                            listaAlbumow,
                            listaPodobnych);
                }
            }

            tx.success();
        }

        return artysta;
    }

    public List<String> dajNazwyWezlowWRelacji(ZarzadcaBazaNeo4J.TypyWezlow typWezla, String nazwa, ZarzadcaBazaNeo4J.TypyRelacji typRelacji, Direction kierunek) {
        List<String> lista = new ArrayList<>();
        String nazwaPola = zwrocNazwePolaUnikatowego(typWezla);

        try (Transaction tx = graphDB.beginTx()) {
            for (Node n : GlobalGraphOperations.at(graphDB).getAllNodesWithLabel(typWezla)) {
                String ps = (String) n.getProperty(nazwaPola);

                if (ps.equals(nazwa)) {
                    for (Relationship relacja : n.getRelationships(typRelacji, kierunek)) {
                        Node wezelKoncowy = null;
                        switch (kierunek) {
                            case OUTGOING:
                                wezelKoncowy = relacja.getEndNode();
                                break;
                            case INCOMING:
                                wezelKoncowy = relacja.getStartNode();
                                break;
                        }

                        for (Label etykieta : wezelKoncowy.getLabels()) {
                            switch (etykieta.name()) {
                                case "UZYTKOWNIK":
                                    lista.add((String) wezelKoncowy.getProperty("pseudonim"));
                                    break;
                                default:
                                    lista.add((String) wezelKoncowy.getProperty("nazwa"));
                                    break;
                            }
                        }
                    }

                    break;
                }
            }

            tx.success();
        }

        return lista;
    }

    public List<String> dajNazwyWezlowWRelacji(Node wezel, ZarzadcaBazaNeo4J.TypyRelacji typRelacji, Direction kierunek) {
        List<String> lista = new ArrayList<>();

        try (Transaction tx = graphDB.beginTx()) {
            for (Relationship relacja : wezel.getRelationships(typRelacji, kierunek)) {
                Node wezelKoncowy = null;
                switch (kierunek) {
                    case OUTGOING:
                        wezelKoncowy = relacja.getEndNode();
                        break;
                    case INCOMING:
                        wezelKoncowy = relacja.getStartNode();
                        break;
                }

                for (Label etykieta : wezelKoncowy.getLabels()) {
                    switch (etykieta.name()) {
                        case "UZYTKOWNIK":
                            lista.add((String) wezelKoncowy.getProperty("pseudonim"));
                            break;
                        default:
                            lista.add((String) wezelKoncowy.getProperty("nazwa"));
                            break;
                    }
                }
            }

            tx.success();
        }

        return lista;
    }

    public void dodajWezel(Wezel wezel) {
        String komunikat = null;

        if (wezel instanceof Uzytkownik) {
            komunikat = dodajWezelUzytkownika((Uzytkownik) wezel);
        } else if (wezel instanceof Artysta) {
            komunikat = dodajWezelArtysty((Artysta) wezel);
        } else if (wezel instanceof Album) {
            komunikat = dodajWezelAlbum((Album) wezel);
        }
        if (komunikat == null) {
            zdejmijFlageProblemu();
        } else {
            ustawFlageProblemu(komunikat);
        }
    }

    private String dodajWezelAlbum(Album album) {
        TypyWezlow typWezla = TypyWezlow.ALBUM;
        Node wezelAlbumu = czyIstniejeWezel(typWezla, album.getNazwaAlbumu());

        if (wezelAlbumu != null) {
            return "Album o podanej nazwie już istnieje.";
        } else {
            try (Transaction tx = graphDB.beginTx()) {
                wezelAlbumu = dodajWezel(typWezla);
                ustalWlasciwosc(wezelAlbumu, "nazwa", album.getNazwaAlbumu());
                ustalWlasciwosc(wezelAlbumu, "czas_trwania", album.getCzasTrwania());
                ustalWlasciwosc(wezelAlbumu, "liczba_utworow", String.valueOf(album.getLiczbaUtworow()));
                ustalWlasciwosc(wezelAlbumu, "rok_wydania", String.valueOf(album.getRokWydania()));

                for (String wykonawca : album.getNazwaArtystow()) {
                    Node wezelWykonawcy = czyIstniejeWezel(TypyWezlow.ARTYSTA, wykonawca);
                    if (wezelWykonawcy != null) {
                        System.out.println("WYKONAWCA ISTNIEJE - " + wykonawca);
                    } else {
                        System.out.println("WYKONAWCA NIE ISTNIEJE (BLAD) - " + wykonawca);
                        return "Wybrany artysta dla albumu nie istnieje.";
                    }

                    ustalRelacje(wezelWykonawcy, wezelAlbumu, TypyRelacji.NAGRAL);
                }

                tx.success();
            }
        }

        return null;
    }

    private String dodajWezelArtysty(Artysta artysta) {
        TypyWezlow typWezla = TypyWezlow.ARTYSTA;
        Node wezelArtysty = czyIstniejeWezel(typWezla, artysta.getNazwaZespolu());

        if (wezelArtysty != null) {
            return "Artysta o podanej nazwie już istnieje.";
        } else {
            try (Transaction tx = graphDB.beginTx()) {
                wezelArtysty = dodajWezel(typWezla);
                ustalWlasciwosc(wezelArtysty, "nazwa", artysta.getNazwaZespolu());
                ustalWlasciwosc(wezelArtysty, "opis", artysta.getOpis());

                Node wezelKraju = czyIstniejeWezel(TypyWezlow.KRAJ, artysta.getKrajPochodzenia());
                if (wezelKraju != null) {
                    System.out.println("KRAJ ISTNIEJE");
                } else {
                    wezelKraju = dodajWezel(TypyWezlow.KRAJ);
                    ustalWlasciwosc(wezelKraju, "nazwa", artysta.getKrajPochodzenia());
                    System.out.println("KRAJ NIE ISTNIEJE");
                }

                ustalRelacje(wezelArtysty, wezelKraju, TypyRelacji.POCHODZI);

                for (String gatunek : artysta.getListaGatunkow()) {
                    Node wezelGatunku = czyIstniejeWezel(TypyWezlow.GATUNEK, gatunek);
                    if (wezelGatunku != null) {
                        System.out.println("GATUNEK ISTNIEJE - " + gatunek);
                    } else {
                        wezelGatunku = dodajWezel(TypyWezlow.GATUNEK);
                        ustalWlasciwosc(wezelGatunku, "nazwa", gatunek);
                        System.out.println("GATUNEK NIE ISTNIEJE - " + gatunek);
                    }

                    ustalRelacje(wezelArtysty, wezelGatunku, TypyRelacji.NALEZY);
                }

                tx.success();
            }
        }

        return null;
    }

    private String dodajWezelUzytkownika(Uzytkownik uzytkownik) {
        TypyWezlow typWezla = TypyWezlow.UZYTKOWNIK;
        Node wezelUzytkownika = czyIstniejeWezel(typWezla, uzytkownik.getPseudonim());

        if (wezelUzytkownika != null) {
            return "Użytkownik o podanym pseudonimie już istnieje.";
        } else {
            try (Transaction tx = graphDB.beginTx()) {
                wezelUzytkownika = dodajWezel(typWezla);
                ustalWlasciwosc(wezelUzytkownika, "pseudonim", uzytkownik.getPseudonim());
                ustalWlasciwosc(wezelUzytkownika, "imie", uzytkownik.getImie());
                ustalWlasciwosc(wezelUzytkownika, "nazwisko", uzytkownik.getNazwisko());
                ustalWlasciwosc(wezelUzytkownika, "wiek", String.valueOf(uzytkownik.getWiek()));
                ustalWlasciwosc(wezelUzytkownika, "plec", uzytkownik.getPlec().dajLancuch());

                Node wezelKraju = czyIstniejeWezel(TypyWezlow.KRAJ, uzytkownik.getKrajPochodzenia());
                if (wezelKraju != null) {
                    System.out.println("KRAJ ISTNIEJE");
                } else {
                    wezelKraju = dodajWezel(TypyWezlow.KRAJ);
                    ustalWlasciwosc(wezelKraju, "nazwa", uzytkownik.getKrajPochodzenia());
                    System.out.println("KRAJ NIE ISTNIEJE");
                }

                ustalRelacje(wezelUzytkownika, wezelKraju, TypyRelacji.POCHODZI);

                tx.success();
            }
        }

        return null;
    }

    private Node dodajWezel(TypyWezlow typWezla) {
        Node node = graphDB.createNode(typWezla);
        return node;
    }

    private void ustalWlasciwosc(Node node, String typ, String nazwa) {
        if (!nazwa.isEmpty() && nazwa != null) {
            node.setProperty(typ, nazwa);
        }
    }

    private void ustalRelacje(Node node1, Node node2, TypyRelacji relacja) {
        node1.createRelationshipTo(node2, relacja);
    }
    
    private Relationship sprawdzCzyIstniejeRelacja(Node node1, Node node2, TypyRelacji relacja) {
        Relationship wynik = null;
        
        for (Relationship rel : node1.getRelationships(relacja, Direction.OUTGOING)) {
            if(rel.getEndNode().getId() == node2.getId()) {
                wynik = rel;
                break;
            }
        }
        
        return wynik;
    }

    public void usunWezel(Wezel wezel) {
        String komunikat = null;

        if (wezel instanceof Uzytkownik) {
            komunikat = usunWezelUzytkownika((Uzytkownik) wezel);
        } else if (wezel instanceof Artysta) {
            komunikat = usunWezelArtysty((Artysta) wezel);
        } else if (wezel instanceof Album) {
            komunikat = usunWezelAlbum((Album) wezel);
        }

        if (komunikat == null) {
            zdejmijFlageProblemu();
        } else {
            ustawFlageProblemu(komunikat);
        }

    }

    private String usunWezelArtysty(Artysta artysta) {
        TypyWezlow typWezla = TypyWezlow.ARTYSTA;
        Node wezelArtysty = czyIstniejeWezel(typWezla, artysta.getNazwaZespolu());

        if (wezelArtysty == null) {
            return "Artysta o podanej nazwie nie istnieje. Nie udało się go usunąć.";
        } else {
            try (Transaction tx = graphDB.beginTx()) {

                Node wezelKraju = czyIstniejeWezel(TypyWezlow.KRAJ, artysta.getKrajPochodzenia());
                if (wezelKraju != null) {
                    if (liczbaElementowWIterable(wezelKraju.getRelationships()) == 1) {
                        usunWszystkieRelacje(wezelKraju);
                        wezelKraju.delete();
                        System.out.println("Usunieto kraj - " + artysta.getKrajPochodzenia());
                    }
                }

                for (String gatunek : artysta.getListaGatunkow()) {

                    Node wezelGatunku = czyIstniejeWezel(TypyWezlow.GATUNEK, gatunek);
                    if (wezelGatunku != null) {
                        if (liczbaElementowWIterable(wezelGatunku.getRelationships()) == 1) {
                            usunWszystkieRelacje(wezelGatunku);
                            wezelGatunku.delete();
                            System.out.println("Usunieto gatunek - " + gatunek);
                        }
                    }

                }

                for (String album : artysta.getListaAlbumow()) {

                    Node wezelAlbumu = czyIstniejeWezel(TypyWezlow.ALBUM, album);
                    if (wezelAlbumu != null) {
                        usunWszystkieRelacje(wezelAlbumu);
                        wezelAlbumu.delete();
                        System.out.println("Usunieto album - " + album);
                    }

                }

                usunWszystkieRelacje(wezelArtysty);
                wezelArtysty.delete();

                tx.success();
            }
        }

        return null;
    }

    private String usunWezelUzytkownika(Uzytkownik uzytkownik) {
        TypyWezlow typWezla = TypyWezlow.UZYTKOWNIK;
        Node wezelUzytkownika = czyIstniejeWezel(typWezla, uzytkownik.getPseudonim());

        if (wezelUzytkownika == null) {
            return "Użytkownik o podanym pseudonimie nie istnieje. Nie udało się go usunąć.";
        } else {
            try (Transaction tx = graphDB.beginTx()) {

                Node wezelKraju = czyIstniejeWezel(TypyWezlow.KRAJ, uzytkownik.getKrajPochodzenia());
                if (wezelKraju != null) {
                    if (liczbaElementowWIterable(wezelKraju.getRelationships()) == 1) {
                        usunWszystkieRelacje(wezelKraju);
                        wezelKraju.delete();
                        System.out.println("Usunieto kraj");
                    }
                }

                usunWszystkieRelacje(wezelUzytkownika);
                wezelUzytkownika.delete();

                tx.success();
            }
        }

        return null;
    }

    private String usunWezelAlbum(Album album) {
        TypyWezlow typWezla = TypyWezlow.ALBUM;
        Node wezelAlbumu = czyIstniejeWezel(typWezla, album.getNazwaAlbumu());

        if (wezelAlbumu == null) {
            return "Album o podanej nazwie nie istnieje. Nie udało się go usunąć.";
        } else {
            try (Transaction tx = graphDB.beginTx()) {
                usunWszystkieRelacje(wezelAlbumu);
                wezelAlbumu.delete();

                tx.success();
            }
        }

        return null;
    }

    private void usunWszystkieRelacje(Node wezel) {
        for (Relationship relacja : wezel.getRelationships()) {
            relacja.delete();
        }
    }

    private int liczbaElementowWIterable(Iterable<?> iterable) {
        if (iterable instanceof Collection<?>) {
            return ((Collection<?>) iterable).size();
        } else {
            int liczbaElementow = 0;
            Iterator it = iterable.iterator();
            while (it.hasNext()) {
                it.next();
                liczbaElementow++;
            }

            return liczbaElementow;
        }
    }

    private Node czyIstniejeWezel(TypyWezlow typWezla, String nazwa) {
        Node wezel = null;
        String nazwaPola = zwrocNazwePolaUnikatowego(typWezla);

        try (Transaction tx = graphDB.beginTx()) {
            for (Node n : GlobalGraphOperations.at(graphDB).getAllNodesWithLabel(typWezla)) {
                String nz = (String) n.getProperty(nazwaPola);

                if (nz.equals(nazwa)) {
                    wezel = n;
                    break;
                }
            }

            tx.success();
        }

        return wezel;
    }

    private String zwrocNazwePolaUnikatowego(TypyWezlow typWezla) {
        switch (typWezla) {
            case UZYTKOWNIK:
                return "pseudonim";
            default:
                return "nazwa";
        }
    }

    public void wyswietlOknoBleduJesliPotrzeba() {
        if (ZarzadcaBazaNeo4J.CZY_WYSTAPIL_PROBLEM) {
            JOptionPane.showMessageDialog(OknoGlowne.dajInstancje(), ZarzadcaBazaNeo4J.KOMUNIKAT_OSTATNIEJ_OPERACJI, "Błąd operacji", JOptionPane.ERROR_MESSAGE);
        }
        
        zdejmijFlageProblemu();
    }
}
