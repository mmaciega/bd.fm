package pl.bdfm.logic;

import java.util.List;

/**
 *
 * @author Mateusz Macięga Piotr Gębala
 */
public class Uzytkownik extends Wezel {
    public enum Plci {
        N("Brak"), M("Mężczyzna"), K("Kobieta");
        
        private final String lancuch;

        private Plci(String lancuch) {
            this.lancuch = lancuch;
        }

        public String dajLancuch() {
            return lancuch;
        }
    }
    
    public static Plci znajdzPlec(String plec) {
        for (Plci p : Plci.values()) {
            if(p.dajLancuch().toLowerCase().equals(plec.toLowerCase())) {
                return p;
            }
        }
        return Plci.N;
    }
    
    public static String[] zwrocTablicaLancuchowPlci() {
        String[] tablica = new String[Plci.values().length];
        
        for (int i = 0; i < Plci.values().length; i++) {
            tablica[i] = Plci.values()[i].dajLancuch();
        }
        
        return tablica;
    }
    
    private String pseudonim, imie, nazwisko, krajPochodzenia;
    private int wiek;
    private Plci plec;
    
    private List<String> listaZnajomych;
    private List<String> listaUlubionych;
    
    public Uzytkownik(long ID, String pseudonim, String imie, String nazwisko, String krajPochodenia, int wiek, Plci plec, List<String> listaZnajomych, List<String> listaUlubionych) {
        super(ID);
        
        this.pseudonim = pseudonim;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.krajPochodzenia = krajPochodenia;
        this.wiek = wiek;
        this.plec = plec;
        this.listaZnajomych = listaZnajomych;
        this.listaUlubionych = listaUlubionych;
    }
    
    public Uzytkownik(long ID, String pseudonim, List<String> listaZnajomych, List<String> listaUlubionych) {
        super(ID);
        
        this.pseudonim = pseudonim;
        this.imie = "";
        this.nazwisko = "";
        this.krajPochodzenia = "";
        this.wiek = 0;
        this.plec = Plci.N;
        this.listaZnajomych = listaZnajomych;
        this.listaUlubionych = listaUlubionych;
    }
    
    public Uzytkownik(String pseudonim, String imie, String nazwisko, String krajPochodenia, int wiek, Plci plec) {
        super(-1);
        
        this.pseudonim = pseudonim;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.krajPochodzenia = krajPochodenia;
        this.wiek = wiek;
        this.plec = plec;
        this.listaZnajomych = null;
        this.listaUlubionych = null;
    }

    /**
     * @return the pseudonim
     */
    public String getPseudonim() {
        return pseudonim;
    }

    /**
     * @param pseudonim the pseudonim to set
     */
    public void setPseudonim(String pseudonim) {
        this.pseudonim = pseudonim;
    }

    /**
     * @return the imie
     */
    public String getImie() {
        return imie;
    }

    /**
     * @param imie the imie to set
     */
    public void setImie(String imie) {
        this.imie = imie;
    }

    /**
     * @return the nazwisko
     */
    public String getNazwisko() {
        return nazwisko;
    }

    /**
     * @param nazwisko the nazwisko to set
     */
    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    /**
     * @return the krajPochodzenia
     */
    public String getKrajPochodzenia() {
        return krajPochodzenia;
    }

    /**
     * @param krajPochodzenia the krajPochodzenia to set
     */
    public void setKrajPochodzenia(String krajPochodzenia) {
        this.krajPochodzenia = krajPochodzenia;
    }

    /**
     * @return the wiek
     */
    public int getWiek() {
        return wiek;
    }

    /**
     * @param wiek the wiek to set
     */
    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    /**
     * @return the plec
     */
    public Plci getPlec() {
        return plec;
    }

    /**
     * @param plec the plec to set
     */
    public void setPlec(Plci plec) {
        this.plec = plec;
    }

    /**
     * @return the listaZnajomych
     */
    public List<String> getListaZnajomych() {
        return listaZnajomych;
    }

    /**
     * @return the listaUlubionych
     */
    public List<String> getListaUlubionych() {
        return listaUlubionych;
    }
    
    @Override
    public String toString() {
        return (getPseudonim() + ", " + getImie() + ", " + getNazwisko() + ", " + getWiek() + " lat, " + getKrajPochodzenia() + ", " + getPlec().dajLancuch());
        
    }
    
}
