/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.bdfm.logic;

import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import pl.bdfm.gfx.OknoGlowne;
import pl.bdfm.gfx.PanelAlbumu;
import pl.bdfm.gfx.PanelArtysty;
import pl.bdfm.gfx.PanelUzytkownika;
import pl.bdfm.gfx.paneleMenu.PanelDodajAlbum;
import pl.bdfm.gfx.paneleMenu.PanelDodajPodobienstwo;
import pl.bdfm.gfx.paneleMenu.PanelDodajPolubienie;
import pl.bdfm.gfx.paneleMenu.PanelDodajZnajomosc;
import pl.bdfm.gfx.paneleMenu.PanelUsunAlbum;
import pl.bdfm.gfx.paneleMenu.PanelUsunPodobienstwo;
import pl.bdfm.gfx.paneleMenu.PanelUsunPolubienie;
import pl.bdfm.gfx.paneleMenu.PanelUsunZnajomosc;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class ZarzadcaLinkami implements MouseListener {
    
    private ZarzadcaLinkami() {
    }
    
    public static ZarzadcaLinkami dajInstancje() {
        return ZarzadcaLinkamiHolder.INSTANCE;
    }

    private static class ZarzadcaLinkamiHolder {

        private static final ZarzadcaLinkami INSTANCE = new ZarzadcaLinkami();
    }
    
    @Override
    public void mouseEntered(MouseEvent me) {
        JComponent element = (JComponent) me.getSource();
        element.setFont(element.getFont().deriveFont(Font.BOLD));
    }

    @Override
    public void mouseExited(MouseEvent me) {
        final JComponent zrodlo = (JComponent) me.getSource();
        zrodlo.setFont(zrodlo.getFont().deriveFont(Font.PLAIN));
    }

    /**
     *
     * @param me
     */
    @Override
    public void mouseClicked(MouseEvent me) {
        final JComponent zrodlo = (JComponent) me.getSource();
        final ZarzadcaBazaNeo4J.TypyWezlow typWezla = (ZarzadcaBazaNeo4J.TypyWezlow) zrodlo.getClientProperty("typWezla");
        final String nazwa = (String) zrodlo.getClientProperty("nazwa");
        
        String nazwaOkna = "";
        PanelArtysty panelArtysty;
        PanelUzytkownika panelUzytkownika;
        final String typOperacji;
        switch(typWezla) {
            case ARTYSTA:
                typOperacji = (String) zrodlo.getClientProperty("typOperacji");

                if ((typOperacji != null) && (typOperacji.equals("dodajPodobienstwo"))) {
                    panelArtysty = (PanelArtysty) OknoGlowne.dajInstancje().getContentPane();

                    PanelDodajPodobienstwo panelDodajPodobienstwo = new PanelDodajPodobienstwo(panelArtysty.getArtysta().getNazwaZespolu());
                    panelDodajPodobienstwo.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelDodajPodobienstwo.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                } else if ( (typOperacji != null) && (typOperacji.equals("dodajPolubienie")) ) {
                    panelUzytkownika = (PanelUzytkownika) OknoGlowne.dajInstancje().getContentPane();

                    PanelDodajPolubienie panelDodajPolubienie = new PanelDodajPolubienie(panelUzytkownika.getUzytkownik().getPseudonim());
                    panelDodajPolubienie.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelDodajPolubienie.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                } else if ( (typOperacji != null) && (typOperacji.equals("usunPodobienstwo")) ) {
                    System.out.println("Usun podobienstwo");
                    
                    panelArtysty = (PanelArtysty) OknoGlowne.dajInstancje().getContentPane();

                    PanelUsunPodobienstwo panelUsunPodobienstwo = new PanelUsunPodobienstwo(panelArtysty.getArtysta().getNazwaZespolu());
                    panelUsunPodobienstwo.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelUsunPodobienstwo.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                
                } else if ( (typOperacji != null) && (typOperacji.equals("usunPolubienie")) ) {
                    
                    System.out.println("Usun polubienie");
                    
                    panelUzytkownika = (PanelUzytkownika) OknoGlowne.dajInstancje().getContentPane();
                    PanelUsunPolubienie panelUsunPolubienia = new PanelUsunPolubienie(panelUzytkownika.getUzytkownik().getPseudonim());
                    panelUsunPolubienia.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelUsunPolubienia.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba(); 
                
                } else {
                        
                    Artysta artysta = (Artysta) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(typWezla, nazwa);
                    panelArtysty = new PanelArtysty(artysta);

                    OknoGlowne.dajInstancje().zmienPanelGlowny(panelArtysty);

                    nazwaOkna = " - Artysta: " + artysta.getNazwaZespolu();
                    OknoGlowne.dajInstancje().setTitle(OknoGlowne.NAZWA_APLIKACJI + nazwaOkna);
                    
                }
                break;
            case UZYTKOWNIK:
                typOperacji = (String) zrodlo.getClientProperty("typOperacji");

                 if ((typOperacji != null) && (typOperacji.equals("dodajZnajomego"))) {
                    panelUzytkownika = (PanelUzytkownika) OknoGlowne.dajInstancje().getContentPane();

                    PanelDodajZnajomosc panelDodajZnajomosc = new PanelDodajZnajomosc(panelUzytkownika.getUzytkownik().getPseudonim());
                    panelDodajZnajomosc.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelDodajZnajomosc.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                 } else if ((typOperacji != null) && (typOperacji.equals("usunZnajomego"))) {
                     
                    panelUzytkownika = (PanelUzytkownika) OknoGlowne.dajInstancje().getContentPane();

                    PanelUsunZnajomosc panelUsunZnajomosc = new PanelUsunZnajomosc(panelUzytkownika.getUzytkownik().getPseudonim());
                    panelUsunZnajomosc.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelUsunZnajomosc.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                 
                 } else              
                 {
            
                    Uzytkownik uzytkownik = (Uzytkownik) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(typWezla, nazwa);
                    panelUzytkownika = new PanelUzytkownika(uzytkownik);

                    OknoGlowne.dajInstancje().zmienPanelGlowny(panelUzytkownika);

                    nazwaOkna = " - Użytkownik: " + uzytkownik.getPseudonim();
                    OknoGlowne.dajInstancje().setTitle(OknoGlowne.NAZWA_APLIKACJI + nazwaOkna);
                 
                 }
                break;
            case ALBUM:
                 typOperacji = (String) zrodlo.getClientProperty("typOperacji");
                
                if ((typOperacji != null) && (typOperacji.equals("dodaj"))) {
                        System.out.println("Dodaj");
                        
                        panelArtysty = (PanelArtysty) OknoGlowne.dajInstancje().getContentPane();
                        
                        PanelDodajAlbum panelDodajAlbum = new PanelDodajAlbum(panelArtysty.getArtysta().getNazwaZespolu());
                        panelDodajAlbum.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                        panelDodajAlbum.wyswietlOknoDialogowe();
                        ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                    
                } else if ((typOperacji != null) && (typOperacji.equals("usunAlbum"))) {
                        System.out.println("Usun album");
                        
                    panelArtysty = (PanelArtysty) OknoGlowne.dajInstancje().getContentPane();
                    PanelUsunAlbum panelUsunAlbum = new PanelUsunAlbum(panelArtysty.getArtysta().getNazwaZespolu());
                    panelUsunAlbum.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelUsunAlbum.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                    
                } else {
                
                    Album album = (Album) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(typWezla, nazwa);

                    JOptionPane.showOptionDialog(OknoGlowne.dajInstancje(), 
                            new PanelAlbumu(album), "Informacje o albumie muzycznym", 
                            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, 
                            null, new String[]{"Zamknij"}, "Zamknij");
                }
                break;
        }
        
        
    }
    
//<editor-fold defaultstate="collapsed" desc="zdarzenia nieobsłużone">
    @Override
    public void mousePressed(MouseEvent me) {
    }
    
    @Override
    public void mouseReleased(MouseEvent me) {
    }
//</editor-fold>
}
