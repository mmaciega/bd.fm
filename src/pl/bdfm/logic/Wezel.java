/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.bdfm.logic;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public abstract class Wezel {
    private long ID;
    
    public Wezel(long ID) {
        this.ID = ID;
    }

    /**
     * @return the ID
     */
    public long getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(long ID) {
        this.ID = ID;
    }
    
    
}
