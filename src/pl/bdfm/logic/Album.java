/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.bdfm.logic;

import java.util.List;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class Album extends Wezel {
    private String nazwaAlbumu;
    private List<String> nazwaArtystow;
    private String czasTrwania;
    private int rokWydania;
    private int liczbaUtworow;
    
    public Album(long ID, String nazwaAlbumu, List<String> nazwaArtystow, String czasTrwania, int rokWydania, int liczbaUtworow) {
        super(ID);
        this.nazwaAlbumu = nazwaAlbumu;
        this.nazwaArtystow = nazwaArtystow;
        this.czasTrwania = czasTrwania;
        this.rokWydania = rokWydania;
        this.liczbaUtworow = liczbaUtworow;
    }
    
    public Album(String nazwaAlbumu, List<String> nazwaArtystow, String czasTrwania, int rokWydania, int liczbaUtworow) {
        super(-1);
        this.nazwaAlbumu = nazwaAlbumu;
        this.nazwaArtystow = nazwaArtystow;
        this.czasTrwania = czasTrwania;
        this.rokWydania = rokWydania;
        this.liczbaUtworow = liczbaUtworow;
    }

    /**
     * @return the czasTrwania
     */
    public String getCzasTrwania() {
        return czasTrwania;
    }

    /**
     * @return the rokWydania
     */
    public int getRokWydania() {
        return rokWydania;
    }

    /**
     * @return the liczbaUtworow
     */
    public int getLiczbaUtworow() {
        return liczbaUtworow;
    }

    /**
     * @return the nazwaAlbumu
     */
    public String getNazwaAlbumu() {
        return nazwaAlbumu;
    }

    /**
     * @return the nazwaArtystow
     */
    public List<String> getNazwaArtystow() {
        return nazwaArtystow;
    }
    
    @Override
    public String toString() {
        String wykonawcy = "";
        for (int i = 0; i < getNazwaArtystow().size()-1; i++) {
            wykonawcy += getNazwaArtystow().get(i) + ": ";
        }
        wykonawcy += getNazwaArtystow().get(getNazwaArtystow().size()-1);
        
        return (getNazwaAlbumu() + ", " + wykonawcy + ", " + getCzasTrwania()+ ", " + String.valueOf(getLiczbaUtworow()) + " utworow, " + String.valueOf(getRokWydania()));
        
    }
}
