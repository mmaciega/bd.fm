
package pl.bdfm.logic;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public interface IObserwowany {
    public enum TypyPowiadomienia {
        DODANIE, USUNIECIE, EDYCJA;
    }
    
    /**
     * Metoda wirtualna odpowiedzialna za zarejestrowanie nowego obserwatora
     * @param obserwator Obiekt będący obserwatorem
     */
    void zarejestrujObserwatora(IObserwator obserwator);
    /**
     * Metoda wirtualna odpowiedzialna za wyrejestrowanie obserwatora
     * @param obserwator Obiekt będący obserwatorem
     */
    void wyrejestrujObserwatora(IObserwator obserwator);
    /**
     * Metoda odpowiedzialna za powiadomienie wszystkich obserwatorów o zajściu zdarzenia
     */
    void powiadomObserwatorow();
}
