
package pl.bdfm.logic;

import org.neo4j.graphdb.Direction;
import pl.bdfm.logic.IObserwowany.TypyPowiadomienia;
import pl.bdfm.logic.ZarzadcaBazaNeo4J.TypyRelacji;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public interface IObserwator {
    void aktualizacjaWezla(TypyPowiadomienia typPowiadomienia, Wezel wezel);
    
    void aktualizacjaRelacji(TypyPowiadomienia typPowiadomienia, TypyRelacji typRelacji, Wezel wezel1, Wezel wezel2, Direction kierunek);
}
