/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bdfm.logic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import pl.bdfm.gfx.OknoGlowne;
import pl.bdfm.gfx.PanelSpis;
import pl.bdfm.gfx.components.JSearchTextField;
import pl.bdfm.gfx.paneleMenu.PanelDodajAlbum;
import pl.bdfm.gfx.paneleMenu.PanelDodajArtyste;
import pl.bdfm.gfx.paneleMenu.PanelDodajPodobienstwo;
import pl.bdfm.gfx.paneleMenu.PanelDodajPolubienie;
import pl.bdfm.gfx.paneleMenu.PanelDodajUzytkownika;
import pl.bdfm.gfx.paneleMenu.PanelDodajZnajomosc;
import pl.bdfm.gfx.paneleMenu.PanelOknaDialogowego;
import pl.bdfm.gfx.paneleMenu.PanelUsunAlbum;
import pl.bdfm.gfx.paneleMenu.PanelUsunUzytkownika;
import pl.bdfm.gfx.paneleMenu.PanelUsunArtyste;
import pl.bdfm.gfx.paneleMenu.PanelUsunPodobienstwo;
import pl.bdfm.gfx.paneleMenu.PanelUsunPolubienie;
import pl.bdfm.gfx.paneleMenu.PanelUsunZnajomosc;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class ZarzadzcaMenu implements ActionListener {

    private ZarzadzcaMenu() {}

    public static ZarzadzcaMenu dajInstancje() {
        return ZarzadzanieMenuHolder.INSTANCE;
    }
    
    private static class ZarzadzanieMenuHolder {

        private static final ZarzadzcaMenu INSTANCE = new ZarzadzcaMenu();
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        Object obiekt = ae.getSource();

        if (obiekt instanceof JMenuItem) {
            JMenuItem elementMenu = (JMenuItem) obiekt;
            JFrame oknoGlowne = (JFrame) elementMenu.getClientProperty("okno");
            String tekstPrzycisku = (String) elementMenu.getClientProperty("tekst");
            
            int odpowiedz;
            switch (tekstPrzycisku) {
                case "zakoncz":
                    
                    odpowiedz = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz wyłączyć program?", "Zakończ", JOptionPane.YES_NO_OPTION);
                    if (odpowiedz == JOptionPane.YES_OPTION) {
                        oknoGlowne.dispatchEvent(new WindowEvent(oknoGlowne, WindowEvent.WINDOW_CLOSING));
                    }  
                    break;
                case "oprogramie":
                    
                    JOptionPane.showMessageDialog(oknoGlowne, "Projekt\nBazyDanych.FM\n\nAutorzy: Mateusz Macięga, Piotr Gębala\nRok: 2014\nWersja: 1.0", "O programie", JOptionPane.INFORMATION_MESSAGE);
                    break;
                case "spis":
                    
                    ((OknoGlowne) oknoGlowne).zmienPanelGlowny(new PanelSpis());
                    OknoGlowne.dajInstancje().setTitle(OknoGlowne.NAZWA_APLIKACJI + " - Spis wszystkich użytkowników i artystów");
                    break;
                case "dodajUzytkownika":
                    
                    PanelOknaDialogowego panelDodajUzytkownika = new PanelDodajUzytkownika();
                    panelDodajUzytkownika.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});
                        
                    odpowiedz = JOptionPane.showConfirmDialog(oknoGlowne, panelDodajUzytkownika, "Dodaj użytkownika" , JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                    if (odpowiedz == JOptionPane.YES_OPTION) {
                        ((IObserwowany) panelDodajUzytkownika).powiadomObserwatorow();
                    }
                    
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                    break;
                    
                case "dodajArtyste": 
                    PanelDodajArtyste panelDodajArtyste = new PanelDodajArtyste();
                    panelDodajArtyste.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});
                    
                    panelDodajArtyste.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                    break;
                    
                case "dodajAlbum":
                    PanelDodajAlbum panelDodajAlbum = new PanelDodajAlbum();
                    panelDodajAlbum.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelDodajAlbum.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                    break;
                    
                case "usunUzytkownika":
                    PanelUsunUzytkownika panelUsunUzytkownika = new PanelUsunUzytkownika();
                    panelUsunUzytkownika.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelUsunUzytkownika.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                    break;
                    
                case "usunArtyste":
                    PanelUsunArtyste panelUsunArtyste = new PanelUsunArtyste();
                    panelUsunArtyste.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelUsunArtyste.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                    break;
                    
                case "usunAlbum":
                    PanelUsunAlbum panelUsunAlbum = new PanelUsunAlbum();
                    panelUsunAlbum.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelUsunAlbum.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                    break;
                case "dodajZnajomosc":
                    PanelDodajZnajomosc panelDodajZnajomosc = new PanelDodajZnajomosc();
                    panelDodajZnajomosc.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelDodajZnajomosc.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                    break;
                case "dodajPodobienstwo":
                    PanelDodajPodobienstwo panelDodajPodobienstwo = new PanelDodajPodobienstwo();
                    panelDodajPodobienstwo.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelDodajPodobienstwo.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                    break;
                case "dodajPolubienie":
                    PanelDodajPolubienie panelDodajPolubienie = new PanelDodajPolubienie();
                    panelDodajPolubienie.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelDodajPolubienie.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                    break;
                case "usunZnajomosc":
                    PanelUsunZnajomosc panelUsunZnajomosc = new PanelUsunZnajomosc();
                    panelUsunZnajomosc.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelUsunZnajomosc.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba();
                    break;
                case "usunPodobienstwo":
                    PanelUsunPodobienstwo panelUsunPodobienstwo = new PanelUsunPodobienstwo();
                    panelUsunPodobienstwo.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelUsunPodobienstwo.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba(); 
                    break;
                case "usunPolubienie":
                    PanelUsunPolubienie panelUsunPolubienia = new PanelUsunPolubienie();
                    panelUsunPolubienia.zarejestrujObserwatorow(new Object[]{ZarzadcaBazaNeo4J.dajInstancje(), OknoGlowne.dajInstancje().getContentPane()});

                    panelUsunPolubienia.wyswietlOknoDialogowe();
                    ZarzadcaBazaNeo4J.dajInstancje().wyswietlOknoBleduJesliPotrzeba(); 
                    break;
            }
        } else if (obiekt instanceof JSearchTextField) {
            
            JSearchTextField elementSzukaj = (JSearchTextField) obiekt;
            JFrame oknoGlowne = (JFrame) elementSzukaj.getClientProperty("okno");
            JOptionPane.showMessageDialog(null, "Szukam");
            
            //((OknoGlowne) oknoGlowne).zmienPanelGlowny(new PanelUzytkownika());
        }
    }
    
//    void zarejestrujObserwatorow(PanelOknaDialogowego panel, Object[] obserwatorzy) {
//        for (Object object : obserwatorzy) {
//            if(object instanceof IObserwator) {
//                panel.zarejestrujObserwatora((IObserwator) object);
//            }   
//        }
//    }
    
//    void wyswietlOknoBleduJesliPotrzeba() {
//        if (ZarzadcaBazaNeo4J.CZY_WYSTAPIL_PROBLEM) {
//            JOptionPane.showMessageDialog(OknoGlowne.dajInstancje(), ZarzadcaBazaNeo4J.KOMUNIKAT_OSTATNIEJ_OPERACJI, "Błąd operacji" , JOptionPane.ERROR_MESSAGE);
//        }
//    }
}
