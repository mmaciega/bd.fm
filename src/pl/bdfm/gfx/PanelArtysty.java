/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.bdfm.gfx;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Stroke;
import java.util.List;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import org.neo4j.graphdb.Direction;
import pl.bdfm.gfx.components.ScrollablePanel;
import pl.bdfm.logic.Album;
import pl.bdfm.logic.Artysta;
import pl.bdfm.logic.IObserwator;
import pl.bdfm.logic.IObserwowany;
import pl.bdfm.logic.Wezel;
import pl.bdfm.logic.ZarzadcaBazaNeo4J;
import pl.bdfm.logic.ZarzadcaKursorem;
import pl.bdfm.logic.ZarzadcaLinkami;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelArtysty extends JPanel implements IObserwator  {
    private final JPanel panelZDanymi;
    private final JPanel panelZPowiazaniami;
    private final Artysta artysta;
    
    
    public PanelArtysty(Artysta artysta) {
        setLayout(new BorderLayout());
        setBackground(Color.white);
        
        this.artysta = artysta;
        
        panelZDanymi = new PanelZDanymi(artysta);
        panelZPowiazaniami = new PanelZPowiazaniami(artysta.getListaAlbumow(), artysta.getListaPodobnych());
        
        add(panelZDanymi, BorderLayout.NORTH);
        add(panelZPowiazaniami, BorderLayout.CENTER);
    }
    
    public Artysta getArtysta() {
        return artysta;
    }

    @Override
    public void aktualizacjaWezla(IObserwowany.TypyPowiadomienia typPowiadomienia, Wezel wezel) {
        ZarzadcaBazaNeo4J.TypyWezlow typWezla = null;
        if (wezel instanceof Album) {
            typWezla = ZarzadcaBazaNeo4J.TypyWezlow.ALBUM;
        } else if (wezel instanceof Artysta) {
            typWezla = ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA;
        }

        if(typWezla != null) {
            switch(typPowiadomienia) {
                case DODANIE:
                    if(typWezla == ZarzadcaBazaNeo4J.TypyWezlow.ALBUM)
                        ((PanelZPowiazaniami) panelZPowiazaniami).dodajNowyElement(typWezla, wezel, ((PanelZDanymi) panelZDanymi).getArtysta());
                    break;
                case USUNIECIE:
                    ((PanelZPowiazaniami) panelZPowiazaniami).usunElement(typWezla, wezel, ((PanelZDanymi) panelZDanymi).getArtysta());
                    break;
                default:
                    System.out.println("Test");
                    break;
            }
        }
    }

    @Override
    public void aktualizacjaRelacji(IObserwowany.TypyPowiadomienia typPowiadomienia, ZarzadcaBazaNeo4J.TypyRelacji typRelacji, Wezel wezel1, Wezel wezel2, Direction kierunek) {
        Wezel wezelDodany = null;
        if(wezel1 instanceof Artysta && ((Artysta)wezel1).getNazwaZespolu().equals(artysta.getNazwaZespolu())  ) {
            wezelDodany = wezel2;
        } else if(wezel2 instanceof Artysta && ((Artysta)wezel2).getNazwaZespolu().equals(artysta.getNazwaZespolu())  ) {
            wezelDodany = wezel1;
        }
     
        if(wezelDodany != null ) {
            ZarzadcaBazaNeo4J.TypyWezlow typWezlaDodanego = null;
            if (wezelDodany instanceof Artysta) {
                typWezlaDodanego = ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA;
            }
        
            if(typWezlaDodanego != null) {
                switch(typPowiadomienia) {
                    case DODANIE:
                        ((PanelZPowiazaniami) panelZPowiazaniami).dodajNowyElement(typWezlaDodanego, wezelDodany, artysta);
                        break;
                    case USUNIECIE:
                        ((PanelZPowiazaniami) panelZPowiazaniami).usunElement(typWezlaDodanego, wezelDodany, artysta);
                        break;
                }
            }
        }
    }
    
    class PanelZDanymi extends JPanel {
        private final Artysta artysta;
        
        public PanelZDanymi(Artysta artysta) {
            this.artysta = artysta;
            setOpaque(false);
            
            setLayout(new GridLayout(1, 2));

            add(wygenerujPanelZDanymi());
            add(wygenerujPanelZOpisem());
        }
        
        public Artysta getArtysta() {
            return artysta;
        }
        
        private JPanel wygenerujPanelZDanymi() {
            JPanel panel = new JPanel();
            panel.setOpaque(false);
            panel.setLayout(new BorderLayout());
            
            JPanel panelD = new JPanel();
            panelD.setOpaque(false);
            panelD.setLayout(new BoxLayout(panelD, BoxLayout.Y_AXIS));
            panelD.setBorder(new EmptyBorder(15, 20, 30, 20));
            
            JTextArea nazwaZespolu=new JTextArea(artysta.getNazwaZespolu());
            nazwaZespolu.setEditable(false);
            nazwaZespolu.setBackground(null);
            nazwaZespolu.setBorder(null);
            nazwaZespolu.setOpaque(false);
            nazwaZespolu.setFont(new Font("Verdana", Font.BOLD, 25));
            nazwaZespolu.setForeground(Color.black);
            nazwaZespolu.setDisabledTextColor(Color.black);
            nazwaZespolu.setWrapStyleWord(true);
            nazwaZespolu.setLineWrap(true);

            JPanel panelZGatunkami = new JPanel(new WrapLayout(FlowLayout.LEFT,0,0));
            panelZGatunkami.setOpaque(false);
            
            for (int i = 0; i < artysta.getListaGatunkow().size()-1; i++) {
                JLabel etykietaGatunek =new JLabel();
                etykietaGatunek.setBackground(null);
                etykietaGatunek.setOpaque(false);
                etykietaGatunek.setFont(new Font("Verdana", Font.PLAIN, 12));
                etykietaGatunek.setForeground(Color.black);
//                etykietaGatunek.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
//                etykietaGatunek.addMouseListener(ZarzadcaLinkami.dajInstancje());
                
                etykietaGatunek.setText(artysta.getListaGatunkow().get(i));
                
                panelZGatunkami.add(etykietaGatunek);
                panelZGatunkami.add(new JLabel(", "));
            }
            
            JLabel etykietaGatunek = new JLabel();
            etykietaGatunek.setBackground(null);
            etykietaGatunek.setOpaque(false);
            etykietaGatunek.setFont(new Font("Verdana", Font.PLAIN, 12));
            etykietaGatunek.setForeground(Color.black);
            etykietaGatunek.setText(artysta.getListaGatunkow().get(artysta.getListaGatunkow().size()-1));
//            etykietaGatunek.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
//            etykietaGatunek.addMouseListener(ZarzadcaLinkami.dajInstancje());
            
            panelZGatunkami.add(etykietaGatunek);

         
            JTextField krajPochodzenia=new JTextField(artysta.getKrajPochodzenia().toUpperCase());
            krajPochodzenia.setEditable(false);
            krajPochodzenia.setBackground(null);
            krajPochodzenia.setBorder(null);
            krajPochodzenia.setOpaque(false);
            krajPochodzenia.setFont(new Font("Verdana", Font.PLAIN, 12));
            krajPochodzenia.setForeground(Color.black);
            krajPochodzenia.setDisabledTextColor(Color.black);
        
            panelD.add(nazwaZespolu);
            panelD.add(Box.createRigidArea(new Dimension(0,5)));
            panelD.add(panelZGatunkami);
            panelD.add(Box.createRigidArea(new Dimension(0,5)));
            panelD.add(krajPochodzenia);
            
            panel.add(panelD, BorderLayout.CENTER);
            
            return panel;
        }
        
        private JPanel wygenerujPanelZOpisem() {
            JPanel panel = new JPanel() {
                @Override
                public void paintComponent(Graphics g) {
                    super.paintComponent(g);
                    Graphics2D g2d = (Graphics2D) g;

                    int rozmiarKropkowania = 1;

                    Stroke dashed = new BasicStroke(rozmiarKropkowania, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{1}, 0);

                    g2d.setStroke(dashed);
                    g2d.setColor(Color.GRAY);
                    g2d.drawLine(2, 0, 2, (int) getHeight() - (rozmiarKropkowania+2));
                }
            };
            
            panel.setOpaque(false);
            panel.setLayout(new BorderLayout());
            panel.setBorder(new EmptyBorder(15, 30, 15, 10));
            
            JPanel panelD = new JPanel();
            panelD.setOpaque(false);
            panelD.setLayout(new BoxLayout(panelD, BoxLayout.Y_AXIS));
            
            JLabel etykietaOpis = new JLabel("Opis artysty: ");
            etykietaOpis.setOpaque(false);
            etykietaOpis.setFont(new Font("Verdana", Font.PLAIN, 12));
            etykietaOpis.setForeground(Color.black);
            
            JTextArea opisTekst = new JTextArea(4,10);
            opisTekst.setText(artysta.getOpis());
            opisTekst.setEditable(false);
            opisTekst.setBackground(Color.white);
            opisTekst.setBorder(null);
            opisTekst.setFont(new Font("Verdana", Font.PLAIN, 12));
            opisTekst.setForeground(Color.black);
            opisTekst.setWrapStyleWord(true);
            opisTekst.setLineWrap(true);
            opisTekst.setAlignmentX(LEFT_ALIGNMENT);
            opisTekst.setCaretPosition(0);
                  
            JScrollPane panelZOpisem = new JScrollPane(opisTekst);
            panelZOpisem.setBorder(new LineBorder(Color.gray));
            panelZOpisem.setAlignmentX(LEFT_ALIGNMENT);
            
            
            panelD.add(etykietaOpis);
            panelD.add(panelZOpisem); 

            panel.add(panelD);
            
            return panel;
        }
        
        
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            
            int rozmiarKropkowania = 1;
            
            Stroke dashed = new BasicStroke(rozmiarKropkowania, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{1}, 0);
            
            g2d.setStroke(dashed);
            g2d.setColor(Color.GRAY);
            g2d.drawLine(0, (int) getHeight()-(rozmiarKropkowania+2), (int) getWidth(), (int) getHeight()-(rozmiarKropkowania+2));
        }
    }
    
    class PanelZPowiazaniami extends JPanel {
        private static final String NAGLOWEK_PO_LEWEJ_STRONIE = "Albumy";
        private static final String NAGLOWEK_PO_PRAWEJ_STRONIE = "Podobni";
        private static final int MARGINES_POD_NAGLOWKAMI = 20;
        
        private final List<String> listaAlbumow;
        private final List<String> listaPodobnych;
        
        private final JPanel panelZListaAlbumow;
        private final JPanel panelZListaPodobnych;
        
        private final JLabel naglowekLewo;
        private final JLabel naglowekPrawo;
        
        public PanelZPowiazaniami(List<String> listaAlbumow, List<String> listaPodobnych) {
            super();
            this.listaAlbumow = listaAlbumow;
            this.listaPodobnych = listaPodobnych;
            
            setBorder(new EmptyBorder(20, 0, 0, 0));
            setOpaque(false);
            setLayout(new BorderLayout());
            
            JPanel panelZNaglowkami = new JPanel();
            panelZNaglowkami.setLayout(new GridLayout(1, 2));
            panelZNaglowkami.setBorder(new EmptyBorder(0, 25, MARGINES_POD_NAGLOWKAMI, 0));
            panelZNaglowkami.setOpaque(false);
             
            JPanel panelZLewymNaglowkiem = new JPanel(new FlowLayout(FlowLayout.LEFT,0,0));
            naglowekLewo = new JLabel(NAGLOWEK_PO_LEWEJ_STRONIE + " (" + listaAlbumow.size() + ")");
            naglowekLewo.setFont(new Font("Verdana", Font.BOLD, 22));
            naglowekLewo.setForeground(Color.red);
            
            JPanel panelTest2 = new JPanel();
            panelTest2.setLayout(new BoxLayout(panelTest2, BoxLayout.Y_AXIS));
            
            JLabel dodajAlbum = new JLabel("+ dodaj");
            dodajAlbum.setFont(new Font("Verdana", Font.PLAIN, 10));
            dodajAlbum.setForeground(Color.GRAY);
            dodajAlbum.setBorder(new EmptyBorder(0,10,0,0));
            dodajAlbum.setAlignmentY(CENTER_ALIGNMENT);
            dodajAlbum.putClientProperty("typWezla", ZarzadcaBazaNeo4J.TypyWezlow.ALBUM);
            dodajAlbum.putClientProperty("typOperacji", "dodaj");
            
            dodajAlbum.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
            dodajAlbum.addMouseListener(ZarzadcaLinkami.dajInstancje());
            
            JLabel usunAlbum = new JLabel("- usun");
            usunAlbum.setFont(new Font("Verdana", Font.PLAIN, 10));
            usunAlbum.setForeground(Color.GRAY);
            usunAlbum.setBorder(new EmptyBorder(0,10,0,0));
            usunAlbum.setAlignmentY(CENTER_ALIGNMENT);
            usunAlbum.putClientProperty("typWezla", ZarzadcaBazaNeo4J.TypyWezlow.ALBUM);
            usunAlbum.putClientProperty("typOperacji", "usunAlbum");
            
            usunAlbum.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
            usunAlbum.addMouseListener(ZarzadcaLinkami.dajInstancje());
            
            panelTest2.add(dodajAlbum);
            panelTest2.add(Box.createVerticalBox());
            panelTest2.add(usunAlbum);
            
//            dodajAlbum.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
//            dodajAlbum.addMouseListener(this);
           
            panelZLewymNaglowkiem.add(naglowekLewo);
            panelZLewymNaglowkiem.add(panelTest2);
            
            JPanel panelZPrawymNaglowkiem = new JPanel(new FlowLayout(FlowLayout.LEFT,0,0));
            naglowekPrawo = new JLabel(NAGLOWEK_PO_PRAWEJ_STRONIE + " (" + listaPodobnych.size() + ")");
            naglowekPrawo.setFont(new Font("Verdana", Font.BOLD, 22));
            naglowekPrawo.setForeground(Color.red);
            
            JPanel panelTest = new JPanel();
            panelTest.setLayout(new BoxLayout(panelTest, BoxLayout.Y_AXIS));
            
            JLabel dodajPodobienstwo = new JLabel("+ dodaj");
            dodajPodobienstwo.setFont(new Font("Verdana", Font.PLAIN, 10));
            dodajPodobienstwo.setForeground(Color.GRAY);
            dodajPodobienstwo.setBorder(new EmptyBorder(0,10,0,0));
            dodajPodobienstwo.setAlignmentY(CENTER_ALIGNMENT);
            dodajPodobienstwo.putClientProperty("typWezla", ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);
            dodajPodobienstwo.putClientProperty("typOperacji", "dodajPodobienstwo");
            
            dodajPodobienstwo.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
            dodajPodobienstwo.addMouseListener(ZarzadcaLinkami.dajInstancje());
            
            JLabel usunPodobienstwo = new JLabel("- usun");
            usunPodobienstwo.setFont(new Font("Verdana", Font.PLAIN, 10));
            usunPodobienstwo.setForeground(Color.GRAY);
            usunPodobienstwo.setBorder(new EmptyBorder(0,10,0,0));
            usunPodobienstwo.setAlignmentY(CENTER_ALIGNMENT);
            usunPodobienstwo.putClientProperty("typWezla", ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);
            usunPodobienstwo.putClientProperty("typOperacji", "usunPodobienstwo");
            
            usunPodobienstwo.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
            usunPodobienstwo.addMouseListener(ZarzadcaLinkami.dajInstancje());
            

            panelTest.add(dodajPodobienstwo);
            panelTest.add(Box.createVerticalBox());
            panelTest.add(usunPodobienstwo);
            
            panelZPrawymNaglowkiem.add(naglowekPrawo);
            panelZPrawymNaglowkiem.add(panelTest);

            panelZNaglowkami.add(panelZLewymNaglowkiem);
            panelZNaglowkami.add(panelZPrawymNaglowkiem);
            
            ScrollablePanel panel = new ScrollablePanel(new GridLayout(1 ,2));
            panel.setScrollableWidth( ScrollablePanel.ScrollableSizeHint.FIT );
            panel.setBackground(Color.white);
            panel.setBorder(new EmptyBorder(10, 25, 0, 0));
            
            panelZListaAlbumow = MetodyPanelowe.wygnerujPanelZLista(this.listaAlbumow, ZarzadcaBazaNeo4J.TypyWezlow.ALBUM);
            panelZListaPodobnych = MetodyPanelowe.wygnerujPanelZLista(this.listaPodobnych, ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);
            
            panel.add(panelZListaAlbumow);
            panel.add(panelZListaPodobnych);

            final JScrollPane panelZeScrollem = new JScrollPane(panel);
            panelZeScrollem.setBorder(null);
            panelZeScrollem.getViewport().setBackground(Color.white);
            
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    panelZeScrollem.getViewport().setViewPosition(new java.awt.Point(0, 0));
                }
            });
            
            add(panelZNaglowkami, BorderLayout.NORTH);
            add(panelZeScrollem, BorderLayout.CENTER);
        }
        
        private void dodajLink(JPanel panel, String tekst, ZarzadcaBazaNeo4J.TypyWezlow typWezla) {          
            JTextArea tekst1=new JTextArea(tekst);
            tekst1.setEditable(false);
            tekst1.setBackground(null);
            tekst1.setBorder(null);
            tekst1.setOpaque(false);
            tekst1.setFont(new Font("Verdana", Font.PLAIN, 15));
            tekst1.setForeground(Color.black);
            tekst1.setDisabledTextColor(Color.black);
            tekst1.setWrapStyleWord(true);
            tekst1.setLineWrap(true);
            
            tekst1.putClientProperty("typWezla", typWezla);
            tekst1.putClientProperty("nazwa", tekst);
            
            tekst1.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
            tekst1.addMouseListener(ZarzadcaLinkami.dajInstancje());
            
            panel.add(tekst1);
            panel.add(Box.createRigidArea(new Dimension(0,15)));
        }
        
        private int znajdzLink(JPanel panel, String tekst) {
            int numer = -1;
            
            Component[] komponenty = panel.getComponents();
            for (int i = 0; i < komponenty.length; i++) {
                if(komponenty[i] instanceof JTextArea) {
                    JTextArea link = (JTextArea) komponenty[i];
                    
                    if (link.getText().equals(tekst)) {
                        numer = i;
                        break;
                    }
                }
            }
            
            return numer;
        }
        
        private void aktualizujNaglowki() {
            naglowekLewo.setText(NAGLOWEK_PO_LEWEJ_STRONIE + 
                    " (" + listaAlbumow.size() + ")");
            naglowekPrawo.setText(NAGLOWEK_PO_PRAWEJ_STRONIE +
                    " (" + listaPodobnych.size() + ")");
        }
        
        public void dodajNowyElement(ZarzadcaBazaNeo4J.TypyWezlow typWezla, 
                                     Wezel wezel, Artysta artysta) {
            Component komponent;
            
            switch (typWezla) {
                case ALBUM:
                    if (((Album) wezel).getNazwaArtystow().contains(artysta.getNazwaZespolu())  &&
                            !listaAlbumow.contains(((Album) wezel).getNazwaAlbumu())) {
                        listaAlbumow.add(((Album) wezel).getNazwaAlbumu());

                        komponent = panelZListaAlbumow.getComponent(0);

                        if (komponent instanceof JPanel) {
                            dodajLink((JPanel) komponent, ((Album) wezel).getNazwaAlbumu(), typWezla);
                            ((JPanel) komponent).revalidate();
                            aktualizujNaglowki();
                        }
                    }
                    
                    break;
                case ARTYSTA:
                    
                    if (!listaPodobnych.contains(((Artysta) wezel).getNazwaZespolu())) {
                        listaPodobnych.add(((Artysta) wezel).getNazwaZespolu());
                        
                        komponent = panelZListaPodobnych.getComponent(0);

                        if (komponent instanceof JPanel) {
                            dodajLink((JPanel) komponent, ((Artysta) wezel).getNazwaZespolu(), typWezla);
                            ((JPanel) komponent).revalidate();
                            aktualizujNaglowki();
                        }
                    }
                    
                    break;
            }
        }
        
        public void usunElement(ZarzadcaBazaNeo4J.TypyWezlow typWezla, 
                                Wezel wezel, Artysta artysta) {
            Component komponent;
            
            switch (typWezla) {
                case ALBUM:
                    if (((Album) wezel).getNazwaArtystow().contains(artysta.getNazwaZespolu())) {
                        listaAlbumow.remove(((Album) wezel).getNazwaAlbumu());

                        komponent = panelZListaAlbumow.getComponent(0);

                        if (komponent instanceof JPanel) {
                            int numer = znajdzLink((JPanel) komponent, ((Album) wezel).getNazwaAlbumu());
                            
                            ((JPanel) komponent).remove(numer);
                            ((JPanel) komponent).remove(numer);
                            ((JPanel) komponent).revalidate();
                            aktualizujNaglowki();
                        }
                    }
                    
                    break;
                    
                case ARTYSTA:
                    if ( artysta.getNazwaZespolu().equals( ((Artysta) wezel).getNazwaZespolu()) ) {
                        
                        JOptionPane.showMessageDialog(OknoGlowne.dajInstancje(), "Usunięto artystę. Wracam do spisu.", "Powrót do spisu", JOptionPane.INFORMATION_MESSAGE);
                        
                        OknoGlowne.dajInstancje().zmienPanelGlowny(new PanelSpis());
                        OknoGlowne.dajInstancje().setTitle(OknoGlowne.NAZWA_APLIKACJI + " - Spis wszystkich użytkowników i artystów");
                    
                    } else if (listaPodobnych.contains(((Artysta) wezel).getNazwaZespolu())) {
                        
                        listaPodobnych.remove(((Artysta) wezel).getNazwaZespolu());

                        komponent = panelZListaPodobnych.getComponent(0);

                        if (komponent instanceof JPanel) {
                            int numer = znajdzLink((JPanel) komponent, ((Artysta) wezel).getNazwaZespolu());

                            ((JPanel) komponent).remove(numer);
                            ((JPanel) komponent).remove(numer);
                            ((JPanel) komponent).revalidate();
                            aktualizujNaglowki();
                        }
                        
                    }
                    
                    break;
            }
        }
    }
}
