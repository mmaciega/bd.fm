package pl.bdfm.gfx;

import java.awt.Color;
import java.awt.Dimension;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import pl.bdfm.logic.ZarzadcaOkna;
import pl.bdfm.logic.ZarzadzcaMenu;

/**
 * Metoda reprezentująca okno główne aplikacji.
 * @author Mateusz Macięga, Piotr Gębala
 */
public class OknoGlowne extends JFrame {
    /** Stały rozmiar okna, pierwszy element to szereokość, drugi wysokość okna */
    public final static int[] POCZATKOWY_ROZMIAR_OKNA = {550, 700};
    /** Nazwa okna */
    public final static String NAZWA_APLIKACJI = "BazaDanych.FM";
    
    /** Rozwijane opcje w menu */
    private JMenuItem opcjaZakoncz, opcjaOProgramie;
    private JTextField opcjaSzukaj;
    
    private OknoGlowne() {
        setTitle(NAZWA_APLIKACJI);
        setPreferredSize(new Dimension(POCZATKOWY_ROZMIAR_OKNA[0], POCZATKOWY_ROZMIAR_OKNA[1]));
        setJMenuBar(wygenerujMenu());
        setContentPane(new PanelStartowy());
        setResizable(false);
        
        UIManager.put("OptionPane.background", Color.white);
        UIManager.put("Panel.background", Color.white);  
        UIManager.put("ComboBox.disabledForeground", Color.black);
        
        //ustawiamy ikone aplikacji
        URL iconURL = getClass().getResource("/icon.jpg");
        ImageIcon obrazek = new ImageIcon(iconURL);
        setIconImage(obrazek.getImage());

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        pack();
        
        setLocationRelativeTo(null);
        getContentPane().requestFocusInWindow();
        addWindowListener((ZarzadcaOkna.dajInstancje()));
    }
    
    public static OknoGlowne dajInstancje() {
        return OknoGlowneHolder.INSTANCE;
    }

    private static class OknoGlowneHolder { 
        private static final OknoGlowne INSTANCE = new OknoGlowne();
    }
    
    private JMenuBar wygenerujMenu() {
        JMenuBar pasekMenu = new JMenuBar();
        JMenu menuPlik = new JMenu("Plik");
        pasekMenu.add(menuPlik);

        opcjaZakoncz = new JMenuItem("Zakoncz");
        opcjaZakoncz.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaZakoncz.putClientProperty("tekst", "zakoncz");
        opcjaZakoncz.putClientProperty("okno", this);
        menuPlik.add(opcjaZakoncz);
        
        JMenu menuWyswietl = new JMenu("Wyświetl");
        pasekMenu.add(menuWyswietl);
        JMenuItem opcjaSpis = new JMenuItem("Spis użytkowników i artystów");
        opcjaSpis.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaSpis.putClientProperty("tekst", "spis");
        opcjaSpis.putClientProperty("okno", this);
        menuWyswietl.add(opcjaSpis);  

        JMenu menuOperacje = new JMenu("Operacje");
        pasekMenu.add(menuOperacje);
        JMenu menuDodajWezel = new JMenu("Dodaj węzeł");
        JMenuItem opcjaUzytkownika = new JMenuItem("Użytkownik");
        opcjaUzytkownika.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaUzytkownika.putClientProperty("tekst", "dodajUzytkownika");
        opcjaUzytkownika.putClientProperty("okno", this);
        menuDodajWezel.add(opcjaUzytkownika);
        
        JMenuItem opcjaArtysty = new JMenuItem("Artysta");
        opcjaArtysty.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaArtysty.putClientProperty("tekst", "dodajArtyste");
        opcjaArtysty.putClientProperty("okno", this);
        menuDodajWezel.add(opcjaArtysty);
        
        JMenuItem opcjaAlbumu = new JMenuItem("Album");
        opcjaAlbumu.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaAlbumu.putClientProperty("tekst", "dodajAlbum");
        opcjaAlbumu.putClientProperty("okno", this);
        menuDodajWezel.add(opcjaAlbumu);
        
        menuOperacje.add(menuDodajWezel);
        
        /////////////////////////////////////////////
        
        JMenu menuUsunWezel = new JMenu("Usuń węzeł");
        
        JMenuItem opcjaUsunUzytkownika = new JMenuItem("Użytkownik");
        opcjaUsunUzytkownika.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaUsunUzytkownika.putClientProperty("tekst", "usunUzytkownika");
        opcjaUsunUzytkownika.putClientProperty("okno", this);
        menuUsunWezel.add(opcjaUsunUzytkownika);
        
        JMenuItem opcjaUsunArtyste = new JMenuItem("Artysta");
        opcjaUsunArtyste.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaUsunArtyste.putClientProperty("tekst", "usunArtyste");
        opcjaUsunArtyste.putClientProperty("okno", this);
        menuUsunWezel.add(opcjaUsunArtyste);
        
        JMenuItem opcjaUsunAlbum = new JMenuItem("Album");
        opcjaUsunAlbum.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaUsunAlbum.putClientProperty("tekst", "usunAlbum");
        opcjaUsunAlbum.putClientProperty("okno", this);
        menuUsunWezel.add(opcjaUsunAlbum);
        
        
        menuOperacje.add(menuUsunWezel);
        
        /////////////////////////////////////////////
        
        menuOperacje.addSeparator();
        
        /////////////////////////////////////////////
        
        JMenu menuDodajRelacje = new JMenu("Dodaj relację");
        
        JMenuItem opcjaDodajZnajomosc = new JMenuItem("Znajomość");
        opcjaDodajZnajomosc.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaDodajZnajomosc.putClientProperty("tekst", "dodajZnajomosc");
        opcjaDodajZnajomosc.putClientProperty("okno", this);
        menuDodajRelacje.add(opcjaDodajZnajomosc);
        
        JMenuItem opcjaDodajPodobienstwo = new JMenuItem("Podobieństwo");
        opcjaDodajPodobienstwo.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaDodajPodobienstwo.putClientProperty("tekst", "dodajPodobienstwo");
        opcjaDodajPodobienstwo.putClientProperty("okno", this);
        menuDodajRelacje.add(opcjaDodajPodobienstwo);
        
        JMenuItem opcjaDodajPolubienie = new JMenuItem("Polubienie");
        opcjaDodajPolubienie.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaDodajPolubienie.putClientProperty("tekst", "dodajPolubienie");
        opcjaDodajPolubienie.putClientProperty("okno", this);
        menuDodajRelacje.add(opcjaDodajPolubienie);
        
        
        menuOperacje.add(menuDodajRelacje);
        
        /////////////////////////////////////////////
        
        JMenu menuUsunRelacje = new JMenu("Usuń relację");
        
        JMenuItem opcjaUsunZnajomosc = new JMenuItem("Znajomość");
        opcjaUsunZnajomosc.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaUsunZnajomosc.putClientProperty("tekst", "usunZnajomosc");
        opcjaUsunZnajomosc.putClientProperty("okno", this);
        menuUsunRelacje.add(opcjaUsunZnajomosc);
        
        JMenuItem opcjaUsunPodobienstwo = new JMenuItem("Podobieństwo");
        opcjaUsunPodobienstwo.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaUsunPodobienstwo.putClientProperty("tekst", "usunPodobienstwo");
        opcjaUsunPodobienstwo.putClientProperty("okno", this);
        menuUsunRelacje.add(opcjaUsunPodobienstwo);
//        
        JMenuItem opcjaUsunPolubienie = new JMenuItem("Polubienie");
        opcjaUsunPolubienie.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaUsunPolubienie.putClientProperty("tekst", "usunPolubienie");
        opcjaUsunPolubienie.putClientProperty("okno", this);
        menuUsunRelacje.add(opcjaUsunPolubienie);
        
        
        menuOperacje.add(menuUsunRelacje);
        
        /////////////////////////////////////////////
        
        JMenu menuPomoc = new JMenu("Pomoc");
        pasekMenu.add(menuPomoc);
        opcjaOProgramie = new JMenuItem("O programie");
        opcjaOProgramie.addActionListener(ZarzadzcaMenu.dajInstancje());
        opcjaOProgramie.putClientProperty("tekst", "oprogramie");
        opcjaOProgramie.putClientProperty("okno", this);
        menuPomoc.add(opcjaOProgramie);  
        
//        pasekMenu.add(Box.createHorizontalGlue());
//        
//        opcjaSzukaj = new JSearchTextField();
//        ((JIconTextField) opcjaSzukaj).setIcon("iconSearch.png");
//        opcjaSzukaj.setMaximumSize( opcjaSzukaj.getPreferredSize() );
//        opcjaSzukaj.addActionListener(ZarzadzcaMenu.dajInstancje());
//        opcjaSzukaj.putClientProperty("okno", this);
//        pasekMenu.add(opcjaSzukaj);
//        
//        pasekMenu.setBorder(new EmptyBorder(0, 0, 0, 5));
        
        return pasekMenu;
    }
    
    public void zmienPanelGlowny(JPanel panel) {
            setContentPane(panel);
            revalidate();
            repaint(); 
    }
}
