package pl.bdfm.gfx;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelStartowy extends JPanel {
    
    public PanelStartowy() {
        setLayout(new GridBagLayout());
        setBackground(Color.white);
        
        JPanel panelZZawartoscia = new JPanel();
        panelZZawartoscia.setOpaque(false);
        panelZZawartoscia.setLayout(new BoxLayout(panelZZawartoscia, BoxLayout.Y_AXIS));
        
        JLabel tekst1 = new JLabel("Projekt");
        tekst1.setFont(new Font("Verdana", Font.BOLD, 60));
        tekst1.setAlignmentX(Component.CENTER_ALIGNMENT);
        tekst1.setForeground(Color.black);
        
        JLabel tekst2 = new JLabel("BazyDanych.FM");
        tekst2.setFont(new Font("Verdana", Font.BOLD, 40));
        tekst2.setAlignmentX(Component.CENTER_ALIGNMENT);
        tekst2.setForeground(Color.black);
        
        JLabel avatarLabel = new JLabel();

        try {
            Image obraz;
            obraz = ImageIO.read(getClass().getResourceAsStream("/BDFMlogo 300.png"));
            ImageIcon obrazek = new ImageIcon(obraz);
            avatarLabel.setIcon(obrazek);
            avatarLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        } 
        catch (IOException ex) 
        {
            System.out.println("Nie ma takiego pliku");
        }
               
        
        JLabel tekst3 = new JLabel("Autorzy: Mateusz Macięga, Piotr Gębala");
        tekst3.setFont(new Font("Verdana", Font.BOLD, 15));
        tekst3.setAlignmentX(Component.CENTER_ALIGNMENT);
        tekst3.setForeground(Color.black);  
        
        panelZZawartoscia.add(tekst1);
        panelZZawartoscia.add(Box.createRigidArea(new Dimension(0, 10)));
        panelZZawartoscia.add(tekst2);
        panelZZawartoscia.add(Box.createRigidArea(new Dimension(0, 15)));
        panelZZawartoscia.add(avatarLabel);
        panelZZawartoscia.add(Box.createRigidArea(new Dimension(0, 50)));
        panelZZawartoscia.add(tekst3);
        
        
        add(panelZZawartoscia);
    }
   
}
