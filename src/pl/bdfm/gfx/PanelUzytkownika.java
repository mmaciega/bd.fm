/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.bdfm.gfx;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Stroke;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import org.neo4j.graphdb.Direction;
import pl.bdfm.gfx.components.ScrollablePanel;
import pl.bdfm.logic.Artysta;
import pl.bdfm.logic.IObserwator;
import pl.bdfm.logic.IObserwowany;
import pl.bdfm.logic.Uzytkownik;
import pl.bdfm.logic.Wezel;
import pl.bdfm.logic.ZarzadcaBazaNeo4J;
import pl.bdfm.logic.ZarzadcaKursorem;
import pl.bdfm.logic.ZarzadcaLinkami;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelUzytkownika extends JPanel implements IObserwator {
    private final JPanel panelZDanymi;
    private final JPanel panelZPowiazaniami;
    private final Uzytkownik uzytkownik;
    
    public PanelUzytkownika(Uzytkownik uzytkownik) {
        setLayout(new BorderLayout());
        setBackground(Color.white);
        
        this.uzytkownik = uzytkownik;
        panelZDanymi = new PanelZDanymi(uzytkownik);
        panelZPowiazaniami = new PanelZPowiazaniami(uzytkownik.getListaZnajomych(), uzytkownik.getListaUlubionych());
        
        add(panelZDanymi, BorderLayout.NORTH);
        add(panelZPowiazaniami, BorderLayout.CENTER);
    }

    public Uzytkownik getUzytkownik() {
        return uzytkownik;
    }
    
    @Override
    public void aktualizacjaWezla(IObserwowany.TypyPowiadomienia typPowiadomienia, Wezel wezel) {
        ZarzadcaBazaNeo4J.TypyWezlow typWezla = null;
        if(wezel instanceof Uzytkownik) {
            typWezla = ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK;
        } else if (wezel instanceof Artysta) {
            typWezla = ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA;
        }
        
        if(typWezla != null) {
            switch(typPowiadomienia) {
                case USUNIECIE:
                        ((PanelZPowiazaniami) panelZPowiazaniami).usunElement(typWezla, wezel, uzytkownik);
                    break;
            }
        }
    }

    @Override
    public void aktualizacjaRelacji(IObserwowany.TypyPowiadomienia typPowiadomienia, ZarzadcaBazaNeo4J.TypyRelacji typRelacji, Wezel wezel1, Wezel wezel2, Direction kierunek) {
        Wezel wezelDodany = null;
        if(wezel1 instanceof Uzytkownik && ((Uzytkownik)wezel1).getPseudonim().equals(uzytkownik.getPseudonim())  ) {
            wezelDodany = wezel2;
        } else if(wezel2 instanceof Uzytkownik && ((Uzytkownik)wezel2).getPseudonim().equals(uzytkownik.getPseudonim())  ) {
            wezelDodany = wezel1;
        }
     
        if(wezelDodany != null ) {
            ZarzadcaBazaNeo4J.TypyWezlow typWezlaDodanego = null;
            if(wezelDodany instanceof Uzytkownik) {
                typWezlaDodanego = ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK;
            } else if (wezelDodany instanceof Artysta) {
                typWezlaDodanego = ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA;
            }
        
            if(typWezlaDodanego != null) {
                switch(typPowiadomienia) {
                    case DODANIE:
                        ((PanelZPowiazaniami) panelZPowiazaniami).dodajNowyElement(typWezlaDodanego, wezelDodany, uzytkownik);
                        break;
                    case USUNIECIE:
                        ((PanelZPowiazaniami) panelZPowiazaniami).usunElement(typWezlaDodanego, wezelDodany, uzytkownik);
                        break;
                }
            }
        }
    }
    
    class PanelZDanymi extends JPanel {
        private final Uzytkownik uzytkownik;
        
        public PanelZDanymi(Uzytkownik uzytkownik) {
            this.uzytkownik = uzytkownik;
            setOpaque(false);
            setLayout(new BorderLayout());
            
            add(wygenerujPanelZDanymi(), BorderLayout.CENTER);
            add(wygenerujPanelZAvatarem(), BorderLayout.EAST);
        }
        
        private JPanel wygenerujPanelZDanymi() {
            JPanel panel = new JPanel();
            panel.setOpaque(false);
            panel.setLayout(new BorderLayout());
            
            JPanel panelD = new JPanel();
            panelD.setOpaque(false);
            panelD.setLayout(new BoxLayout(panelD, BoxLayout.Y_AXIS));
            panelD.setBorder(new EmptyBorder(15, 20, 30, 20));
            
            JTextArea nazwaUzytkownika = new JTextArea(uzytkownik.getPseudonim());
            nazwaUzytkownika.setEditable(false);
            nazwaUzytkownika.setBackground(null);
            nazwaUzytkownika.setBorder(null);
            nazwaUzytkownika.setOpaque(false);
            nazwaUzytkownika.setFont(new Font("Verdana", Font.BOLD, 25));
            nazwaUzytkownika.setForeground(Color.black);
            nazwaUzytkownika.setDisabledTextColor(Color.black);
            nazwaUzytkownika.setWrapStyleWord(true);
            nazwaUzytkownika.setLineWrap(true);
            
            JTextField imieINazwisko=new JTextField(uzytkownik.getImie() + " " + uzytkownik.getNazwisko());
            imieINazwisko.setEditable(false);
            imieINazwisko.setBackground(null);
            imieINazwisko.setBorder(null);
            imieINazwisko.setOpaque(false);
            imieINazwisko.setFont(new Font("Verdana", Font.BOLD, 12));
            imieINazwisko.setForeground(Color.black);
            imieINazwisko.setDisabledTextColor(Color.black);

            JTextField trzeciWiersz=new JTextField((uzytkownik.getPlec() != Uzytkownik.Plci.N ? uzytkownik.getPlec().dajLancuch() + ", " : "") 
                    + (uzytkownik.getWiek() != 0 ? uzytkownik.getWiek() + " lat, " : "")
                    + (!uzytkownik.getKrajPochodzenia().equals("") ? uzytkownik.getKrajPochodzenia().toUpperCase() : ""));
            trzeciWiersz.setEditable(false);
            trzeciWiersz.setBackground(null);
            trzeciWiersz.setBorder(null);
            trzeciWiersz.setOpaque(false);
            trzeciWiersz.setFont(new Font("Verdana", Font.PLAIN, 12));
            trzeciWiersz.setForeground(Color.black);
            trzeciWiersz.setDisabledTextColor(Color.black);
            
            JTextField czwartyWiersz=new JTextField("edytuj");
            czwartyWiersz.setEditable(false);
            czwartyWiersz.setBackground(null);
            czwartyWiersz.setBorder(null);
            czwartyWiersz.setOpaque(false);
            czwartyWiersz.setFont(new Font("Verdana", Font.PLAIN, 10));
            czwartyWiersz.setForeground(Color.gray);
            czwartyWiersz.setDisabledTextColor(Color.black);
         
            
            panelD.add(nazwaUzytkownika);
            panelD.add(Box.createRigidArea(new Dimension(0,5)));
            panelD.add(imieINazwisko);
            panelD.add(Box.createRigidArea(new Dimension(0,5)));
            panelD.add(trzeciWiersz);
//            panelD.add(Box.createRigidArea(new Dimension(0,5)));
//            panelD.add(czwartyWiersz);
            
            panel.add(panelD, BorderLayout.CENTER);
            
            return panel;
        }
        
        private JPanel wygenerujPanelZAvatarem() {
            JPanel panel = new JPanel() {
                @Override
                public void paintComponent(Graphics g) {
                    super.paintComponent(g);
                    Graphics2D g2d = (Graphics2D) g;

                    int rozmiarKropkowania = 1;

                    Stroke dashed = new BasicStroke(rozmiarKropkowania, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{1}, 0);

                    g2d.setStroke(dashed);
                    g2d.setColor(Color.GRAY);
                    g2d.drawLine(2, 0, 2, (int) getHeight() - (rozmiarKropkowania+2));
                }
            };
            
            panel.setOpaque(false);
            panel.setBorder(new EmptyBorder(10, 30, 10, 20));
            
            JLabel avatarLabel = new JLabel();
            
            try {
                Image obraz;
                obraz = ImageIO.read(getClass().getResourceAsStream("/defaultAvatarSmaller.jpg"));
                ImageIcon obrazek = new ImageIcon(obraz);
                avatarLabel.setIcon(obrazek);
                avatarLabel.setBorder(new LineBorder(Color.black));
            } 
            catch (IOException ex) 
            {
                System.out.println("Nie ma takiego pliku");
            }
            
            panel.add(avatarLabel);
            
            return panel;
        }
        
        
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            
            int rozmiarKropkowania = 1;
            
            Stroke dashed = new BasicStroke(rozmiarKropkowania, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{1}, 0);
            
            g2d.setStroke(dashed);
            g2d.setColor(Color.GRAY);
            g2d.drawLine(0, (int) getHeight()-(rozmiarKropkowania+2), (int) getWidth(), (int) getHeight()-(rozmiarKropkowania+2));
        }
    }
    
    class PanelZPowiazaniami extends JPanel {
        private static final String NAGLOWEK_PO_LEWEJ_STRONIE = "Znajomi";
        private static final String NAGLOWEK_PO_PRAWEJ_STRONIE = "Ulubione";
        private static final int MARGINES_POD_NAGLOWKAMI = 20;
        
        private final List<String> listaZnajomych;
        private final List<String> listaUlubionych;
        
        private final JPanel panelZListaZnajomych;
        private final JPanel panelZListaUlubionych;
        
        private final JLabel naglowekLewo;
        private final JLabel naglowekPrawo;
        
        public PanelZPowiazaniami(List<String> listaZnajomych, List<String> listaUlubionych) {
            super();
            this.listaZnajomych = listaZnajomych;
            this.listaUlubionych = listaUlubionych;
            
            setBorder(new EmptyBorder(20, 0, 0, 0));
            setOpaque(false);
            setLayout(new BorderLayout());
            
            JPanel panelZNaglowkami = new JPanel();
            panelZNaglowkami.setLayout(new GridLayout(1, 2));
            panelZNaglowkami.setBorder(new EmptyBorder(0, 25, MARGINES_POD_NAGLOWKAMI, 0));
            panelZNaglowkami.setOpaque(false);
             
            JPanel panelZLewymNaglowkiem = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
            naglowekLewo = new JLabel(NAGLOWEK_PO_LEWEJ_STRONIE + " (" + listaZnajomych.size() + ")");
            naglowekLewo.setFont(new Font("Verdana", Font.BOLD, 22));
            naglowekLewo.setForeground(Color.red);
            
            JPanel panelTest = new JPanel();
            panelTest.setLayout(new BoxLayout(panelTest, BoxLayout.Y_AXIS));
//            panelTest.setBorder(new LineBorder(Color.yellow));
            
            JLabel dodajZnajomego = new JLabel("+ dodaj");
            dodajZnajomego.setFont(new Font("Verdana", Font.PLAIN, 10));
            dodajZnajomego.setForeground(Color.GRAY);
            dodajZnajomego.setBorder(new EmptyBorder(0,10,0,0));
            dodajZnajomego.setAlignmentY(CENTER_ALIGNMENT);
            dodajZnajomego.putClientProperty("typWezla", ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK);
            dodajZnajomego.putClientProperty("typOperacji", "dodajZnajomego");
            
            dodajZnajomego.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
            dodajZnajomego.addMouseListener(ZarzadcaLinkami.dajInstancje());
            
            JLabel usunZnajomego = new JLabel("- usun");
            usunZnajomego.setFont(new Font("Verdana", Font.PLAIN, 10));
            usunZnajomego.setForeground(Color.GRAY);
            usunZnajomego.setBorder(new EmptyBorder(0,10,0,0));
            usunZnajomego.setAlignmentY(CENTER_ALIGNMENT);
            usunZnajomego.putClientProperty("typWezla", ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK);
            usunZnajomego.putClientProperty("typOperacji", "usunZnajomego");
            
            usunZnajomego.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
            usunZnajomego.addMouseListener(ZarzadcaLinkami.dajInstancje());
            
            panelTest.add(dodajZnajomego);
            panelTest.add(Box.createVerticalBox());
            panelTest.add(usunZnajomego);
           
            panelZLewymNaglowkiem.add(naglowekLewo);
            panelZLewymNaglowkiem.add(panelTest);
            
            JPanel panelZPrawymNaglowkiem = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
            
            JPanel panelTest2 = new JPanel();
            panelTest2.setLayout(new BoxLayout(panelTest2, BoxLayout.Y_AXIS));
            
            naglowekPrawo = new JLabel(NAGLOWEK_PO_PRAWEJ_STRONIE + " (" + listaUlubionych.size() + ")");
            naglowekPrawo.setFont(new Font("Verdana", Font.BOLD, 22));
            naglowekPrawo.setForeground(Color.red);
            
            JLabel dodajPolubienie = new JLabel("+ dodaj");
            dodajPolubienie.setFont(new Font("Verdana", Font.PLAIN, 10));
            dodajPolubienie.setForeground(Color.GRAY);
            dodajPolubienie.setBorder(new EmptyBorder(0,10,0,0));
            dodajPolubienie.setAlignmentY(CENTER_ALIGNMENT);
            dodajPolubienie.putClientProperty("typWezla", ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);
            dodajPolubienie.putClientProperty("typOperacji", "dodajPolubienie");
            
            dodajPolubienie.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
            dodajPolubienie.addMouseListener(ZarzadcaLinkami.dajInstancje());
            
            JLabel usunPolubienie = new JLabel("- usun");
            usunPolubienie.setFont(new Font("Verdana", Font.PLAIN, 10));
            usunPolubienie.setForeground(Color.GRAY);
            usunPolubienie.setBorder(new EmptyBorder(0,10,0,0));
            usunPolubienie.setAlignmentY(CENTER_ALIGNMENT);
            usunPolubienie.putClientProperty("typWezla", ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);
            usunPolubienie.putClientProperty("typOperacji", "usunPolubienie");
            
            usunPolubienie.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
            usunPolubienie.addMouseListener(ZarzadcaLinkami.dajInstancje());
            
            panelTest2.add(dodajPolubienie);
            panelTest2.add(Box.createVerticalBox());
            panelTest2.add(usunPolubienie);
//            
            panelZPrawymNaglowkiem.add(naglowekPrawo);
            panelZPrawymNaglowkiem.add(panelTest2);
            
            panelZNaglowkami.add(panelZLewymNaglowkiem);
            panelZNaglowkami.add(panelZPrawymNaglowkiem);
            
            ScrollablePanel panel = new ScrollablePanel(new GridLayout(1,2));
            panel.setScrollableWidth( ScrollablePanel.ScrollableSizeHint.FIT );
            panel.setBackground(Color.white);
            panel.setLayout(new GridLayout(1, 2));
            panel.setBorder(new EmptyBorder(10, 25, 0, 0));
  
            panelZListaZnajomych = wygnerujPanelZLista(this.listaZnajomych, ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK);
            panelZListaUlubionych = wygnerujPanelZLista(this.listaUlubionych, ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);
            
            panel.add(panelZListaZnajomych);
            panel.add(panelZListaUlubionych);

            final JScrollPane panelZeScrollem = new JScrollPane(panel);
            panelZeScrollem.setBorder(null);
            panelZeScrollem.getViewport().setBackground(Color.white);
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    panelZeScrollem.getViewport().setViewPosition(new java.awt.Point(0, 0));
                }
            });   
            
            add(panelZNaglowkami, BorderLayout.NORTH);
            add(panelZeScrollem, BorderLayout.CENTER);
        }
        
        private JPanel wygnerujPanelZLista(List<String> lista, ZarzadcaBazaNeo4J.TypyWezlow typWezla) {
            JPanel panelOpakowujacy = new JPanel();
            panelOpakowujacy.setOpaque(false);
            panelOpakowujacy.setLayout(new BorderLayout());
            
            JPanel panelZLinkami = new JPanel();
            panelZLinkami.setOpaque(false);
            panelZLinkami.setLayout(new BoxLayout(panelZLinkami, BoxLayout.Y_AXIS));
            
            for (String element : lista) {
                dodajLink(panelZLinkami, element, typWezla);
            }

            panelOpakowujacy.add(panelZLinkami, BorderLayout.NORTH);
            
            return panelOpakowujacy;
        }
        
        private void dodajLink(JPanel panel, String tekst, ZarzadcaBazaNeo4J.TypyWezlow typWezla) {
            JTextArea tekst1=new JTextArea(tekst);
            tekst1.setEditable(false);
            tekst1.setBackground(null);
            tekst1.setBorder(null);
            tekst1.setOpaque(false);
            tekst1.setFont(new Font("Verdana", Font.PLAIN, 15));
            tekst1.setForeground(Color.black);
            tekst1.setDisabledTextColor(Color.black);
            tekst1.setWrapStyleWord(true);
            tekst1.setLineWrap(true);
            
            tekst1.putClientProperty("typWezla", typWezla);
            tekst1.putClientProperty("nazwa", tekst);

            tekst1.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
            tekst1.addMouseListener(ZarzadcaLinkami.dajInstancje());
            
            panel.add(tekst1);
            panel.add(Box.createRigidArea(new Dimension(0,15)));
        }
        
        public void usunElement(ZarzadcaBazaNeo4J.TypyWezlow typWezla, Wezel wezel, Uzytkownik uzytkownik) {
            Component komponent;
            
            switch (typWezla) {
                case UZYTKOWNIK:
                    if ( uzytkownik.getPseudonim().equals( ((Uzytkownik) wezel).getPseudonim() ) ) {
                        
                        JOptionPane.showMessageDialog(OknoGlowne.dajInstancje(), "Usunięto użytkownika. Wracam do spisu.", "Powrót do spisu", JOptionPane.INFORMATION_MESSAGE);
                        
                        OknoGlowne.dajInstancje().zmienPanelGlowny(new PanelSpis());
                        OknoGlowne.dajInstancje().setTitle(OknoGlowne.NAZWA_APLIKACJI + " - Spis wszystkich użytkowników i artystów");
                    } else if (listaZnajomych.contains(((Uzytkownik) wezel).getPseudonim())) {
                        
                        listaZnajomych.remove(((Uzytkownik) wezel).getPseudonim());

                        komponent = panelZListaZnajomych.getComponent(0);

                        if (komponent instanceof JPanel) {
                            int numer = znajdzLink((JPanel) komponent, ((Uzytkownik) wezel).getPseudonim());

                            ((JPanel) komponent).remove(numer);
                            ((JPanel) komponent).remove(numer);
                            ((JPanel) komponent).revalidate();
                            aktualizujNaglowki();
                        }
                        
                    }
                    
                    break;
                    
                case ARTYSTA:
                    if (listaUlubionych.contains(((Artysta) wezel).getNazwaZespolu())) {
                        
                        listaUlubionych.remove(((Artysta) wezel).getNazwaZespolu());

                        komponent = panelZListaUlubionych.getComponent(0);

                        if (komponent instanceof JPanel) {
                            int numer = znajdzLink((JPanel) komponent, ((Artysta) wezel).getNazwaZespolu());

                            ((JPanel) komponent).remove(numer);
                            ((JPanel) komponent).remove(numer);
                            ((JPanel) komponent).revalidate();
                            aktualizujNaglowki();
                        }
                        
                    }
                    
                    break;
            }
        }
        
        public void dodajNowyElement(ZarzadcaBazaNeo4J.TypyWezlow typWezla, Wezel wezel, Uzytkownik uzytkownik) {
            Component komponent;
//            
            switch (typWezla) {
                case UZYTKOWNIK:
                    
                    if (!listaZnajomych.contains(((Uzytkownik) wezel).getPseudonim())) {
                        listaZnajomych.add(((Uzytkownik) wezel).getPseudonim());
                        
                        komponent = panelZListaZnajomych.getComponent(0);

                        if (komponent instanceof JPanel) {
                            dodajLink((JPanel) komponent, ((Uzytkownik) wezel).getPseudonim(), typWezla);
                            ((JPanel) komponent).revalidate();
                            aktualizujNaglowki();
                        }
                    }
                    
                    break;
                case ARTYSTA:
                    if (!listaUlubionych.contains(((Artysta) wezel).getNazwaZespolu())) {
                        listaUlubionych.add(((Artysta) wezel).getNazwaZespolu());
                        
                        komponent = panelZListaUlubionych.getComponent(0);

                        if (komponent instanceof JPanel) {
                            dodajLink((JPanel) komponent, ((Artysta) wezel).getNazwaZespolu(), typWezla);
                            ((JPanel) komponent).revalidate();
                            aktualizujNaglowki();
                        }
                    }
                    
                    break;
            }
        }
        
        private void aktualizujNaglowki() {
            naglowekLewo.setText(NAGLOWEK_PO_LEWEJ_STRONIE + " (" + listaZnajomych.size() + ")");
            naglowekPrawo.setText(NAGLOWEK_PO_PRAWEJ_STRONIE + " (" + listaUlubionych.size() + ")");
        }
        
        private int znajdzLink(JPanel panel, String tekst) {
            int numer = -1;
            
            Component[] komponenty = panel.getComponents();
            for (int i = 0; i < komponenty.length; i++) {
                if(komponenty[i] instanceof JTextArea) {
                    JTextArea link = (JTextArea) komponenty[i];
                    
                    if (link.getText().equals(tekst)) {
                        numer = i;
                        break;
                    }
                }
            }
            
            return numer;
        }
    }
}
