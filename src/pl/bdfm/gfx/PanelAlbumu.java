/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bdfm.gfx;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Stroke;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import pl.bdfm.logic.Album;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelAlbumu extends JPanel {

    Album album;

    private static final int MARGINES_POD_NAZWA_PLYTY = 30;
    
    private static final int MARGINES_OD_LINII_KROPKOWANEJ = 35;

    public PanelAlbumu(Album album) {
        this.album = album;
        setBackground(Color.white);
        setLayout(new BorderLayout());

        add(wygenerujPanelZDanymi(), BorderLayout.CENTER);
        add(wygenerujPanelZAvatarem(), BorderLayout.EAST);
    }

    private JPanel wygenerujPanelZDanymi() {
        JPanel panel = new JPanel();
        panel.setOpaque(false);
        panel.setLayout(new BorderLayout());

        JPanel panelD = new JPanel();
        panelD.setOpaque(false);
        panelD.setLayout(new BoxLayout(panelD, BoxLayout.Y_AXIS));
        panelD.setBorder(new EmptyBorder(0, 0, 0, MARGINES_OD_LINII_KROPKOWANEJ));

        JTextArea nazwaArtysty = new JTextArea(album.getNazwaAlbumu());
        nazwaArtysty.setEditable(false);
        nazwaArtysty.setBackground(null);
        nazwaArtysty.setBorder(null);
        nazwaArtysty.setOpaque(false);
        nazwaArtysty.setFont(new Font("Verdana", Font.BOLD, 15));
        nazwaArtysty.setForeground(Color.black);
        nazwaArtysty.setDisabledTextColor(Color.black);

        JPanel panelZArtystami = new JPanel(new WrapLayout(FlowLayout.LEFT, 0, 0));
        panelZArtystami.setOpaque(false);

        for (int i = 0; i < album.getNazwaArtystow().size() - 1; i++) {
            JLabel etykietaArtysta = new JLabel();
            etykietaArtysta.setFont(new Font("Verdana", Font.PLAIN, 14));
            etykietaArtysta.setForeground(Color.black);
            etykietaArtysta.setText(album.getNazwaArtystow().get(i));

            panelZArtystami.add(etykietaArtysta);
            panelZArtystami.add(new JLabel(", "));
        }

        JLabel etykietaArtysta = new JLabel();
        etykietaArtysta.setFont(new Font("Verdana", Font.PLAIN, 14));
        etykietaArtysta.setForeground(Color.black);
        etykietaArtysta.setText(album.getNazwaArtystow().get(album.getNazwaArtystow().size() - 1));
        panelZArtystami.add(etykietaArtysta);

        JTextField rokWydania = new JTextField("Rok wydania: " + String.valueOf(album.getRokWydania()));
        rokWydania.setEditable(false);
        rokWydania.setBackground(null);
        rokWydania.setBorder(null);
        rokWydania.setOpaque(false);
        rokWydania.setFont(new Font("Verdana", Font.PLAIN, 12));
        rokWydania.setForeground(Color.black);
        rokWydania.setDisabledTextColor(Color.black);

        JTextField czasTrwania = new JTextField("Czas trwania: " + album.getCzasTrwania());
        czasTrwania.setEditable(false);
        czasTrwania.setBackground(null);
        czasTrwania.setBorder(null);
        czasTrwania.setOpaque(false);
        czasTrwania.setFont(new Font("Verdana", Font.PLAIN, 12));
        czasTrwania.setForeground(Color.black);
        czasTrwania.setDisabledTextColor(Color.black);

        JTextField liczbaUtworow = new JTextField("Liczba utworów: " + String.valueOf(album.getLiczbaUtworow()));
        liczbaUtworow.setEditable(false);
        liczbaUtworow.setBackground(null);
        liczbaUtworow.setBorder(null);
        liczbaUtworow.setOpaque(false);
        liczbaUtworow.setFont(new Font("Verdana", Font.PLAIN, 12));
        liczbaUtworow.setForeground(Color.black);
        liczbaUtworow.setDisabledTextColor(Color.black);

        panelD.add(nazwaArtysty);
        panelD.add(Box.createRigidArea(new Dimension(0, 5)));
        panelD.add(panelZArtystami);
        panelD.add(Box.createRigidArea(new Dimension(0, MARGINES_POD_NAZWA_PLYTY)));
        panelD.add(rokWydania);
        panelD.add(Box.createRigidArea(new Dimension(0, 5)));
        panelD.add(czasTrwania);
        panelD.add(Box.createRigidArea(new Dimension(0, 5)));
        panelD.add(liczbaUtworow);

        panel.add(panelD, BorderLayout.CENTER);

        return panel;
    }

    private JPanel wygenerujPanelZAvatarem() {
        JPanel panel = new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2d = (Graphics2D) g;

                int rozmiarKropkowania = 1;

                Stroke dashed = new BasicStroke(rozmiarKropkowania, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{1}, 0);

                g2d.setStroke(dashed);
                g2d.setColor(Color.GRAY);
                g2d.drawLine(2, 0, 2, (int) getHeight() - (rozmiarKropkowania + 2));
            }
        };

        panel.setOpaque(false);
        panel.setBorder(new EmptyBorder(0, MARGINES_OD_LINII_KROPKOWANEJ, 0, 0));

        JLabel avatarLabel = new JLabel();

        try {
            Image obraz;
            obraz = ImageIO.read(getClass().getResourceAsStream("/defaultAlbumCoverSmallest.png"));
            ImageIcon obrazek = new ImageIcon(obraz);
            avatarLabel.setIcon(obrazek);
            avatarLabel.setBorder(new LineBorder(Color.black));
        } catch (IOException ex) {
            System.out.println("Nie ma takiego pliku");
        }

        panel.add(avatarLabel);

        return panel;
    }
}
