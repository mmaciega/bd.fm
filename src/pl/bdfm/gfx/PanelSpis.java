/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.bdfm.gfx;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Stroke;
import java.util.List;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import org.neo4j.graphdb.Direction;
import pl.bdfm.gfx.components.ScrollablePanel;
import pl.bdfm.logic.Artysta;
import pl.bdfm.logic.IObserwator;
import pl.bdfm.logic.IObserwowany;
import pl.bdfm.logic.Uzytkownik;
import pl.bdfm.logic.Wezel;
import pl.bdfm.logic.ZarzadcaBazaNeo4J;
import pl.bdfm.logic.ZarzadcaKursorem;
import pl.bdfm.logic.ZarzadcaLinkami;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelSpis extends JPanel implements IObserwator {
    private final JPanel panelZInformacjami;
    private final JPanel panelZPowiazaniami;
    
    public PanelSpis() {
        setLayout(new BorderLayout());
        setBackground(Color.white);
        
        panelZInformacjami = new PanelZInformacjami();
        panelZPowiazaniami = new PanelZPowiazaniami
        (ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK), ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA));
        
        add(panelZInformacjami, BorderLayout.NORTH);
        add(panelZPowiazaniami, BorderLayout.CENTER);
    }

    @Override
    public void aktualizacjaWezla(IObserwowany.TypyPowiadomienia typPowiadomienia, Wezel wezel) {
        ZarzadcaBazaNeo4J.TypyWezlow typWezla = null;
        if(wezel instanceof Uzytkownik) {
            typWezla = ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK;
        } else if (wezel instanceof Artysta) {
            typWezla = ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA;
        }
        
        if(typWezla != null) {
            switch(typPowiadomienia) {
                case DODANIE:
                    ((PanelZPowiazaniami) panelZPowiazaniami).dodajNowyElement(typWezla, wezel);
                    break;
                case USUNIECIE:
                    ((PanelZPowiazaniami) panelZPowiazaniami).usunElement(typWezla, wezel);
                    break;
            }
        }
    }
    
    

    @Override
    public void aktualizacjaRelacji(IObserwowany.TypyPowiadomienia typPowiadomienia, ZarzadcaBazaNeo4J.TypyRelacji typRelacji, Wezel wezel1, Wezel wezel2, Direction kierunek) {
        
    }
    
    class PanelZInformacjami extends JPanel {
        public PanelZInformacjami() {
            setLayout(new BorderLayout());
            setOpaque(false);
            
            add(wygenerujPanelZInformacjami(), BorderLayout.WEST);
        }
        
        private JPanel wygenerujPanelZInformacjami() {
            JPanel panel = new JPanel();
            panel.setOpaque(false);
            
            JPanel panelD = new JPanel();
            panelD.setOpaque(false);
            panelD.setLayout(new BoxLayout(panelD, BoxLayout.Y_AXIS));
            panelD.setBorder(new EmptyBorder(15, 20, 30, 20));
            
            JTextField tekst1=new JTextField("Spis wszystkich użytkowników i artystów".toUpperCase());
            tekst1.setEditable(false);
            tekst1.setBackground(null);
            tekst1.setOpaque(false);
            tekst1.setBorder(null);
            tekst1.setFont(new Font("Verdana", Font.BOLD, 18));
            tekst1.setForeground(Color.black);
            tekst1.setDisabledTextColor(Color.black);

            panelD.add(tekst1);
            
            panel.add(panelD);
            
            return panel;
        }
        
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            
            int rozmiarKropkowania = 1;
            
            Stroke dashed = new BasicStroke(rozmiarKropkowania, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{1}, 0);
            
            g2d.setStroke(dashed);
            g2d.setColor(Color.GRAY);
            g2d.drawLine(0, (int) getHeight()-(rozmiarKropkowania+2), (int) getWidth(), (int) getHeight()-(rozmiarKropkowania+2));
        }
    }
    
    class PanelZPowiazaniami extends JPanel {
        private static final String NAGLOWEK_PO_LEWEJ_STRONIE = "Użytkownicy";
        private static final String NAGLOWEK_PO_PRAWEJ_STRONIE = "Artyści";
        private static final int MARGINES_POD_NAGLOWKAMI = 20;
        
        private final List<String> listaUzytkownikow;
        private final List<String> listaArtystow;
        
        private final JPanel panelZListaUzytkownikow;
        private final JPanel panelZListaArtystow;
        
        private final JLabel naglowekLewo;
        private final JLabel naglowekPrawo;
        
        public PanelZPowiazaniami(List<String> listaUzytkownikow, List<String> listaArtystow) {
            super();
            this.listaUzytkownikow = listaUzytkownikow;
            this.listaArtystow = listaArtystow;
            
            setOpaque(false);
            setBorder(new EmptyBorder(20, 0, 0, 0));
            setLayout(new BorderLayout());
            
            JPanel panelZNaglowkami = new JPanel();
            panelZNaglowkami.setLayout(new GridLayout(1, 2));
            panelZNaglowkami.setBorder(new EmptyBorder(0, 25, MARGINES_POD_NAGLOWKAMI, 0));
            panelZNaglowkami.setOpaque(false);
             
            naglowekLewo = new JLabel(NAGLOWEK_PO_LEWEJ_STRONIE + " (" + listaUzytkownikow.size() + ")");
            naglowekLewo.setFont(new Font("Verdana", Font.BOLD, 22));
            naglowekLewo.setForeground(Color.red);
            
            naglowekPrawo = new JLabel(NAGLOWEK_PO_PRAWEJ_STRONIE + " (" + listaArtystow.size() + ")");
            naglowekPrawo.setFont(new Font("Verdana", Font.BOLD, 22));
            naglowekPrawo.setForeground(Color.red);
            
            panelZNaglowkami.add(naglowekLewo);
            panelZNaglowkami.add(naglowekPrawo);
  
            ScrollablePanel panel = new ScrollablePanel(new GridLayout(1,2));
            panel.setScrollableWidth( ScrollablePanel.ScrollableSizeHint.FIT );
            panel.setBackground(Color.white);
            panel.setLayout(new GridLayout(1, 2));
            panel.setBorder(new EmptyBorder(10, 25, 0, 0));
            
            
            panelZListaUzytkownikow = wygnerujPanelZLista(this.listaUzytkownikow, ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK);
            panelZListaArtystow = wygnerujPanelZLista(this.listaArtystow, ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);
                    
            panel.add(panelZListaUzytkownikow);
            panel.add(panelZListaArtystow);
            
            final JScrollPane panelZeScrollem = new JScrollPane(panel);
            panelZeScrollem.setBorder(null);
            panelZeScrollem.getViewport().setBackground(Color.white);
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    panelZeScrollem.getViewport().setViewPosition(new java.awt.Point(0, 0));
                }
            });

            add(panelZNaglowkami, BorderLayout.NORTH);
            add(panelZeScrollem, BorderLayout.CENTER);
        }
        
        private JPanel wygnerujPanelZLista(List<String> lista, ZarzadcaBazaNeo4J.TypyWezlow typWezla) {
            JPanel panelOpakowujacy = new JPanel();
            panelOpakowujacy.setOpaque(false);
            panelOpakowujacy.setLayout(new BorderLayout());
            
            JPanel panelZLinkami = new JPanel();
            panelZLinkami.setOpaque(false);
            panelZLinkami.setLayout(new BoxLayout(panelZLinkami, BoxLayout.Y_AXIS));
            
            for (String element : lista) {
                dodajLink(panelZLinkami, element, typWezla);
            }

            panelOpakowujacy.add(panelZLinkami, BorderLayout.NORTH);
            
            return panelOpakowujacy;
        }
        
        private void dodajLink(JPanel panel, String tekst, ZarzadcaBazaNeo4J.TypyWezlow typWezla) {            
            JTextArea tekst1=new JTextArea(tekst);
            tekst1.setEditable(false);
            tekst1.setBackground(null);
            tekst1.setBorder(null);
            tekst1.setOpaque(false);
            tekst1.setFont(new Font("Verdana", Font.PLAIN, 15));
            tekst1.setForeground(Color.black);
            tekst1.setDisabledTextColor(Color.black);
            tekst1.setWrapStyleWord(true);
            tekst1.setLineWrap(true);
            
            tekst1.putClientProperty("typWezla", typWezla);
            tekst1.putClientProperty("nazwa", tekst);
            
            JFrame oknoGlowne = (JFrame) SwingUtilities.getWindowAncestor(this);
            tekst1.putClientProperty("okno", oknoGlowne);
            
            tekst1.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
            tekst1.addMouseListener(ZarzadcaLinkami.dajInstancje());
            
            panel.add(tekst1);
            panel.add(Box.createRigidArea(new Dimension(0,15)));
        }
        
        private void aktualizujNaglowki() {
            naglowekLewo.setText(NAGLOWEK_PO_LEWEJ_STRONIE + " (" + listaUzytkownikow.size() + ")");
            naglowekPrawo.setText(NAGLOWEK_PO_PRAWEJ_STRONIE + " (" + listaArtystow.size() + ")");
        }
        
        public void dodajNowyElement(ZarzadcaBazaNeo4J.TypyWezlow typWezla, Wezel wezel) {
            Component komponent;
            
            switch (typWezla) {
                case UZYTKOWNIK:
                    listaUzytkownikow.add(((Uzytkownik) wezel).getPseudonim());
                    System.out.println(panelZListaUzytkownikow.getComponentCount());
                    
                    komponent = panelZListaUzytkownikow.getComponent(0);
                    
                    if (komponent instanceof JPanel) {
                        dodajLink((JPanel) komponent, ((Uzytkownik) wezel).getPseudonim(), typWezla);
                        ((JPanel) komponent).revalidate();
                        aktualizujNaglowki();
                    };
                    
                    break;
                    
                case ARTYSTA:
                    listaArtystow.add(((Artysta) wezel).getNazwaZespolu());
                    
                    komponent = panelZListaArtystow.getComponent(0);
                    
                    if (komponent instanceof JPanel) {
                        dodajLink((JPanel) komponent, ((Artysta) wezel).getNazwaZespolu(), typWezla);
                        ((JPanel) komponent).revalidate();
                        aktualizujNaglowki();
                    };
                    
                    break;
            }
        }
        
        public void usunElement(ZarzadcaBazaNeo4J.TypyWezlow typWezla, Wezel wezel) {
            Component komponent;
            
            switch (typWezla) {
                case UZYTKOWNIK:
                    listaUzytkownikow.remove(((Uzytkownik) wezel).getPseudonim());

                    komponent = panelZListaUzytkownikow.getComponent(0);
                    
                    if (komponent instanceof JPanel) {
                        int numer = znajdzLink((JPanel) komponent, ((Uzytkownik) wezel).getPseudonim());

                        ((JPanel) komponent).remove(numer);
                        ((JPanel) komponent).remove(numer);
                        ((JPanel) komponent).revalidate();
                        aktualizujNaglowki();
                    };
                    
                    break;
                    
                case ARTYSTA:
                    listaArtystow.remove(((Artysta) wezel).getNazwaZespolu());

                    komponent = panelZListaArtystow.getComponent(0);
                    
                    if (komponent instanceof JPanel) {
                        int numer = znajdzLink((JPanel) komponent, ((Artysta) wezel).getNazwaZespolu());

                        ((JPanel) komponent).remove(numer);
                        ((JPanel) komponent).remove(numer);
                        ((JPanel) komponent).revalidate();
                        aktualizujNaglowki();
                    };
                    
                    break;
            }
        }
        
        private int znajdzLink(JPanel panel, String tekst) {
            int numer = -1;
            
            Component[] komponenty = panel.getComponents();
            for (int i = 0; i < komponenty.length; i++) {
                if(komponenty[i] instanceof JTextArea) {
                    JTextArea link = (JTextArea) komponenty[i];
                    
                    if (link.getText().equals(tekst)) {
                        numer = i;
                        break;
                    }
                }
            }
            
            return numer;
        }
    }
}
