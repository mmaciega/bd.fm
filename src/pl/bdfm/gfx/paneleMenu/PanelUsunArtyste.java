
package pl.bdfm.gfx.paneleMenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import static java.awt.Component.LEFT_ALIGNMENT;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import net.miginfocom.swing.MigLayout;
import pl.bdfm.gfx.OknoGlowne;
import pl.bdfm.logic.Artysta;
import pl.bdfm.logic.IObserwator;
import pl.bdfm.logic.ZarzadcaBazaNeo4J;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelUsunArtyste extends PanelOknaDialogowego implements ItemListener, ActionListener {
    private final JDialog oknoDialogowe;
    private final JPanel panelZElementami;
    
    private final JButton przyciskOK, przyciskCancel;
    
    private final JTextField krajPoleTekstowe;
    private final JLabel gatunekEtykieta;
    private final JComboBox nazwaPoleWyboru;
    private final JTextArea opisPoleTekstowe;
    private final List<JTextField> listaGatunkowPoleTekstowe;
    
    private Artysta artysta;
    
    public PanelUsunArtyste() {
        listaGatunkowPoleTekstowe = new ArrayList<>();
        
        oknoDialogowe = new JDialog(OknoGlowne.dajInstancje(), "Usuń artyste", true);
        oknoDialogowe.setLayout(new BorderLayout());
        oknoDialogowe.setResizable(false);
        
        panelZElementami = new JPanel();
        panelZElementami.setLayout(new MigLayout());
        
        panelZElementami.add(stworzEtykiete("Nazwa"));
        String[] tablicaOpcjiPolaWyboru = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA).toArray(new String[0]);
        nazwaPoleWyboru = new JComboBox(tablicaOpcjiPolaWyboru);
        panelZElementami.add(nazwaPoleWyboru, "growx, wrap");
        nazwaPoleWyboru.addItemListener(this);
        
        artysta = (Artysta) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA, String.valueOf(nazwaPoleWyboru.getSelectedItem()));
        
        panelZElementami.add(stworzEtykiete("Kraj pochodzenia"));
        krajPoleTekstowe = new JTextField("",18);
        krajPoleTekstowe.setEnabled(false);
        krajPoleTekstowe.setDisabledTextColor(Color.black);
        panelZElementami.add(krajPoleTekstowe, "span 2, growx, wrap");

        panelZElementami.add(stworzEtykiete("Opis"));

        opisPoleTekstowe = new JTextArea(6,18);
        opisPoleTekstowe.setEditable(true);
        opisPoleTekstowe.setBackground(Color.white);
        opisPoleTekstowe.setBorder(null);
        opisPoleTekstowe.setFont(new Font("Verdana", Font.PLAIN, 12));
        opisPoleTekstowe.setForeground(Color.black);
        opisPoleTekstowe.setWrapStyleWord(true);
        opisPoleTekstowe.setLineWrap(true);
        opisPoleTekstowe.setAlignmentX(LEFT_ALIGNMENT);
        opisPoleTekstowe.setCaretPosition(0);
        opisPoleTekstowe.setEnabled(false);
        opisPoleTekstowe.setDisabledTextColor(Color.black);

        JScrollPane panelZOpisem = new JScrollPane(opisPoleTekstowe);
        panelZOpisem.setBorder(new LineBorder(Color.gray));
        panelZOpisem.setAlignmentX(LEFT_ALIGNMENT);

        panelZElementami.add(panelZOpisem, "span 2, growx, wrap");
        
        gatunekEtykieta = stworzEtykiete("Gatunki");
        panelZElementami.add(gatunekEtykieta);
        
        for (String gatunek : artysta.getListaGatunkow()) {
            JTextField gatunekPoleTekstowe = new JTextField(gatunek);
            gatunekPoleTekstowe.setFont(new Font("Verdana", Font.PLAIN, 12));
            gatunekPoleTekstowe.setDisabledTextColor(Color.black);
            gatunekPoleTekstowe.setBackground(Color.white);
            gatunekPoleTekstowe.setEnabled(false);
            
            listaGatunkowPoleTekstowe.add(gatunekPoleTekstowe);
            
            String warunki = "growx, wrap";
            if(listaGatunkowPoleTekstowe.size() > 1) {
                warunki = "skip, growx, wrap";
            }
            
            panelZElementami.add(gatunekPoleTekstowe, warunki);
        }
        
        
        aktualizujElementyOkna();
        
        JPanel panelZPrzyciskami = new JPanel();
        przyciskOK = new JButton("OK");
        przyciskOK.addActionListener(this);
        przyciskCancel = new JButton("Cancel");
        przyciskCancel.addActionListener(this);
        panelZPrzyciskami.add(przyciskOK);
        panelZPrzyciskami.add(przyciskCancel);
        
        oknoDialogowe.add(panelZElementami, BorderLayout.CENTER);
        oknoDialogowe.add(panelZPrzyciskami, BorderLayout.SOUTH);
        oknoDialogowe.pack();
        oknoDialogowe.setLocationRelativeTo(OknoGlowne.dajInstancje());

    }
    
    private void aktualizujElementyOkna() {
        krajPoleTekstowe.setText(artysta.getKrajPochodzenia());
        opisPoleTekstowe.setText(artysta.getOpis());
        
        for (JTextField gatunek : listaGatunkowPoleTekstowe) {
            int numer = znajdzNumerKomponentu(gatunek, panelZElementami);
            panelZElementami.remove(numer);
        }
        
        listaGatunkowPoleTekstowe.clear();
        
        int numer = znajdzNumerKomponentu(gatunekEtykieta, panelZElementami);
        for (String gatunek : artysta.getListaGatunkow()) {
            JTextField wykonawcaPoleTekstowe = new JTextField(gatunek);
            wykonawcaPoleTekstowe.setFont(new Font("Verdana", Font.PLAIN, 12));
            wykonawcaPoleTekstowe.setDisabledTextColor(Color.black);
            wykonawcaPoleTekstowe.setBackground(Color.white);
            wykonawcaPoleTekstowe.setEnabled(false);
            
            listaGatunkowPoleTekstowe.add(wykonawcaPoleTekstowe);
            
            String warunki = "growx, wrap";
            if(listaGatunkowPoleTekstowe.size() > 1) {
                warunki = "skip, growx, wrap";
            }
            
            panelZElementami.add(wykonawcaPoleTekstowe, warunki, numer + (listaGatunkowPoleTekstowe.size()-1) + 1);
        }
        
        odswiezOknoDialogowe();
    }
    
    public void wyswietlOknoDialogowe() {
        oknoDialogowe.setVisible(true);
    }
    
    private void odswiezOknoDialogowe() {
        panelZElementami.revalidate();
        oknoDialogowe.pack();
    }
    
    @Override
    public void powiadomObserwatorow() {        
        System.out.println(artysta);   
        for (IObserwator obserwator : obserwatorzy) {
            obserwator.aktualizacjaWezla(TypyPowiadomienia.USUNIECIE, artysta);
        }    
        
        System.out.println("Powiadomienie");
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        Object obiekt = ae.getSource();
        
        if(obiekt instanceof JButton) {
            JButton przycisk = (JButton) obiekt;
            
            if (przycisk == przyciskCancel) {
                oknoDialogowe.setVisible(false);
            } else if (przycisk == przyciskOK) {
                powiadomObserwatorow();
                oknoDialogowe.setVisible(false);
            }
        }
    }


    private int znajdzNumerKomponentu(Object komponent, JPanel panel) {
        int numer = -1;
        int i = 0;
        
        Component[] komponenty = panel.getComponents();
        for (Component object : komponenty) {
                if(object == komponent) {
                    numer = i; 
                    break;
                }
                ++i;
        }
        
        return numer;
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if(ie.getSource() instanceof JComboBox) {
            JComboBox poleWyboru = (JComboBox) ie.getSource();
            
            if (ie.getStateChange() == ItemEvent.SELECTED) {
                artysta = (Artysta) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA, String.valueOf(poleWyboru.getSelectedItem()));
                System.out.println("Zmieniono combobox na " + artysta);
                
                aktualizujElementyOkna();
           }
        }
    }
    
}
