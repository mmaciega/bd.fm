
package pl.bdfm.gfx.paneleMenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import pl.bdfm.gfx.OknoGlowne;
import pl.bdfm.logic.Album;
import pl.bdfm.logic.IObserwator;
import pl.bdfm.logic.ZarzadcaBazaNeo4J;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelDodajAlbum  extends PanelOknaDialogowego implements ActionListener {
    private JDialog oknoDialogowe;
    private JPanel panelZElementami;
    
    private JButton przyciskOK, przyciskCancel;
    
    private JTextField nazwaPoleTekstowe, rokWydaniaPoleTekstowe, liczbaPoleTekstowe, 
            czasPoleTekstowe;
    private final List<JComboBox> listaWykonawcowPoleWyboru;
    private String[] tablicaOpcjiPolaWyboru;
    
    private final static String PUSTY_ELEMENT_POLA_WYBORU = "------";
    
    public PanelDodajAlbum() {
        listaWykonawcowPoleWyboru = new ArrayList<>();
        List<String> listaWykonawcow = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);
        listaWykonawcow.add(0, PUSTY_ELEMENT_POLA_WYBORU);
        tablicaOpcjiPolaWyboru = new String[listaWykonawcow.size()];
        tablicaOpcjiPolaWyboru = listaWykonawcow.toArray(new String[0]);
        
        generowanieOknaDialogowego(null);
    }
    
    public PanelDodajAlbum(String nazwaZespolu) {
        listaWykonawcowPoleWyboru = new ArrayList<>();
        List<String> listaWykonawcow = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);
        listaWykonawcow.add(0, PUSTY_ELEMENT_POLA_WYBORU);
        tablicaOpcjiPolaWyboru = new String[listaWykonawcow.size()];
        tablicaOpcjiPolaWyboru = listaWykonawcow.toArray(new String[0]);
        
        generowanieOknaDialogowego(nazwaZespolu);
    }
    
    private void generowanieOknaDialogowego(String nazwaZespolu) {
                oknoDialogowe = new JDialog(OknoGlowne.dajInstancje(), "Dodaj album", true);
        oknoDialogowe.setLayout(new BorderLayout());
        oknoDialogowe.setResizable(false);
        
        panelZElementami = new JPanel();
        panelZElementami.setLayout(new MigLayout());
        
        panelZElementami.add(stworzEtykiete("Nazwa albumu"));
        nazwaPoleTekstowe = new JTextField("",22);
        panelZElementami.add(nazwaPoleTekstowe, "wrap");
        
        panelZElementami.add(stworzEtykiete("Wykonawcy"));
        
        JComboBox wykonawcaPoleWyboru = new JComboBox(tablicaOpcjiPolaWyboru);
        wykonawcaPoleWyboru.setFont(new Font("Verdana", Font.PLAIN, 12));
        wykonawcaPoleWyboru.setBackground(Color.white);
        
        if (nazwaZespolu != null) {
            wykonawcaPoleWyboru.setSelectedItem(nazwaZespolu);
            wykonawcaPoleWyboru.setEnabled(false);
        }
        
        listaWykonawcowPoleWyboru.add(wykonawcaPoleWyboru);
        panelZElementami.add(wykonawcaPoleWyboru, "split 2, growx");

        JButton przycisk = new JButton("+");
        przycisk.setMargin(new Insets(0, 0, 0, 0));
        przycisk.addActionListener(this);
        panelZElementami.add(przycisk, "width 25px:pref, wrap");
        
        panelZElementami.add(stworzEtykiete("Liczba utworów"));
        liczbaPoleTekstowe = new JTextField("", 4);
        panelZElementami.add(liczbaPoleTekstowe, "span 2, wrap");

        panelZElementami.add(stworzEtykiete("Rok wydania"));
        rokWydaniaPoleTekstowe = new JTextField("", 4);
        panelZElementami.add(rokWydaniaPoleTekstowe, "split 2, wrap");

        panelZElementami.add(stworzEtykiete("Czas trwania"));
        czasPoleTekstowe = new JTextField("", 4);
        panelZElementami.add(czasPoleTekstowe, "split 2");
        
        JPanel panelZPrzyciskami = new JPanel();
        przyciskOK = new JButton("OK");
        przyciskOK.addActionListener(this);
        przyciskCancel = new JButton("Cancel");
        przyciskCancel.addActionListener(this);
        panelZPrzyciskami.add(przyciskOK);
        panelZPrzyciskami.add(przyciskCancel);
        
        oknoDialogowe.add(panelZElementami, BorderLayout.CENTER);
        oknoDialogowe.add(panelZPrzyciskami, BorderLayout.SOUTH);
        oknoDialogowe.pack();
        oknoDialogowe.setLocationRelativeTo(OknoGlowne.dajInstancje());
    }
    
    public void wyswietlOknoDialogowe() {
        oknoDialogowe.setVisible(true);
    }
    
    private void odswiezOknoDialogowe() {
        panelZElementami.revalidate();
        oknoDialogowe.pack();
    }
    
    @Override
    public void powiadomObserwatorow() {
        Album album = wygenerujAlbum();
        
        System.out.println(album);   
        for (IObserwator obserwator : obserwatorzy) {
            obserwator.aktualizacjaWezla(TypyPowiadomienia.DODANIE, album);
        }    
        
        System.out.println("Powiadomienie");
    }
    
    private Album wygenerujAlbum() {
        int rokWydania = 0;
        if (!rokWydaniaPoleTekstowe.getText().isEmpty()) {
            rokWydania = Integer.valueOf(rokWydaniaPoleTekstowe.getText().trim());
        }
        
        int liczbaUtworow = 0;
        if (!liczbaPoleTekstowe.getText().isEmpty()) {
            liczbaUtworow = Integer.valueOf(liczbaPoleTekstowe.getText().trim());
        }
        
        List<String> listaWykonawcow = new ArrayList();
        for (JComboBox wykonawcaPoleWyboru : listaWykonawcowPoleWyboru) {
            if (!String.valueOf(wykonawcaPoleWyboru.getSelectedItem()).equals(PUSTY_ELEMENT_POLA_WYBORU)
                    && !listaWykonawcow.contains(String.valueOf(wykonawcaPoleWyboru.getSelectedItem()))) {
                listaWykonawcow.add(String.valueOf(wykonawcaPoleWyboru.getSelectedItem()));
            }
        }
        
        Album album = new Album(nazwaPoleTekstowe.getText().trim(), 
                listaWykonawcow, 
                czasPoleTekstowe.getText().trim(), 
                rokWydania,
                liczbaUtworow);
        
        return album;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        Object obiekt = ae.getSource();  
        
        if(obiekt instanceof JButton) {
            JButton przycisk = (JButton) obiekt;
            
            if ((przycisk.getText().equals("+"))) {
                int numer = znajdzNumerKomponentu(obiekt, panelZElementami);
                
                JButton przyciskMinus = new JButton("-");
                przyciskMinus.addActionListener(this);
                przyciskMinus.setMargin(new Insets(0, 0, 0, 0));

                JComboBox wykonawcaPoleTekstowe = new JComboBox(tablicaOpcjiPolaWyboru);
                wykonawcaPoleTekstowe.setFont(new Font("Verdana", Font.PLAIN, 12));
                wykonawcaPoleTekstowe.setBackground(Color.white);

                panelZElementami.add(wykonawcaPoleTekstowe, "skip, split 2, growx", numer+((listaWykonawcowPoleWyboru.size()-1)*2)+1);
                panelZElementami.add(przyciskMinus, "width 25px:pref, wrap", numer+((listaWykonawcowPoleWyboru.size()-1)*2)+2);
                listaWykonawcowPoleWyboru.add(wykonawcaPoleTekstowe);
                
                odswiezOknoDialogowe();
            } else if ((przycisk.getText().equals("-"))) { 
                int numer = znajdzNumerKomponentu(obiekt, panelZElementami);

                listaWykonawcowPoleWyboru.remove(panelZElementami.getComponent(numer-1));
                
                panelZElementami.remove(numer);
                panelZElementami.remove(numer-1);
                odswiezOknoDialogowe();
            } else if (przycisk == przyciskCancel) {
                oknoDialogowe.setVisible(false);
            } else if (przycisk == przyciskOK) {
                powiadomObserwatorow();
                oknoDialogowe.setVisible(false);
            }
        }
    }
    
    private int znajdzNumerKomponentu(Object komponent, JPanel panel) {
        int numer = -1;
        int i = 0;
        
        Component[] komponenty = panel.getComponents();
        for (Component object : komponenty) {
                if(object == komponent) {
                    numer = i; 
                    break;
                }
                ++i;
        }
        
        return numer;
    }
    
}
