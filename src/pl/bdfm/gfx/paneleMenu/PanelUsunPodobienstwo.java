
package pl.bdfm.gfx.paneleMenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import static java.awt.Component.LEFT_ALIGNMENT;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import net.miginfocom.swing.MigLayout;
import org.neo4j.graphdb.Direction;
import pl.bdfm.gfx.OknoGlowne;
import pl.bdfm.logic.Artysta;
import pl.bdfm.logic.IObserwator;
import pl.bdfm.logic.ZarzadcaBazaNeo4J;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelUsunPodobienstwo extends PanelOknaDialogowego implements ItemListener, ActionListener {
    private final JDialog oknoDialogowe;
    private final JPanel panelZElementami[] = new JPanel[2];
    
    private JButton przyciskOK, przyciskCancel;
    
    private final JTextField[] krajPoleTekstowe = new JTextField[2];
    private final JLabel[] gatunekEtykieta = new JLabel[2];
    private JComboBox[] nazwaPoleWyboru = new JComboBox[2];
    private final JTextArea[] opisPoleTekstowe = new JTextArea[2];
    private final List<JTextField>[] listaGatunkowPoleTekstowe;
    

    private final List<String> listaArtystow;
    
    private final Artysta[] artysta = new Artysta[2];
    
    private String komunikat;
    private boolean czyBlad;
    
    private final int liczbaArtystow;
    
    public PanelUsunPodobienstwo() {
        super();
        this.listaGatunkowPoleTekstowe = (ArrayList<JTextField>[])new ArrayList[2];
        
        listaArtystow = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);
        
        listaGatunkowPoleTekstowe[0] = new ArrayList<>();
        listaGatunkowPoleTekstowe[1] = new ArrayList<>();
        
        liczbaArtystow = 2;
        oknoDialogowe = new JDialog(OknoGlowne.dajInstancje(), "Usuń relacje podobienstwa", true);
        oknoDialogowe.setLayout(new BorderLayout());
        oknoDialogowe.setResizable(false);
        
        JPanel panelCenter = new JPanel(new GridLayout(1, 2));
        panelCenter.setOpaque(false);
        
        panelZElementami[0] = wygenerujPanelZElementami(0, false);
        panelZElementami[1] = wygenerujPanelZElementami(1, true);
        
        aktualizujElementyOkna(0);
        aktualizujElementyOkna(1);
        
        JPanel panelZPrzyciskami = new JPanel();
        przyciskOK = new JButton("OK");
        przyciskOK.addActionListener(this);
        przyciskCancel = new JButton("Cancel");
        przyciskCancel.addActionListener(this);
        panelZPrzyciskami.add(przyciskOK);
        panelZPrzyciskami.add(przyciskCancel);
        
        panelCenter.add(panelZElementami[0]);
        panelCenter.add(panelZElementami[1]);
        
        oknoDialogowe.add(panelCenter, BorderLayout.CENTER);
        oknoDialogowe.add(panelZPrzyciskami, BorderLayout.SOUTH);
        oknoDialogowe.pack();
        oknoDialogowe.setLocationRelativeTo(OknoGlowne.dajInstancje());
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                nazwaPoleWyboru[0].setSelectedIndex(1);
                nazwaPoleWyboru[0].setSelectedIndex(0);
            }
        });
    }
    
    public PanelUsunPodobienstwo(String nazwa) {
        super();
        
        listaArtystow = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);
        this.listaGatunkowPoleTekstowe = (ArrayList<JTextField>[])new ArrayList[2];
        
        listaGatunkowPoleTekstowe[0] = new ArrayList<>();
        listaGatunkowPoleTekstowe[1] = new ArrayList<>();
        
        liczbaArtystow = 1;
        artysta[0] = (Artysta) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA, nazwa);
        
        oknoDialogowe = new JDialog(OknoGlowne.dajInstancje(), "Dodaj relacje podobienstwa dla " + nazwa, true);
        oknoDialogowe.setLayout(new BorderLayout());
        oknoDialogowe.setResizable(false);
        
        panelZElementami[1] = wygenerujPanelZElementami(1, true);

        
        if (panelZElementami[1] != null) {
            aktualizujElementyOkna(1);
            JPanel panelZPrzyciskami = new JPanel();
            przyciskOK = new JButton("OK");
            przyciskOK.addActionListener(this);
            przyciskCancel = new JButton("Cancel");
            przyciskCancel.addActionListener(this);
            panelZPrzyciskami.add(przyciskOK);
            panelZPrzyciskami.add(przyciskCancel);

            oknoDialogowe.add(panelZElementami[1], BorderLayout.CENTER);
            oknoDialogowe.add(panelZPrzyciskami, BorderLayout.SOUTH);
            oknoDialogowe.pack();
            oknoDialogowe.setLocationRelativeTo(OknoGlowne.dajInstancje());
        }
    }
    
    private JPanel wygenerujPanelZElementami(int indeks, boolean czyUstawic) {
        JPanel panel = new JPanel();
        panel.setLayout(new MigLayout());
        
        panel.add(stworzEtykiete("Nazwa"));
        
        String[] tablicaOpcjiPolaWyboru;
        if(czyUstawic) {
            List<String> listaElementow = artysta[0].getListaPodobnych();
            tablicaOpcjiPolaWyboru= listaElementow.toArray(new String[0]);
        } else {
            tablicaOpcjiPolaWyboru = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA).toArray(new String[0]);
        }
        
        if (tablicaOpcjiPolaWyboru.length != 0) {
        nazwaPoleWyboru[indeks] = new JComboBox(tablicaOpcjiPolaWyboru);        
        panel.add(nazwaPoleWyboru[indeks], "growx, wrap");
        nazwaPoleWyboru[indeks].addItemListener(this);
        
        artysta[indeks] = (Artysta) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA, String.valueOf(nazwaPoleWyboru[indeks].getSelectedItem()));
        
        panel.add(stworzEtykiete("Kraj pochodzenia"));
        krajPoleTekstowe[indeks] = new JTextField("",18);
        krajPoleTekstowe[indeks].setEnabled(false);
        krajPoleTekstowe[indeks].setDisabledTextColor(Color.black);
        panel.add(krajPoleTekstowe[indeks], "span 2, growx, wrap");

        panel.add(stworzEtykiete("Opis"));

        opisPoleTekstowe[indeks] = new JTextArea(6,18);
        opisPoleTekstowe[indeks].setEditable(true);
        opisPoleTekstowe[indeks].setBackground(Color.white);
        opisPoleTekstowe[indeks].setBorder(null);
        opisPoleTekstowe[indeks].setFont(new Font("Verdana", Font.PLAIN, 12));
        opisPoleTekstowe[indeks].setForeground(Color.black);
        opisPoleTekstowe[indeks].setWrapStyleWord(true);
        opisPoleTekstowe[indeks].setLineWrap(true);
        opisPoleTekstowe[indeks].setAlignmentX(LEFT_ALIGNMENT);
        opisPoleTekstowe[indeks].setCaretPosition(0);
        opisPoleTekstowe[indeks].setEnabled(false);
        opisPoleTekstowe[indeks].setDisabledTextColor(Color.black);

        JScrollPane panelZOpisem = new JScrollPane(opisPoleTekstowe[indeks]);
        panelZOpisem.setBorder(new LineBorder(Color.gray));
        panelZOpisem.setAlignmentX(LEFT_ALIGNMENT);

        panel.add(panelZOpisem, "span 2, growx, wrap");
        
        gatunekEtykieta[indeks] = stworzEtykiete("Gatunki");
        panel.add(gatunekEtykieta[indeks]);
        
        for (String gatunek : artysta[indeks].getListaGatunkow()) {
            JTextField gatunekPoleTekstowe = new JTextField(gatunek);
            gatunekPoleTekstowe.setFont(new Font("Verdana", Font.PLAIN, 12));
            gatunekPoleTekstowe.setDisabledTextColor(Color.black);
            gatunekPoleTekstowe.setBackground(Color.white);
            gatunekPoleTekstowe.setEnabled(false);
            
            listaGatunkowPoleTekstowe[indeks].add(gatunekPoleTekstowe);
            
            String warunki = "growx, wrap";
            if(listaGatunkowPoleTekstowe[indeks].size() > 1) {
                warunki = "skip, growx, wrap";
            }
           
            panel.add(gatunekPoleTekstowe, warunki);
        }
        
        return panel;
        } else {
            komunikat = "Brak podobnego zespołu do usunięcia";
            czyBlad = true;
            return null;
        }
    }
    
    public void wyswietlOknoDialogowe() {
        if (!czyBlad) {
            oknoDialogowe.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(OknoGlowne.dajInstancje(), komunikat, "Informacja", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    private void aktualizujElementyOkna(int indeks) {
        krajPoleTekstowe[indeks].setText(artysta[indeks].getKrajPochodzenia());
        opisPoleTekstowe[indeks].setText(artysta[indeks].getOpis());
        
        for (JTextField gatunek : listaGatunkowPoleTekstowe[indeks]) {
            int numer = znajdzNumerKomponentu(gatunek, panelZElementami[indeks]);
           
            if(numer != -1)
                panelZElementami[indeks].remove(numer);
        }
        
        listaGatunkowPoleTekstowe[indeks].clear();
        
        int numer = znajdzNumerKomponentu(gatunekEtykieta[indeks], panelZElementami[indeks]);
        for (String gatunek : artysta[indeks].getListaGatunkow()) {
            JTextField wykonawcaPoleTekstowe = new JTextField(gatunek);
            wykonawcaPoleTekstowe.setFont(new Font("Verdana", Font.PLAIN, 12));
            wykonawcaPoleTekstowe.setDisabledTextColor(Color.black);
            wykonawcaPoleTekstowe.setBackground(Color.white);
            wykonawcaPoleTekstowe.setEnabled(false);
            
            listaGatunkowPoleTekstowe[indeks].add(wykonawcaPoleTekstowe);
            
            String warunki = "growx, wrap";
            if(listaGatunkowPoleTekstowe[indeks].size() > 1) {
                warunki = "skip, growx, wrap";
            }
            
            panelZElementami[indeks].add(wykonawcaPoleTekstowe, warunki, numer + (listaGatunkowPoleTekstowe[indeks].size()-1) + 1);
        }
        
        odswiezOknoDialogowe(indeks);
    }
    
    private void wyczyscElementyOkna(int indeks) {
        krajPoleTekstowe[indeks].setText("");
        opisPoleTekstowe[indeks].setText("");
        
        for (JTextField gatunek : listaGatunkowPoleTekstowe[indeks]) {
            int numer = znajdzNumerKomponentu(gatunek, panelZElementami[indeks]);
           
            panelZElementami[indeks].remove(numer);
        }
    }
    
    private void odswiezOknoDialogowe(int indeks) {
        panelZElementami[indeks].revalidate();
        oknoDialogowe.pack();
    }
    
    private int znajdzNumerKomponentu(Object komponent, JPanel panel) {
        int numer = -1;
        int i = 0;
        
        Component[] komponenty = panel.getComponents();
        for (Component object : komponenty) {
                if(object == komponent) {
                    numer = i; 
                    break;
                }
                ++i;
        }
        
        return numer;
    }

    @Override
    public void powiadomObserwatorow() {        
        System.out.println(artysta[0]);
        System.out.println(artysta[1]);

        for (IObserwator obserwator : obserwatorzy) {
            obserwator.aktualizacjaRelacji(TypyPowiadomienia.USUNIECIE, ZarzadcaBazaNeo4J.TypyRelacji.PODOBNY, artysta[0], artysta[1], Direction.BOTH);
        }    
        
        System.out.println("Powiadomienie");
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if(ie.getSource() instanceof JComboBox) {
            JComboBox poleWyboru = (JComboBox) ie.getSource();
            
            if (ie.getStateChange() == ItemEvent.SELECTED) {
                int indeks;
                if (poleWyboru == nazwaPoleWyboru[0]) {
                    indeks = 0;
                    System.out.println("Jedynka");
                } else {
                    indeks = 1;
                    System.out.println("Dwojka");
                }
                
                artysta[indeks] = (Artysta) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA, String.valueOf(poleWyboru.getSelectedItem()));
                
                if (indeks == 0 ) {
                    nazwaPoleWyboru[1].removeAllItems();
                    for(String s: artysta[0].getListaPodobnych()){
                        nazwaPoleWyboru[1].addItem(s);
                    }
                    
                    if (artysta[0].getListaPodobnych().isEmpty()) {
                        wyczyscElementyOkna(1);
                    }
                } 
                
                aktualizujElementyOkna(indeks);
                
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        Object obiekt = ae.getSource();  
        
        if(obiekt instanceof JButton) {
            JButton przycisk = (JButton) obiekt;
            
            if (przycisk == przyciskCancel) {
                oknoDialogowe.setVisible(false);
            } else if (przycisk == przyciskOK) {
                if ( liczbaArtystow == 2 && String.valueOf(nazwaPoleWyboru[0].getSelectedItem()).equals( String.valueOf(nazwaPoleWyboru[1].getSelectedItem()) ) ) {
                    JOptionPane.showMessageDialog(null, "Zespół nie może być podobny do samego siebie.", "Błąd usuwania relacji podobieństwa", JOptionPane.ERROR_MESSAGE);
                } else if (nazwaPoleWyboru[1].getItemCount() == 0 ) {
                    JOptionPane.showMessageDialog(null, "Brak wybranego artysty do usunięcia", "Błąd usuwania relacji podobieństwa", JOptionPane.ERROR_MESSAGE);

                } else {
                    int odpowiedz = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć wybraną relację podobieństwa?", "Usuń relację podobieństwa", JOptionPane.YES_NO_OPTION);
                    if (odpowiedz == JOptionPane.YES_OPTION) {
                               powiadomObserwatorow();
                               oknoDialogowe.setVisible(false);
                    }
                }
            }
        }    
    }  
}
