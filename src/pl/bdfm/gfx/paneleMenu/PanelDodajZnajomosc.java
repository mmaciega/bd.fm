
package pl.bdfm.gfx.paneleMenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import net.miginfocom.swing.MigLayout;
import org.neo4j.graphdb.Direction;
import pl.bdfm.gfx.OknoGlowne;
import pl.bdfm.logic.IObserwator;
import pl.bdfm.logic.Uzytkownik;
import pl.bdfm.logic.ZarzadcaBazaNeo4J;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelDodajZnajomosc extends PanelOknaDialogowego implements ItemListener, ActionListener {
    private final JComboBox[] pseudonimPoleWyboru = new JComboBox[2];
    private final JTextField[] imiePoleTekstowe = new JTextField[2];
    private final JTextField[] nazwiskoPoleTekstowe = new JTextField[2];
    private final JTextField[] wiekPoleTekstowe = new JTextField[2];
    private final JTextField[] krajPochodzeniaPoleTekstowe = new JTextField[2];
    private final JComboBox[] plecPoleWyboru = new JComboBox[2];
    
    String[][] tablicaOpcjiPolaWyboru = new String[2][];
    private final List<String> listaUzytkownikow;
    
    private final JDialog oknoDialogowe;
    private JPanel panelZElementamiLewo, panelZElementamiPrawo;
    
    private final Uzytkownik[] uzytkownik = new Uzytkownik[2];
    
    private final JButton przyciskOK, przyciskCancel;
    
    private final int liczbaUzytkownikow;
    
    public PanelDodajZnajomosc() {
        super();
        
        liczbaUzytkownikow = 2;
        listaUzytkownikow = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK);
        oknoDialogowe = new JDialog(OknoGlowne.dajInstancje(), "Dodaj relacje znajomości", true);
        oknoDialogowe.setLayout(new BorderLayout());
        oknoDialogowe.setResizable(false);
        
        JPanel panelCenter = new JPanel(new GridLayout(1, 2));
        panelCenter.setOpaque(false);
        
        panelZElementamiLewo = wygenerujPanelZElementami(0, false);
        panelZElementamiPrawo = wygenerujPanelZElementami(1, false);
        
        aktualizujElementyOkna(0);
        aktualizujElementyOkna(1);
        
        JPanel panelZPrzyciskami = new JPanel();
        przyciskOK = new JButton("OK");
        przyciskOK.addActionListener(this);
        przyciskCancel = new JButton("Cancel");
        przyciskCancel.addActionListener(this);
        panelZPrzyciskami.add(przyciskOK);
        panelZPrzyciskami.add(przyciskCancel);
        
        panelCenter.add(panelZElementamiLewo);
        panelCenter.add(panelZElementamiPrawo);
        
        oknoDialogowe.add(panelCenter, BorderLayout.CENTER);
        oknoDialogowe.add(panelZPrzyciskami, BorderLayout.SOUTH);
        oknoDialogowe.pack();
        oknoDialogowe.setLocationRelativeTo(OknoGlowne.dajInstancje());
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                pseudonimPoleWyboru[0].setSelectedIndex(1);
                pseudonimPoleWyboru[0].setSelectedIndex(0);
            }
        });
    }
    
    public PanelDodajZnajomosc(String pseudonim) {
        super();
        
        liczbaUzytkownikow = 1;
        listaUzytkownikow = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK);
        oknoDialogowe = new JDialog(OknoGlowne.dajInstancje(), "Dodaj relacje znajomości dla " + pseudonim, true);
        oknoDialogowe.setLayout(new BorderLayout());
        oknoDialogowe.setResizable(false);
        
        uzytkownik[0] = (Uzytkownik) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK, pseudonim);
        
        for (String string : uzytkownik[0].getListaZnajomych()) {
            System.out.print(string + ", ");
        }
                
        
        panelZElementamiLewo = wygenerujPanelZElementami(1, true);
        aktualizujElementyOkna(1);
        
        JPanel panelZPrzyciskami = new JPanel();
        przyciskOK = new JButton("OK");
        przyciskOK.addActionListener(this);
        przyciskCancel = new JButton("Cancel");
        przyciskCancel.addActionListener(this);
        panelZPrzyciskami.add(przyciskOK);
        panelZPrzyciskami.add(przyciskCancel);

        oknoDialogowe.add(panelZElementamiLewo, BorderLayout.CENTER);
        oknoDialogowe.add(panelZPrzyciskami, BorderLayout.SOUTH);
        oknoDialogowe.pack();
        oknoDialogowe.setLocationRelativeTo(OknoGlowne.dajInstancje());
    }
    
    private JPanel wygenerujPanelZElementami(int indeks, boolean czyUsunac) {
        JPanel panelZElementami = new JPanel();
        panelZElementami.setLayout(new MigLayout());
        panelZElementami.setOpaque(false);

        panelZElementami.add(stworzEtykiete("Pseudonim"));
        
        if(czyUsunac) {
            List<String> listaElementow = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK);
            listaElementow.removeAll(uzytkownik[0].getListaZnajomych());
            listaElementow.remove(uzytkownik[0].getPseudonim());
            tablicaOpcjiPolaWyboru[indeks] = listaElementow.toArray(new String[0]);
        } else {
            tablicaOpcjiPolaWyboru[indeks] = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK).toArray(new String[indeks]);
        }
        
        pseudonimPoleWyboru[indeks] = new JComboBox(tablicaOpcjiPolaWyboru[indeks]);
        panelZElementami.add(pseudonimPoleWyboru[indeks], "span, growx, wrap");
        pseudonimPoleWyboru[indeks].addItemListener(this);
        
        uzytkownik[indeks] = (Uzytkownik) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK, String.valueOf(pseudonimPoleWyboru[indeks].getSelectedItem()));

        imiePoleTekstowe[indeks] = new JTextField();
        panelZElementami.add(stworzEtykiete("Imie"));
        panelZElementami.add(imiePoleTekstowe[indeks], "span, growx, wrap");
        imiePoleTekstowe[indeks].setEnabled(false);
        imiePoleTekstowe[indeks].setDisabledTextColor(Color.black);

        nazwiskoPoleTekstowe[indeks] = new JTextField("");
        panelZElementami.add(stworzEtykiete("Nazwisko"));
        panelZElementami.add(nazwiskoPoleTekstowe[indeks], "span, growx, wrap");
        nazwiskoPoleTekstowe[indeks].setEnabled(false);
        nazwiskoPoleTekstowe[indeks].setDisabledTextColor(Color.black);

        wiekPoleTekstowe[indeks] = new JTextField("", 4);
        panelZElementami.add(stworzEtykiete("Wiek"));
        panelZElementami.add(wiekPoleTekstowe[indeks]);
        wiekPoleTekstowe[indeks].setEnabled(false);
        wiekPoleTekstowe[indeks].setDisabledTextColor(Color.black);

        plecPoleWyboru[indeks] = new JComboBox(Uzytkownik.zwrocTablicaLancuchowPlci());
        plecPoleWyboru[indeks].setFont(new Font("Verdana", Font.PLAIN, 12));
        plecPoleWyboru[indeks].setBackground(Color.white);
        panelZElementami.add(stworzEtykiete("Płeć"), "right");
        panelZElementami.add(plecPoleWyboru[indeks], "wrap");
        plecPoleWyboru[indeks].setEnabled(false);

        krajPochodzeniaPoleTekstowe[indeks] = new JTextField("");
        panelZElementami.add(stworzEtykiete("Kraj pochodzenia"));
        panelZElementami.add(krajPochodzeniaPoleTekstowe[indeks], "span, growx");
        krajPochodzeniaPoleTekstowe[indeks].setEnabled(false);
        krajPochodzeniaPoleTekstowe[indeks].setDisabledTextColor(Color.black);
        
        return panelZElementami;
    }
    
    public void wyswietlOknoDialogowe() {
        oknoDialogowe.setVisible(true);
    }
    
    private void aktualizujElementyOkna(int indeks) {
        imiePoleTekstowe[indeks].setText(uzytkownik[indeks].getImie());
        nazwiskoPoleTekstowe[indeks].setText(uzytkownik[indeks].getNazwisko());
        krajPochodzeniaPoleTekstowe[indeks].setText(uzytkownik[indeks].getKrajPochodzenia());
        wiekPoleTekstowe[indeks].setText(String.valueOf( uzytkownik[indeks].getWiek() ).equals("0") ? "" : String.valueOf( uzytkownik[indeks].getWiek() ));
        plecPoleWyboru[indeks].setSelectedItem(uzytkownik[indeks].getPlec().dajLancuch());
    }

    @Override
    public void powiadomObserwatorow() {        
        System.out.println(uzytkownik[0]);
        System.out.println(uzytkownik[1]);

        for (IObserwator obserwator : obserwatorzy) {
            obserwator.aktualizacjaRelacji(TypyPowiadomienia.DODANIE, ZarzadcaBazaNeo4J.TypyRelacji.ZNA, uzytkownik[0], uzytkownik[1], Direction.BOTH);
        }    
        
        System.out.println("Powiadomienie");
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if(ie.getSource() instanceof JComboBox) {
            JComboBox poleWyboru = (JComboBox) ie.getSource();
            
            if (ie.getStateChange() == ItemEvent.SELECTED) {
                int indeks;
                if (poleWyboru == pseudonimPoleWyboru[0]) {
                    indeks = 0;
                    System.out.println("Jedynka");
                } else {
                    indeks = 1;
                    System.out.println("Dwojka");
                }
                
                uzytkownik[indeks] = (Uzytkownik) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK, String.valueOf(poleWyboru.getSelectedItem()));
                
                if (indeks == 0 ) {
                    List<String> tymczasLista = new ArrayList<>(listaUzytkownikow);
                    tymczasLista.removeAll(uzytkownik[0].getListaZnajomych());
                    tymczasLista.remove(uzytkownik[0].getPseudonim());
                    
                    pseudonimPoleWyboru[1].removeAllItems();
                    for(String s: tymczasLista){
                        pseudonimPoleWyboru[1].addItem(s);
                    }
                    tablicaOpcjiPolaWyboru[1] = tymczasLista.toArray(new String[0]);
                } 
                
                aktualizujElementyOkna(indeks);
                
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        Object obiekt = ae.getSource();  
        
        if(obiekt instanceof JButton) {
            JButton przycisk = (JButton) obiekt;
            
            if (przycisk == przyciskCancel) {
                oknoDialogowe.setVisible(false);
            } else if (przycisk == przyciskOK) {
                if ( liczbaUzytkownikow == 2 && String.valueOf(pseudonimPoleWyboru[0].getSelectedItem()).equals( String.valueOf(pseudonimPoleWyboru[1].getSelectedItem()) ) ) {
                    JOptionPane.showMessageDialog(null, "Nie można być znajomym dla siebie samego", "Błąd tworzenia relacji znajomości", JOptionPane.ERROR_MESSAGE);
                    } else {
                           powiadomObserwatorow();
                           oknoDialogowe.setVisible(false);
                    }
            }
        }    
    }  
}
