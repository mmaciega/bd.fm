
package pl.bdfm.gfx.paneleMenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import static java.awt.Component.LEFT_ALIGNMENT;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import net.miginfocom.swing.MigLayout;
import org.neo4j.graphdb.Direction;
import pl.bdfm.gfx.OknoGlowne;
import pl.bdfm.logic.Artysta;
import pl.bdfm.logic.IObserwator;
import pl.bdfm.logic.Uzytkownik;
import pl.bdfm.logic.ZarzadcaBazaNeo4J;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelDodajPolubienie extends PanelOknaDialogowego implements ItemListener, ActionListener {
    private final JDialog oknoDialogowe;
    private final JPanel panelZElementami[] = new JPanel[2];
    
    private JButton przyciskOK, przyciskCancel;
    
    private JTextField krajPoleTekstowe;
    private JLabel gatunekEtykieta;
    private JComboBox nazwaPoleWyboru;
    private JTextArea opisPoleTekstowe;
    private final List<JTextField> listaGatunkowPoleTekstowe;
    
////////////////////////////////////////////

    private JComboBox pseudonimPoleWyboru;
    private JTextField imiePoleTekstowe;
    private JTextField nazwiskoPoleTekstowe;
    private JTextField wiekPoleTekstowe;
    private JTextField krajPochodzeniaPoleTekstowe;
    private JComboBox plecPoleWyboru;
 String[] tablicaOpcjiPolaWyboru;
    
    private Artysta artysta;
    private Uzytkownik uzytkownik;
    
    private String komunikat;
    private boolean czyBlad;

    private final List<String> listaArtystow;
    
    private final int liczbaFormularzy;
    
    public PanelDodajPolubienie() {
        super();
        
        listaGatunkowPoleTekstowe = new ArrayList<>();
        
        listaArtystow = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);

        
        liczbaFormularzy = 2;
        oknoDialogowe = new JDialog(OknoGlowne.dajInstancje(), "Dodaj relacje polubienia", true);
        oknoDialogowe.setLayout(new BorderLayout());
        oknoDialogowe.setResizable(false);
        
        JPanel panelCenter = new JPanel(new GridLayout(1, 2));
        panelCenter.setOpaque(false);
        
        panelZElementami[0] = wygenerujPanelZElementamiUzytkownika();
        panelZElementami[1] = wygenerujPanelZElementamiArtysty(false);
        
        aktualizujElementyUzytkownika();
        aktualizujElementyArtysty();
        
        JPanel panelZPrzyciskami = new JPanel();
        przyciskOK = new JButton("OK");
        przyciskOK.addActionListener(this);
        przyciskCancel = new JButton("Cancel");
        przyciskCancel.addActionListener(this);
        panelZPrzyciskami.add(przyciskOK);
        panelZPrzyciskami.add(przyciskCancel);
        
        panelCenter.add(panelZElementami[0]);
        panelCenter.add(panelZElementami[1]);
        
        oknoDialogowe.add(panelCenter, BorderLayout.CENTER);
        oknoDialogowe.add(panelZPrzyciskami, BorderLayout.SOUTH);
        oknoDialogowe.pack();
        oknoDialogowe.setLocationRelativeTo(OknoGlowne.dajInstancje());
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                pseudonimPoleWyboru.setSelectedIndex(1);
                pseudonimPoleWyboru.setSelectedIndex(0);
            }
        });
    }
    
    public PanelDodajPolubienie(String pseudonim) {
        super();
        listaGatunkowPoleTekstowe = new ArrayList<>();
        
        liczbaFormularzy = 1;
        listaArtystow = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);

        uzytkownik = (Uzytkownik) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK, pseudonim);
        
        oknoDialogowe = new JDialog(OknoGlowne.dajInstancje(), "Dodaj relacje polubienia dla " + pseudonim, true);
        oknoDialogowe.setLayout(new BorderLayout());
        oknoDialogowe.setResizable(false);
        
        panelZElementami[1] = wygenerujPanelZElementamiArtysty(true);
        
        if (panelZElementami[1] != null) {
            aktualizujElementyArtysty();

            JPanel panelZPrzyciskami = new JPanel();
            przyciskOK = new JButton("OK");
            przyciskOK.addActionListener(this);
            przyciskCancel = new JButton("Cancel");
            przyciskCancel.addActionListener(this);
            panelZPrzyciskami.add(przyciskOK);
            panelZPrzyciskami.add(przyciskCancel);

            oknoDialogowe.add(panelZElementami[1], BorderLayout.CENTER);
            oknoDialogowe.add(panelZPrzyciskami, BorderLayout.SOUTH);
            oknoDialogowe.pack();
            oknoDialogowe.setLocationRelativeTo(OknoGlowne.dajInstancje());
        }
    }
    
    private JPanel wygenerujPanelZElementamiUzytkownika() {
        JPanel panelZElementami = new JPanel();
        panelZElementami.setLayout(new MigLayout());
        panelZElementami.setOpaque(false);

        panelZElementami.add(stworzEtykiete("Pseudonim"));

        tablicaOpcjiPolaWyboru = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK).toArray(new String[0]);
        pseudonimPoleWyboru = new JComboBox(tablicaOpcjiPolaWyboru);
        panelZElementami.add(pseudonimPoleWyboru, "span, growx, wrap");
        pseudonimPoleWyboru.addItemListener(this);

        uzytkownik = (Uzytkownik) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK, String.valueOf(pseudonimPoleWyboru.getSelectedItem()));

        imiePoleTekstowe = new JTextField();
        panelZElementami.add(stworzEtykiete("Imie"));
        panelZElementami.add(imiePoleTekstowe, "span, growx, wrap");
        imiePoleTekstowe.setEnabled(false);
        imiePoleTekstowe.setDisabledTextColor(Color.black);

        nazwiskoPoleTekstowe = new JTextField("");
        panelZElementami.add(stworzEtykiete("Nazwisko"));
        panelZElementami.add(nazwiskoPoleTekstowe, "span, growx, wrap");
        nazwiskoPoleTekstowe.setEnabled(false);
        nazwiskoPoleTekstowe.setDisabledTextColor(Color.black);

        wiekPoleTekstowe = new JTextField("", 4);
        panelZElementami.add(stworzEtykiete("Wiek"));
        panelZElementami.add(wiekPoleTekstowe);
        wiekPoleTekstowe.setEnabled(false);
        wiekPoleTekstowe.setDisabledTextColor(Color.black);

        plecPoleWyboru = new JComboBox(Uzytkownik.zwrocTablicaLancuchowPlci());
        plecPoleWyboru.setFont(new Font("Verdana", Font.PLAIN, 12));
        plecPoleWyboru.setBackground(Color.white);
        panelZElementami.add(stworzEtykiete("Płeć"), "right");
        panelZElementami.add(plecPoleWyboru, "wrap");
        plecPoleWyboru.setEnabled(false);

        krajPochodzeniaPoleTekstowe = new JTextField("");
        panelZElementami.add(stworzEtykiete("Kraj pochodzenia"));
        panelZElementami.add(krajPochodzeniaPoleTekstowe, "span, growx");
        krajPochodzeniaPoleTekstowe.setEnabled(false);
        krajPochodzeniaPoleTekstowe.setDisabledTextColor(Color.black);

        return panelZElementami;
    }
    
    private JPanel wygenerujPanelZElementamiArtysty(boolean czyUsunac) {
        JPanel panel = new JPanel();
        panel.setLayout(new MigLayout());
        
        panel.add(stworzEtykiete("Nazwa"));
        
        if(czyUsunac) {
            List<String> listaElementow = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA);
            listaElementow.removeAll(uzytkownik.getListaUlubionych());
            tablicaOpcjiPolaWyboru = listaElementow.toArray(new String[0]);
        } else {
            tablicaOpcjiPolaWyboru = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA).toArray(new String[0]);
        }
        
        if (tablicaOpcjiPolaWyboru.length != 0) {
        nazwaPoleWyboru = new JComboBox(tablicaOpcjiPolaWyboru);        
        panel.add(nazwaPoleWyboru, "growx, wrap");
        nazwaPoleWyboru.addItemListener(this);
        
        artysta = (Artysta) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA, String.valueOf(nazwaPoleWyboru.getSelectedItem()));
        
        panel.add(stworzEtykiete("Kraj pochodzenia"));
        krajPoleTekstowe = new JTextField("",18);
        krajPoleTekstowe.setEnabled(false);
        krajPoleTekstowe.setDisabledTextColor(Color.black);
        panel.add(krajPoleTekstowe, "span 2, growx, wrap");

        panel.add(stworzEtykiete("Opis"));

        opisPoleTekstowe = new JTextArea(6,18);
        opisPoleTekstowe.setEditable(true);
        opisPoleTekstowe.setBackground(Color.white);
        opisPoleTekstowe.setBorder(null);
        opisPoleTekstowe.setFont(new Font("Verdana", Font.PLAIN, 12));
        opisPoleTekstowe.setForeground(Color.black);
        opisPoleTekstowe.setWrapStyleWord(true);
        opisPoleTekstowe.setLineWrap(true);
        opisPoleTekstowe.setAlignmentX(LEFT_ALIGNMENT);
        opisPoleTekstowe.setCaretPosition(0);
        opisPoleTekstowe.setEnabled(false);
        opisPoleTekstowe.setDisabledTextColor(Color.black);

        JScrollPane panelZOpisem = new JScrollPane(opisPoleTekstowe);
        panelZOpisem.setBorder(new LineBorder(Color.gray));
        panelZOpisem.setAlignmentX(LEFT_ALIGNMENT);

        panel.add(panelZOpisem, "span 2, growx, wrap");
        
        gatunekEtykieta = stworzEtykiete("Gatunki");
        panel.add(gatunekEtykieta);
        
        for (String gatunek : artysta.getListaGatunkow()) {
            JTextField gatunekPoleTekstowe = new JTextField(gatunek);
            gatunekPoleTekstowe.setFont(new Font("Verdana", Font.PLAIN, 12));
            gatunekPoleTekstowe.setDisabledTextColor(Color.black);
            gatunekPoleTekstowe.setBackground(Color.white);
            gatunekPoleTekstowe.setEnabled(false);
            
            listaGatunkowPoleTekstowe.add(gatunekPoleTekstowe);
            
            String warunki = "growx, wrap";
            if(listaGatunkowPoleTekstowe.size() > 1) {
                warunki = "skip, growx, wrap";
            }
           
            panel.add(gatunekPoleTekstowe, warunki);
        }
        
        return panel;
        } else {
            komunikat = "Brak zespołów do polubienia";
            czyBlad = true;
            return null;
        }
    }
    
    public void wyswietlOknoDialogowe() {
        if (!czyBlad) {
            oknoDialogowe.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(OknoGlowne.dajInstancje(), komunikat, "Informacja", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    private void aktualizujElementyUzytkownika() {
        imiePoleTekstowe.setText(uzytkownik.getImie());
        nazwiskoPoleTekstowe.setText(uzytkownik.getNazwisko());
        krajPochodzeniaPoleTekstowe.setText(uzytkownik.getKrajPochodzenia());
        wiekPoleTekstowe.setText(String.valueOf( uzytkownik.getWiek() ).equals("0") ? "" : String.valueOf( uzytkownik.getWiek() ));
        plecPoleWyboru.setSelectedItem(uzytkownik.getPlec().dajLancuch());
    }
    
    private void aktualizujElementyArtysty() {
        krajPoleTekstowe.setText(artysta.getKrajPochodzenia());
        opisPoleTekstowe.setText(artysta.getOpis());
        
        for (JTextField gatunek : listaGatunkowPoleTekstowe) {
            int numer = znajdzNumerKomponentu(gatunek, panelZElementami[1]);
           
            panelZElementami[1].remove(numer);
        }
        
        listaGatunkowPoleTekstowe.clear();
        
        int numer = znajdzNumerKomponentu(gatunekEtykieta, panelZElementami[1]);
        for (String gatunek : artysta.getListaGatunkow()) {
            JTextField wykonawcaPoleTekstowe = new JTextField(gatunek);
            wykonawcaPoleTekstowe.setFont(new Font("Verdana", Font.PLAIN, 12));
            wykonawcaPoleTekstowe.setDisabledTextColor(Color.black);
            wykonawcaPoleTekstowe.setBackground(Color.white);
            wykonawcaPoleTekstowe.setEnabled(false);
            
            listaGatunkowPoleTekstowe.add(wykonawcaPoleTekstowe);
            
            String warunki = "growx, wrap";
            if(listaGatunkowPoleTekstowe.size() > 1) {
                warunki = "skip, growx, wrap";
            }
            
            panelZElementami[1].add(wykonawcaPoleTekstowe, warunki, numer + (listaGatunkowPoleTekstowe.size()-1) + 1);
        }
        
        odswiezOknoDialogowe(1);
    }
    
    private void odswiezOknoDialogowe(int indeks) {
        panelZElementami[indeks].revalidate();
        oknoDialogowe.pack();
    }
    
    private int znajdzNumerKomponentu(Object komponent, JPanel panel) {
        int numer = -1;
        int i = 0;
        
        Component[] komponenty = panel.getComponents();
        for (Component object : komponenty) {
                if(object == komponent) {
                    numer = i; 
                    break;
                }
                ++i;
        }
        
        return numer;
    }

    @Override
    public void powiadomObserwatorow() {        
        System.out.println(uzytkownik);
        System.out.println(artysta);

        for (IObserwator obserwator : obserwatorzy) {
            obserwator.aktualizacjaRelacji(TypyPowiadomienia.DODANIE, ZarzadcaBazaNeo4J.TypyRelacji.LUBI, uzytkownik, artysta, Direction.INCOMING);
        }    
        
        System.out.println("Powiadomienie");
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if(ie.getSource() instanceof JComboBox) {
            JComboBox poleWyboru = (JComboBox) ie.getSource();
            
            if (ie.getStateChange() == ItemEvent.SELECTED) {
                int indeks;
                if (poleWyboru == pseudonimPoleWyboru) {
                    uzytkownik = (Uzytkownik) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK, String.valueOf(poleWyboru.getSelectedItem()));

                    aktualizujElementyUzytkownika();
                    indeks = 0;
                    System.out.println("Jedynka");
                } else {
                    artysta = (Artysta) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA, String.valueOf(poleWyboru.getSelectedItem()));

                    aktualizujElementyArtysty();
                    indeks = 1;
                    System.out.println("Dwojka");
                }
                                
                if (indeks == 0 ) {
                    List<String> tymczasLista = new ArrayList<>(listaArtystow);
                    tymczasLista.removeAll(uzytkownik.getListaUlubionych());
                    
                    nazwaPoleWyboru.removeAllItems();
                    for(String s: tymczasLista){
                        nazwaPoleWyboru.addItem(s);
                    }
                } 
                
                aktualizujElementyArtysty();
                
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        Object obiekt = ae.getSource();  
        
        if(obiekt instanceof JButton) {
            JButton przycisk = (JButton) obiekt;
            
            if (przycisk == przyciskCancel) {
                oknoDialogowe.setVisible(false);
            } else if (przycisk == przyciskOK) {
                powiadomObserwatorow();
                oknoDialogowe.setVisible(false);
            }
        }    
    }  
}
