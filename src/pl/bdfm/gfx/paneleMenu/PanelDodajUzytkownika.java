
package pl.bdfm.gfx.paneleMenu;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import pl.bdfm.logic.FunkcjeTekstowe;
import pl.bdfm.logic.IObserwator;
import pl.bdfm.logic.Uzytkownik;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelDodajUzytkownika extends PanelOknaDialogowego {
    private final JTextField pseudonimPoleTekstowe;
    private final JTextField imiePoleTekstowe;
    private final JTextField nazwiskoPoleTekstowe;
    private final JTextField wiekPoleTekstowe;
    private final JTextField krajPochodzeniaPoleTekstowe;
    private final JComboBox plecPoleWyboru;
    
    public PanelDodajUzytkownika() {
        super();
        
        setLayout(new MigLayout());

        add(stworzEtykiete("Pseudonim"));
        pseudonimPoleTekstowe = new JTextField("");
        add(pseudonimPoleTekstowe, "span, growx, wrap");

        imiePoleTekstowe = new JTextField("");
        add(stworzEtykiete("Imie"));
        add(imiePoleTekstowe, "span, growx, wrap");

        nazwiskoPoleTekstowe = new JTextField("");
        add(stworzEtykiete("Nazwisko"));
        add(nazwiskoPoleTekstowe, "span, growx, wrap");

        wiekPoleTekstowe = new JTextField("", 4);
        add(stworzEtykiete("Wiek"));
        add(wiekPoleTekstowe);

        plecPoleWyboru = new JComboBox(Uzytkownik.zwrocTablicaLancuchowPlci());
        plecPoleWyboru.setFont(new Font("Verdana", Font.PLAIN, 12));
        plecPoleWyboru.setBackground(Color.white);
        add(stworzEtykiete("Płeć"), "right");
        add(plecPoleWyboru, "wrap");

        krajPochodzeniaPoleTekstowe = new JTextField("");
        add(stworzEtykiete("Kraj pochodzenia"));
        add(krajPochodzeniaPoleTekstowe, "span, growx");
    }
    


    @Override
    public void powiadomObserwatorow() {
        Uzytkownik uzytkownik = wygenerujUzytkownika();
        
        System.out.println(uzytkownik);
        
        for (IObserwator obserwator : obserwatorzy) {
            obserwator.aktualizacjaWezla(TypyPowiadomienia.DODANIE, uzytkownik);
        }    
        
        System.out.println("Powiadomienie");
    }
    
    private Uzytkownik wygenerujUzytkownika() {
        String krajPochodzenia = "";
        if (!krajPochodzeniaPoleTekstowe.getText().isEmpty()) {
            krajPochodzenia = FunkcjeTekstowe.duzeLiteryKazdegoSlowa(krajPochodzeniaPoleTekstowe.getText());
        }
        
        int wiek = 0;
        if (!wiekPoleTekstowe.getText().isEmpty()) {
            wiek = Integer.valueOf(wiekPoleTekstowe.getText());
        }
        
        Uzytkownik uzytkownik = new Uzytkownik(pseudonimPoleTekstowe.getText().trim(),
                imiePoleTekstowe.getText().trim(), 
                nazwiskoPoleTekstowe.getText().trim(), 
                krajPochodzenia,
                wiek, 
                Uzytkownik.znajdzPlec((String) plecPoleWyboru.getSelectedItem()));
        
        return uzytkownik;
    }
    
}
