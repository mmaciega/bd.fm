
package pl.bdfm.gfx.paneleMenu;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import pl.bdfm.logic.IObserwator;
import pl.bdfm.logic.IObserwowany;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public abstract class PanelOknaDialogowego extends JPanel implements IObserwowany {
    protected final List<IObserwator> obserwatorzy = new ArrayList<IObserwator>();
    
    /**
     * Metoda pozwalająca na zarejetrowanie obserwatora
     * @param obserwator Obiekt będący obserwatorem
     */
    @Override
    public void zarejestrujObserwatora(IObserwator obserwator) {
        obserwatorzy.add(obserwator);
    }

    /**
     * Metoda pozwalająca na wyrejestrowanie obserwatora
     * @param obserwator Obiekt będący obserwatorem
     */
    @Override
    public void wyrejestrujObserwatora(IObserwator obserwator) {
        int i = obserwatorzy.indexOf(obserwator);
        if (i != 0)
            obserwatorzy.remove(i);
    }

    @Override
    public abstract void powiadomObserwatorow();
    
    protected JLabel stworzEtykiete(String tresc) {
        JLabel etykieta = new JLabel(tresc);
        etykieta.setFont(new Font("Verdana", Font.PLAIN, 12));
        return etykieta;
    }
    
    public void zarejestrujObserwatorow(Object[] obserwatorzy) {
        for (Object object : obserwatorzy) {
            if(object instanceof IObserwator) {
                zarejestrujObserwatora((IObserwator) object);
            }   
        }
    }
}
