
package pl.bdfm.gfx.paneleMenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import pl.bdfm.gfx.OknoGlowne;
import pl.bdfm.logic.IObserwator;
import pl.bdfm.logic.Uzytkownik;
import pl.bdfm.logic.ZarzadcaBazaNeo4J;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelUsunUzytkownika extends PanelOknaDialogowego implements ItemListener, ActionListener {
    private final JComboBox pseudonimPoleWyboru;
    private final JTextField imiePoleTekstowe;
    private final JTextField nazwiskoPoleTekstowe;
    private final JTextField wiekPoleTekstowe;
    private final JTextField krajPochodzeniaPoleTekstowe;
    private final JComboBox plecPoleWyboru;
    
    private final JDialog oknoDialogowe;
    private final JPanel panelZElementami;
    
    private Uzytkownik uzytkownik;
    
    private final JButton przyciskOK, przyciskCancel;
    
    public PanelUsunUzytkownika() {
        super();
        
        oknoDialogowe = new JDialog(OknoGlowne.dajInstancje(), "Usuń użytkownika", true);
        oknoDialogowe.setLayout(new BorderLayout());
        oknoDialogowe.setResizable(false);
        
        panelZElementami = new JPanel();
        panelZElementami.setLayout(new MigLayout());

        panelZElementami.add(stworzEtykiete("Pseudonim"));
        String[] tablicaOpcjiPolaWyboru = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK).toArray(new String[0]);
        pseudonimPoleWyboru = new JComboBox(tablicaOpcjiPolaWyboru);
        panelZElementami.add(pseudonimPoleWyboru, "span, growx, wrap");
        pseudonimPoleWyboru.addItemListener(this);
        
        uzytkownik = (Uzytkownik) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK, String.valueOf(pseudonimPoleWyboru.getSelectedItem()));

        imiePoleTekstowe = new JTextField();
        panelZElementami.add(stworzEtykiete("Imie"));
        panelZElementami.add(imiePoleTekstowe, "span, growx, wrap");
        imiePoleTekstowe.setEnabled(false);
        imiePoleTekstowe.setDisabledTextColor(Color.black);

        nazwiskoPoleTekstowe = new JTextField("");
        panelZElementami.add(stworzEtykiete("Nazwisko"));
        panelZElementami.add(nazwiskoPoleTekstowe, "span, growx, wrap");
        nazwiskoPoleTekstowe.setEnabled(false);
        nazwiskoPoleTekstowe.setDisabledTextColor(Color.black);

        wiekPoleTekstowe = new JTextField("", 4);
        panelZElementami.add(stworzEtykiete("Wiek"));
        panelZElementami.add(wiekPoleTekstowe);
        wiekPoleTekstowe.setEnabled(false);
        wiekPoleTekstowe.setDisabledTextColor(Color.black);

        plecPoleWyboru = new JComboBox(Uzytkownik.zwrocTablicaLancuchowPlci());
        plecPoleWyboru.setFont(new Font("Verdana", Font.PLAIN, 12));
        plecPoleWyboru.setBackground(Color.white);
        panelZElementami.add(stworzEtykiete("Płeć"), "right");
        panelZElementami.add(plecPoleWyboru, "wrap");
        plecPoleWyboru.setEnabled(false);

        krajPochodzeniaPoleTekstowe = new JTextField("");
        panelZElementami.add(stworzEtykiete("Kraj pochodzenia"));
        panelZElementami.add(krajPochodzeniaPoleTekstowe, "span, growx");
        krajPochodzeniaPoleTekstowe.setEnabled(false);
        krajPochodzeniaPoleTekstowe.setDisabledTextColor(Color.black);
        
        aktualizujElementyOkna();
        
        JPanel panelZPrzyciskami = new JPanel();
        przyciskOK = new JButton("OK");
        przyciskOK.addActionListener(this);
        przyciskCancel = new JButton("Cancel");
        przyciskCancel.addActionListener(this);
        panelZPrzyciskami.add(przyciskOK);
        panelZPrzyciskami.add(przyciskCancel);
        
        oknoDialogowe.add(panelZElementami, BorderLayout.CENTER);
        oknoDialogowe.add(panelZPrzyciskami, BorderLayout.SOUTH);
        oknoDialogowe.pack();
        oknoDialogowe.setLocationRelativeTo(OknoGlowne.dajInstancje());
    }
    
    public void wyswietlOknoDialogowe() {
        oknoDialogowe.setVisible(true);
    }
    
    private void aktualizujElementyOkna() {
        imiePoleTekstowe.setText(uzytkownik.getImie());
        nazwiskoPoleTekstowe.setText(uzytkownik.getNazwisko());
        krajPochodzeniaPoleTekstowe.setText(uzytkownik.getKrajPochodzenia());
        wiekPoleTekstowe.setText(String.valueOf( uzytkownik.getWiek() ).equals("0") ? "" : String.valueOf( uzytkownik.getWiek() ));
        plecPoleWyboru.setSelectedItem(uzytkownik.getPlec().dajLancuch());
    }

    @Override
    public void powiadomObserwatorow() {        
        System.out.println(uzytkownik);
        
        for (IObserwator obserwator : obserwatorzy) {
            obserwator.aktualizacjaWezla(TypyPowiadomienia.USUNIECIE, uzytkownik);
        }    
        
        System.out.println("Powiadomienie");
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if(ie.getSource() instanceof JComboBox) {
            JComboBox poleWyboru = (JComboBox) ie.getSource();
            
            if (ie.getStateChange() == ItemEvent.SELECTED) {
                uzytkownik = (Uzytkownik) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.UZYTKOWNIK, String.valueOf(poleWyboru.getSelectedItem()));
                System.out.println("Zmieniono combobox na " + uzytkownik);
                
                aktualizujElementyOkna();
           }
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        Object obiekt = ae.getSource();  
        
        if(obiekt instanceof JButton) {
            JButton przycisk = (JButton) obiekt;
            
            if (przycisk == przyciskCancel) {
                oknoDialogowe.setVisible(false);
            } else if (przycisk == przyciskOK) {
                int odpowiedz = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć wybranego użytkownika?", "Usuń użytkownika", JOptionPane.YES_NO_OPTION);
                if (odpowiedz == JOptionPane.YES_OPTION) {
                   powiadomObserwatorow();
                   oknoDialogowe.setVisible(false);
                }  
            }    
        }
    }
    
}
