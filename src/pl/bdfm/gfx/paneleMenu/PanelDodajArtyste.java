
package pl.bdfm.gfx.paneleMenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import static java.awt.Component.LEFT_ALIGNMENT;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import net.miginfocom.swing.MigLayout;
import pl.bdfm.gfx.OknoGlowne;
import pl.bdfm.logic.Artysta;
import pl.bdfm.logic.FunkcjeTekstowe;
import pl.bdfm.logic.IObserwator;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelDodajArtyste extends PanelOknaDialogowego implements ActionListener {
    private final JDialog oknoDialogowe;
    private final JPanel panelZElementami;
    
    private final JButton przyciskOK, przyciskCancel;
    
    private final JTextField nazwaPoleTekstowe, krajPoleTekstowe;
    private final JTextArea opisPoleTekstowe;
    private final List<JTextField> listaGatunkowPoleTekstowe;
    
    public PanelDodajArtyste() {
        listaGatunkowPoleTekstowe = new ArrayList<>();
        
        oknoDialogowe = new JDialog(OknoGlowne.dajInstancje(), "Dodaj artyste", true);
        oknoDialogowe.setLayout(new BorderLayout());
        oknoDialogowe.setResizable(false);
        
        panelZElementami = new JPanel();
        panelZElementami.setLayout(new MigLayout());
        
        panelZElementami.add(stworzEtykiete("Nazwa"));
        nazwaPoleTekstowe = new JTextField("",18);
        panelZElementami.add(nazwaPoleTekstowe, "wrap");
        
        panelZElementami.add(stworzEtykiete("Kraj pochodzenia"));
        krajPoleTekstowe = new JTextField("",18);
        panelZElementami.add(krajPoleTekstowe, "span 2, growx, wrap");

        panelZElementami.add(stworzEtykiete("Opis"));

        opisPoleTekstowe = new JTextArea(4,18);
        opisPoleTekstowe.setEditable(true);
        opisPoleTekstowe.setBackground(Color.white);
        opisPoleTekstowe.setBorder(null);
        opisPoleTekstowe.setFont(new Font("Verdana", Font.PLAIN, 12));
        opisPoleTekstowe.setForeground(Color.black);
        opisPoleTekstowe.setWrapStyleWord(true);
        opisPoleTekstowe.setLineWrap(true);
        opisPoleTekstowe.setAlignmentX(LEFT_ALIGNMENT);
        opisPoleTekstowe.setCaretPosition(0);

        JScrollPane panelZOpisem = new JScrollPane(opisPoleTekstowe);
        panelZOpisem.setBorder(new LineBorder(Color.gray));
        panelZOpisem.setAlignmentX(LEFT_ALIGNMENT);

        panelZElementami.add(panelZOpisem, "span 2, growx, wrap");
        
        panelZElementami.add(stworzEtykiete("Gatunki"));
        JTextField gatunekPoleTekstowe = new JTextField("");
        listaGatunkowPoleTekstowe.add(gatunekPoleTekstowe);
        panelZElementami.add(gatunekPoleTekstowe, "split 2, growx");

        JButton przycisk = new JButton("+");
        przycisk.setMargin(new Insets(0, 0, 0, 0));
        przycisk.addActionListener(this);
        
        panelZElementami.add(przycisk, "width 25px:pref, wrap");
        
        
        JPanel panelZPrzyciskami = new JPanel();
        przyciskOK = new JButton("OK");
        przyciskOK.addActionListener(this);
        przyciskCancel = new JButton("Cancel");
        przyciskCancel.addActionListener(this);
        panelZPrzyciskami.add(przyciskOK);
        panelZPrzyciskami.add(przyciskCancel);
        
        oknoDialogowe.add(panelZElementami, BorderLayout.CENTER);
        oknoDialogowe.add(panelZPrzyciskami, BorderLayout.SOUTH);
        oknoDialogowe.pack();
        oknoDialogowe.setLocationRelativeTo(OknoGlowne.dajInstancje());

    }
    
    public void wyswietlOknoDialogowe() {
        oknoDialogowe.setVisible(true);
    }
    
    private void odswiezOknoDialogowe() {
        panelZElementami.revalidate();
        oknoDialogowe.pack();
    }
    
    @Override
    public void powiadomObserwatorow() {
        Artysta artysta = wygenerujArtyste();
        
        System.out.println(artysta);   
        for (IObserwator obserwator : obserwatorzy) {
            obserwator.aktualizacjaWezla(TypyPowiadomienia.DODANIE, artysta);
        }    
        
        System.out.println("Powiadomienie");
    }
    
    private Artysta wygenerujArtyste() {
        String krajPochodzenia = "";
        if (!krajPoleTekstowe.getText().isEmpty()) {
            krajPochodzenia = FunkcjeTekstowe.duzeLiteryKazdegoSlowa(krajPoleTekstowe.getText().trim());
        }
        
        List<String> listaGatunkow = new ArrayList();
        for (JTextField gatunekPoleTekstowe : listaGatunkowPoleTekstowe) {
            if (!gatunekPoleTekstowe.getText().isEmpty()) {
                listaGatunkow.add(FunkcjeTekstowe.pierwszaLiteraDuza(gatunekPoleTekstowe.getText().trim()));
            }
        }
        
        Artysta artysta = new Artysta(nazwaPoleTekstowe.getText().trim(), 
                krajPochodzenia, 
                listaGatunkow, 
                opisPoleTekstowe.getText().trim());
        
        return artysta;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        Object obiekt = ae.getSource();
        
        if(obiekt instanceof JButton) {
            JButton przycisk = (JButton) obiekt;
            
            if ((przycisk.getText().equals("+"))) {
                JButton przyciskMinus = new JButton("-");
                przyciskMinus.addActionListener(this);
                przyciskMinus.setMargin(new Insets(0, 0, 0, 0));

                JTextField gatunekPoleTekstowe = new JTextField("");
                listaGatunkowPoleTekstowe.add(gatunekPoleTekstowe);
                panelZElementami.add(gatunekPoleTekstowe, "skip, split 2, growx");
                panelZElementami.add(przyciskMinus, "width 25px:pref, wrap");
                odswiezOknoDialogowe();
                    
            } else if ((przycisk.getText().equals("-"))) { 
                int numer = znajdzNumerKomponentu(obiekt, panelZElementami);

                listaGatunkowPoleTekstowe.remove(panelZElementami.getComponent(numer-1));
                panelZElementami.remove(numer);
                panelZElementami.remove(numer-1);
                odswiezOknoDialogowe();
            } else if (przycisk == przyciskCancel) {
                oknoDialogowe.setVisible(false);
            } else if (przycisk == przyciskOK) {
                powiadomObserwatorow();
                oknoDialogowe.setVisible(false);
            }
        }
    }


    private int znajdzNumerKomponentu(Object komponent, JPanel panel) {
        int numer = -1;
        int i = 0;
        
        Component[] komponenty = panel.getComponents();
        for (Component object : komponenty) {
                if(object == komponent) {
                    numer = i; 
                    break;
                }
                ++i;
        }
        
        return numer;
    }
    
}
