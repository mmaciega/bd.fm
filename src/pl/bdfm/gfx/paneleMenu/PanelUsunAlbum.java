
package pl.bdfm.gfx.paneleMenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import pl.bdfm.gfx.OknoGlowne;
import pl.bdfm.logic.Album;
import pl.bdfm.logic.Artysta;
import pl.bdfm.logic.IObserwator;
import pl.bdfm.logic.ZarzadcaBazaNeo4J;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class PanelUsunAlbum extends PanelOknaDialogowego implements ItemListener, ActionListener {
    private JDialog oknoDialogowe;
    private JPanel panelZElementami;
    
    private JButton przyciskOK, przyciskCancel;
    
    private JComboBox nazwaPoleWyboru;
    private JTextField rokWydaniaPoleTekstowe, liczbaPoleTekstowe, 
            czasPoleTekstowe;
    private JLabel wykonawcyEtykieta;
    private final String[] tablicaOpcjiPolaWyboru;
    private final List<JTextField> listaWykonawcowPoleTekstowe;
    
    private String komunikat;
    private boolean czyBlad;
    
    private Album album;
    
    public PanelUsunAlbum() {
        listaWykonawcowPoleTekstowe = new ArrayList<>();
        tablicaOpcjiPolaWyboru = ZarzadcaBazaNeo4J.dajInstancje().dajWezly(ZarzadcaBazaNeo4J.TypyWezlow.ALBUM).toArray(new String[0]);
        generowanieOknaDialogowego();
    }
    
    public PanelUsunAlbum(String nazwaZespolu) {
        listaWykonawcowPoleTekstowe = new ArrayList<>();
        
        Artysta artysta = (Artysta) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.ARTYSTA, nazwaZespolu);
        tablicaOpcjiPolaWyboru = artysta.getListaAlbumow().toArray(new String[0]);
        
        generowanieOknaDialogowego();
    }
    
    private void generowanieOknaDialogowego() {
        oknoDialogowe = new JDialog(OknoGlowne.dajInstancje(), "Usuń album artysty", true);
        oknoDialogowe.setLayout(new BorderLayout());
        oknoDialogowe.setResizable(false);
        
        panelZElementami = new JPanel();
        panelZElementami.setLayout(new MigLayout());
        
        
        if (tablicaOpcjiPolaWyboru.length != 0) {
        panelZElementami.add(stworzEtykiete("Nazwa albumu"));
        nazwaPoleWyboru = new JComboBox(tablicaOpcjiPolaWyboru);
        panelZElementami.add(nazwaPoleWyboru, "wrap");
        nazwaPoleWyboru.addItemListener(this);
        
        album = (Album) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.ALBUM, String.valueOf(nazwaPoleWyboru.getSelectedItem()));
        
        wykonawcyEtykieta = stworzEtykiete("Wykonawcy");
        panelZElementami.add(wykonawcyEtykieta);
        
        for (String wykonawca : album.getNazwaArtystow()) {
            JTextField wykonawcaPoleTekstowe = new JTextField(wykonawca);
            wykonawcaPoleTekstowe.setFont(new Font("Verdana", Font.PLAIN, 12));
            wykonawcaPoleTekstowe.setDisabledTextColor(Color.black);
            wykonawcaPoleTekstowe.setBackground(Color.white);
            wykonawcaPoleTekstowe.setEnabled(false);
            
            listaWykonawcowPoleTekstowe.add(wykonawcaPoleTekstowe);
            
            String warunki = "growx, wrap";
            if(listaWykonawcowPoleTekstowe.size() > 1) {
                warunki = "skip, growx, wrap";
            }
            
            panelZElementami.add(wykonawcaPoleTekstowe, warunki);
        }
        
        panelZElementami.add(stworzEtykiete("Liczba utworów"));
        liczbaPoleTekstowe = new JTextField(String.valueOf(album.getLiczbaUtworow()), 4);
        liczbaPoleTekstowe.setEnabled(false);
        liczbaPoleTekstowe.setDisabledTextColor(Color.black);
        panelZElementami.add(liczbaPoleTekstowe, "span 2, wrap");

        panelZElementami.add(stworzEtykiete("Rok wydania"));
        rokWydaniaPoleTekstowe = new JTextField(String.valueOf(album.getRokWydania()), 4);
        rokWydaniaPoleTekstowe.setDisabledTextColor(Color.black);
        rokWydaniaPoleTekstowe.setEnabled(false);

        panelZElementami.add(rokWydaniaPoleTekstowe, "split 2, wrap");

        panelZElementami.add(stworzEtykiete("Czas trwania"));
        czasPoleTekstowe = new JTextField(album.getCzasTrwania(), 4);
        czasPoleTekstowe.setDisabledTextColor(Color.black);
        czasPoleTekstowe.setEnabled(false);
        panelZElementami.add(czasPoleTekstowe, "split 2");
        
        JPanel panelZPrzyciskami = new JPanel();
        przyciskOK = new JButton("OK");
        przyciskOK.addActionListener(this);
        przyciskCancel = new JButton("Cancel");
        przyciskCancel.addActionListener(this);
        panelZPrzyciskami.add(przyciskOK);
        panelZPrzyciskami.add(przyciskCancel);
        
        oknoDialogowe.add(panelZElementami, BorderLayout.CENTER);
        oknoDialogowe.add(panelZPrzyciskami, BorderLayout.SOUTH);
        oknoDialogowe.pack();
        oknoDialogowe.setLocationRelativeTo(OknoGlowne.dajInstancje());
        } else {
            komunikat = "Brak albumów zespołu do usunięcia";
            czyBlad = true;
        }
    }
    
    public void wyswietlOknoDialogowe() {
        if (!czyBlad) {
            oknoDialogowe.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(OknoGlowne.dajInstancje(), komunikat, "Informacja", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    private void odswiezOknoDialogowe() {
        panelZElementami.revalidate();
        oknoDialogowe.pack();
    }
    
    private void aktualizujElementyOkna() {
        liczbaPoleTekstowe.setText(String.valueOf(album.getLiczbaUtworow()));
        rokWydaniaPoleTekstowe.setText(String.valueOf(album.getRokWydania()));
        czasPoleTekstowe.setText(album.getCzasTrwania());
        
        for (JTextField wykonawca : listaWykonawcowPoleTekstowe) {
            int numer = znajdzNumerKomponentu(wykonawca, panelZElementami);
           
            panelZElementami.remove(numer);
        }
        
        listaWykonawcowPoleTekstowe.clear();
        
        int numer = znajdzNumerKomponentu(wykonawcyEtykieta, panelZElementami);
        for (String wykonawca : album.getNazwaArtystow()) {
            JTextField wykonawcaPoleTekstowe = new JTextField(wykonawca);
            wykonawcaPoleTekstowe.setFont(new Font("Verdana", Font.PLAIN, 12));
            wykonawcaPoleTekstowe.setDisabledTextColor(Color.black);
            wykonawcaPoleTekstowe.setBackground(Color.white);
            wykonawcaPoleTekstowe.setEnabled(false);
            
            listaWykonawcowPoleTekstowe.add(wykonawcaPoleTekstowe);
            
            String warunki = "growx, wrap";
            if(listaWykonawcowPoleTekstowe.size() > 1) {
                warunki = "skip, growx, wrap";
            }
            
            panelZElementami.add(wykonawcaPoleTekstowe, warunki, numer + (listaWykonawcowPoleTekstowe.size()-1) + 1);
        }
        
        odswiezOknoDialogowe();
    }
    
    @Override
    public void powiadomObserwatorow() {
        System.out.println(album);   
        for (IObserwator obserwator : obserwatorzy) {
            obserwator.aktualizacjaWezla(TypyPowiadomienia.USUNIECIE, album);
        }    
        
        System.out.println("Powiadomienie");    
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if(ie.getSource() instanceof JComboBox) {
            JComboBox poleWyboru = (JComboBox) ie.getSource();
            
            if (ie.getStateChange() == ItemEvent.SELECTED) {
                album = (Album) ZarzadcaBazaNeo4J.dajInstancje().dajWezel(ZarzadcaBazaNeo4J.TypyWezlow.ALBUM, String.valueOf(poleWyboru.getSelectedItem()));
                System.out.println("Zmieniono combobox na " + album);
                
                aktualizujElementyOkna();
           }
        }
    }
    
    private int znajdzNumerKomponentu(Object komponent, JPanel panel) {
        int numer = -1;
        int i = 0;
        
        Component[] komponenty = panel.getComponents();
        for (Component object : komponenty) {
                if(object == komponent) {
                    numer = i; 
                    break;
                }
                ++i;
        }
        
        return numer;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        Object obiekt = ae.getSource();  
        
        if(obiekt instanceof JButton) {
            JButton przycisk = (JButton) obiekt;
            
            if (przycisk == przyciskCancel) {
                oknoDialogowe.setVisible(false);
            } else if (przycisk == przyciskOK) {
                int odpowiedz = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć wybrany album?", "Usuń album", JOptionPane.YES_NO_OPTION);
                if (odpowiedz == JOptionPane.YES_OPTION) {
                   powiadomObserwatorow();
                   oknoDialogowe.setVisible(false);
                }  
            }    
        }
    }
    
}
