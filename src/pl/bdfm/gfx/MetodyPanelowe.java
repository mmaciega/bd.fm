/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.bdfm.gfx;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.List;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import pl.bdfm.logic.ZarzadcaBazaNeo4J;
import pl.bdfm.logic.ZarzadcaKursorem;
import pl.bdfm.logic.ZarzadcaLinkami;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public final class MetodyPanelowe {
    public static JPanel wygnerujPanelZLista(List<String> lista, ZarzadcaBazaNeo4J.TypyWezlow typWezla) {
        JPanel panelOpakowujacy = new JPanel();
        panelOpakowujacy.setOpaque(false);
        panelOpakowujacy.setLayout(new BorderLayout());

        JPanel panelZLinkami = new JPanel();
        panelZLinkami.setOpaque(false);
        panelZLinkami.setLayout(new BoxLayout(panelZLinkami, BoxLayout.Y_AXIS));

        for (String element : lista) {
            dodajLink(panelZLinkami, element, typWezla);
        }

        panelOpakowujacy.add(panelZLinkami, BorderLayout.NORTH);

        return panelOpakowujacy;
    }
    
    private static void dodajLink(JPanel panel, String tekst, ZarzadcaBazaNeo4J.TypyWezlow typWezla) {          
            JTextArea obszarZTekstem=new JTextArea(tekst);
            obszarZTekstem.setEditable(false);
            obszarZTekstem.setBackground(null);
            obszarZTekstem.setBorder(null);
            obszarZTekstem.setOpaque(false);
            obszarZTekstem.setFont(new Font("Verdana", Font.PLAIN, 15));
            obszarZTekstem.setForeground(Color.black);
            obszarZTekstem.setDisabledTextColor(Color.black);
            obszarZTekstem.setWrapStyleWord(true);
            obszarZTekstem.setLineWrap(true);
            
            obszarZTekstem.putClientProperty("typWezla", typWezla);
            obszarZTekstem.putClientProperty("nazwa", tekst);
            
            obszarZTekstem.addMouseMotionListener(ZarzadcaKursorem.dajInstancje());
            obszarZTekstem.addMouseListener(ZarzadcaLinkami.dajInstancje());
            
            panel.add(obszarZTekstem);
            panel.add(Box.createRigidArea(new Dimension(0,15)));
    }
}
