package pl.bdfm;

import pl.bdfm.gfx.OknoGlowne;

/**
 *
 * @author Mateusz Macięga, Piotr Gębala
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        OknoGlowne.dajInstancje();
    }
    
}
